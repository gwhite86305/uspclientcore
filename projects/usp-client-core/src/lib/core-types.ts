export enum SearchLoadingProgress {
	noResults,
	partialResults,
	fullResults
}

export interface ReleaseNote {
	version: string;
	notes: string[];
}

export interface RefData {
	text: string;
	value: string;
}