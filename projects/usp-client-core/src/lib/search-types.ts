import { SearchLoadingProgress, RefData } from './core-types';
import * as momentNs from 'moment';
const moment = momentNs;
const _ = require('lodash');

export const OVERVIEW = 'OVERVIEW';
export const PEOPLE = 'PEOPLE';
export const VEHICLES = 'VEHICLES';
export const CARRIERS = 'CARRIERS';

export interface ServiceStatusError {
	code: number;
	name: string;
	source: string;
	message: string;
}
export interface ServiceStatusModel {
	state: { status: string; message: string };
	error?: ServiceStatusError;
}
export interface ServiceStatusAll {
	[status: string]: ServiceStatusModel;
}
export interface NetworkStatus {
	internet?: boolean;
	vpn?: boolean;
	api?: boolean;
	loggedin?: boolean;
}

export class ResultCategory {
	categoryName: string;
	numResults: number;
	categoryIconName: string;
	categoryIconSrc: string;
	categoryColor: string;
	categoryError: Error;

	constructor(newCat: Partial<ResultCategory>) {
		this.categoryName = _.startCase(newCat.categoryName);
		this.numResults = 0;
		this.categoryIconName = newCat.categoryIconName;
		this.categoryIconSrc = newCat.categoryIconSrc;
		this.categoryColor = newCat.categoryColor;
		this.categoryError = null;
	}
}

export interface RegistrationModel {
	isRegistered: boolean;
	isActive?: boolean;
}

export interface CarrierModel {
	dbaName: string;
	legalName: string;
	dotNumber: number;
	operationClass: string;
	phone: string;
	state: string;

	isInService: boolean;
	isOutOfService: boolean;
	isActive: boolean;
	cargoCarried: string;
	outstandingImpound: boolean;
	unpaidCitationCount: number;

	basics: {
		basicType: string;
		hasExceededThreshold: boolean;
	}[];
	ucrData: any;

	physAddress: {
		street: string;
		city: string;
		state: string;
		zipcode: string;
		country: string;
	};

	// Add to these as we get more vendors
	federalRegistration: RegistrationModel;
	georgiaRegistration: RegistrationModel;
	hhgRegistration: RegistrationModel;
	passengerRegistration: RegistrationModel;
}

export class PersonModel {
	firstName: string;
	middleName: string;
	lastName: string;
	licenseNumber: string;
	birthDate: string;
	birthPlaceCode: string;
	sexCode: string;
	raceCode: string;
	heightFeet: string;
	heightInches: string;
	weight: string;
	eyeColorCode: string;
	hairColorCode: string;
	scarMarkTattooCode: string;

	address: string;
	addressCity: string;
	addressCounty: string;
	addressStateCode: string;
	addressStreetName: string;
	addressZipCode: string;
	class: string;
	expirationDate: string;
	registrationStatusCode: string;
	registrationType: string;

	idNumbers: {
		ncic: string;
		fbi: string;
		ssn: string;
		ncicMisc: string;
	};
	offenses: [
		{
			code: string;
		}
	];
	flags: [
		{
			source: string;
			banner: string;
		}
	];

	constructor() {
		this.firstName = 'N/A';
		this.middleName = 'N/A';
		this.lastName = 'N/A';
		this.licenseNumber = 'N/A';
		this.birthDate = 'N/A';
		this.birthPlaceCode = 'N/A';
		this.sexCode = 'N/A';
		this.raceCode = 'N/A';
		this.heightFeet = 'N/A';
		this.heightInches = 'N/A';
		this.weight = 'N/A';
		this.eyeColorCode = 'N/A';
		this.hairColorCode = 'N/A';
		this.scarMarkTattooCode = 'N/A';

		this.address = 'N/A';
		this.addressCity = 'N/A';
		this.addressCounty = 'N/A';
		this.addressStateCode = 'N/A';
		this.addressStreetName = 'N/A';
		this.addressZipCode = 'N/A';
		this.class = 'N/A';
		this.expirationDate = 'N/A';
		this.registrationStatusCode = 'N/A';
		this.registrationType = 'N/A';

		this.idNumbers = {
			ncic: 'N/A',
			fbi: 'N/A',
			ssn: 'N/A',
			ncicMisc: 'N/A'
		};
		this.offenses = [{ code: 'N/A' }];
	}
}

export class OwnerModel {
	firstName: string;
	middleName: string;
	lastName: string;
	birthDate: string;
	licenseNumber: string;

	constructor() {
		this.firstName = 'N/A';
		this.middleName = 'N/A';
		this.lastName = 'N/A';
		this.birthDate = 'N/A';
		this.licenseNumber = 'N/A';
	}
}

export class VehicleModel {
	year: string;
	colorCode: string;
	makeCode: string;
	modelCode: string;
	styleCode: string;
	plateNumber: string;
	plateTypeCode: string;
	plateYear: string;
	tagExpirationDate: string;
	tagIssueDate: string;
	address: string;
	freeText: string;

	autoInsurance: string;
	plateState: string;
	registrationStatusCode: string;
	registrationType: string;
	registrationTypeCode: string;

	owner: OwnerModel;
	coOwner: OwnerModel;
	idNumbers: {
		vin: string;
		originatingAgency: string;
		ncic: string;
		fbi: string;
		ssn: string;
		ncicMisc: string;
	};
	flags: [
		{
			source: string;
			banner: string;
		}
	];

	constructor() {
		this.year = 'N/A';
		this.colorCode = 'N/A';
		this.makeCode = 'N/A';
		this.modelCode = 'N/A';
		this.styleCode = 'N/A';
		this.plateNumber = 'N/A';
		this.plateTypeCode = 'N/A';
		this.plateYear = 'N/A';
		this.tagExpirationDate = 'N/A';
		this.tagIssueDate = 'N/A';
		this.address = 'N/A';
		this.freeText = 'N/A';

		this.autoInsurance = 'N/A';
		this.plateState = 'N/A';
		this.registrationStatusCode = 'N/A';
		this.registrationType = 'N/A';
		this.registrationTypeCode = 'N/A';

		this.owner = new OwnerModel();
		this.coOwner = new OwnerModel();
		this.idNumbers = {
			vin: 'N/A',
			originatingAgency: 'N/A',
			ncic: 'N/A',
			fbi: 'N/A',
			ssn: 'N/A',
			ncicMisc: 'N/A'
		};
	}
}

export class CitationModel {
	DocumentID: string;
	TicketNum: string;
	TicketCounty: string;
	TicketCountyNum: number;
	TicketCity: string;
	TicketCityNum: number;
	DateIssued: string;
	DocStatus: number;
	DocStatusName: string;
	DocType: string;
	CreatorLEA: string;
	CreatorUserID: string;
	CreatorBadgeNum: string;
	CreatorLastName: string;
	OfficerName: string;
	VoidReason: string;
	DefendantFirstName: string;
	DefendantMiddleName: string;
	DefendantLastName: string;
	DefendantGender: string;
	DefendantDOB: string;
	LicNum: string;
	LicState: string;
	Courthouse: string;
	CourtDate: string;
	CourtCode: number;
	Jailed: boolean;
	RealCourtCode: string;
	VehTagNum: string;
	VehTagState: string;
	Charge: string;
	ChargeDescription: string;
	IsClerkNotesAvailable: string;
	TransferredStatus: string;

	constructor(newModel: CitationModel) {
		this.DocumentID = newModel.DocumentID;
		this.TicketNum = newModel.TicketNum;
		this.TicketCounty = newModel.TicketCounty;
		this.TicketCountyNum = newModel.TicketCountyNum;
		this.TicketCity = newModel.TicketCity;
		this.TicketCityNum = newModel.TicketCityNum;
		this.DateIssued = newModel.DateIssued;
		this.DocStatus = newModel.DocStatus;
		this.DocStatusName = newModel.DocStatusName;
		this.DocType = newModel.DocType;
		this.CreatorLEA = newModel.CreatorLEA;
		this.CreatorUserID = newModel.CreatorUserID;
		this.CreatorBadgeNum = newModel.CreatorBadgeNum;
		this.CreatorLastName = newModel.CreatorLastName;
		this.OfficerName = newModel.OfficerName;
		this.VoidReason = newModel.VoidReason;
		this.DefendantFirstName = newModel.DefendantFirstName;
		this.DefendantMiddleName = newModel.DefendantMiddleName;
		this.DefendantLastName = newModel.DefendantLastName;
		this.DefendantGender = newModel.DefendantGender;
		this.DefendantDOB = newModel.DefendantDOB;
		this.LicNum = newModel.LicNum;
		this.LicState = newModel.LicState;
		this.Courthouse = newModel.Courthouse;
		this.CourtDate = newModel.CourtDate;
		this.CourtCode = newModel.CourtCode;
		this.Jailed = newModel.Jailed;
		this.RealCourtCode = newModel.RealCourtCode;
		this.VehTagNum = newModel.VehTagNum;
		this.VehTagState = newModel.VehTagState;
		this.Charge = newModel.Charge;
		this.ChargeDescription = newModel.ChargeDescription;
		this.IsClerkNotesAvailable = newModel.IsClerkNotesAvailable;
		this.TransferredStatus = newModel.TransferredStatus;
	}
}

export class ThreatBadge {
	badgeName: string;
	tooltip: string;
	keywords: string[];
	imageSrc: string;
	noCount: boolean;
	threatCount: number;


	constructor() {
		this.threatCount = 0;
	}
}

export enum ThreatLevel {
	None,
	Low,
	High,
	Default
}

export class CitationCharge {
	Charge: string;
	ChargeCount: number;

	constructor(charge: string, count: number) {
		this.Charge = charge;
		this.ChargeCount = count;
	}
}

export class ChargeExceptions {
	ExpiredLicenses: boolean;
	NoLicenses: boolean;
	RecentDUIs: boolean;

	constructor(expired: number, noLic: number, duis: number) {
		this.ExpiredLicenses = expired > 0;
		this.NoLicenses = noLic > 0;
		this.RecentDUIs = duis > 0;
	}
}

export class AtlasQueryLogModel {
	QueryLogID: string;
	DateSearched: string;
	OfficerID: string;
	OfficerName: string;
	QueryType: string;
	State: string;
	LicNum: string;
	VehTagNum: string;
	DefendantFirstName: string;
	DefendantLastName: string;
	DefendantGender: string;
	DefendantDOB: string;
	AgencyName: string;
	District: string;
	QueryLogNote: string;

	constructor() {
		this.QueryLogID = null;
		this.DateSearched = null;
		this.OfficerID = null;
		this.OfficerName = null;
		this.QueryType = null;
		this.State = null;
		this.LicNum = null;
		this.VehTagNum = null;
		this.DefendantFirstName = null;
		this.DefendantLastName = null;
		this.DefendantGender = null;
		this.DefendantDOB = null;
		this.AgencyName = null;
		this.District = null;
		this.QueryLogNote = null;
	}

	convertToSearchTerms() {
		let searchTerms: SearchListObj[] = [];

		switch (this.QueryType) {
			case 'vehicle':
				searchTerms.push({ name: 'VehTagState', value: this.State, type: 'vehicle' });
				searchTerms.push({ name: 'VehTagNum', value: this.VehTagNum, type: 'vehicle' });
				break;
			case 'person':
				searchTerms.push({ name: 'DefendantFirstName', value: this.DefendantFirstName, type: 'person' });
				searchTerms.push({ name: 'DefendantLastName', value: this.DefendantLastName, type: 'person' });
				searchTerms.push({ name: 'DefendantGender', value: this.DefendantGender, type: 'person' });
				searchTerms.push({ name: 'DefendantDOB', value: this.DefendantDOB, type: 'person' });
				break;
			case 'dln':
				searchTerms.push({ name: 'LicState', value: this.State, type: 'vehicle' });
				searchTerms.push({ name: 'LicNum', value: this.LicNum, type: 'vehicle' });
				break;
			default:
				return;
		}

		return searchTerms;
	}

	parseFromSearchTerms(searchTerms: SearchListObj[]) {
		//Atlas query log table has a different value for the license and history types
		let queryType = searchTerms[0].type;
		if (queryType == 'license')
			this.QueryType = 'dln';
		else if (queryType == 'history')
			return;
		else
			this.QueryType = queryType;

		switch (queryType) {
			case 'person':
				this.DefendantFirstName = _.get(_.find(searchTerms, ['name', 'DefendantFirstName']), 'value', '').toUpperCase();
				this.DefendantLastName = _.get(_.find(searchTerms, ['name', 'DefendantLastName']), 'value', '').toUpperCase();
				this.DefendantGender = _.get(_.find(searchTerms, ['name', 'DefendantGender']), 'value', '').toUpperCase();
				this.DefendantDOB = _.get(_.find(searchTerms, ['name', 'DefendantDOB']), 'value', '');
				break;
			case 'vehicle':
				this.State = _.get(_.find(searchTerms, ['name', 'VehTagState']), 'value', '');
				this.VehTagNum = _.get(_.find(searchTerms, ['name', 'VehTagNum']), 'value', '');
				break;
			case 'license':
				this.State = _.get(_.find(searchTerms, ['name', 'LicState']), 'value', '');
				this.LicNum = _.get(_.find(searchTerms, ['name', 'LicNum']), 'value', '');
				break;
			default:
				break;
		}
	}

	mapForInsert() {
		return {
			datetime: this.DateSearched,
			officeruserid: this.OfficerID,
			querytype: this.QueryType,
			state: this.State,
			dln: this.LicNum,
			vehicletagnumber: this.VehTagNum,
			firstname: this.DefendantFirstName,
			lastname: this.DefendantLastName,
			sex: this.DefendantGender,
			dob: this.DefendantDOB,
			querylognote: this.QueryLogNote,
			lea: this.AgencyName,
			district: this.District
		}
	}

	mapForSearch(maxLogs: string, interdictionUser: boolean, startDate: string, endDate: string, keyword: string) {
		let searchBody = {
			SearchingOfficerID: this.OfficerID,
			IsInterdictionUser: interdictionUser,
		};

		if (startDate) searchBody['StartDate'] = startDate;
		if (endDate) searchBody['EndDate'] = endDate;
		if (keyword) searchBody['Keyword'] = keyword;
		if (maxLogs) searchBody['MaxLogEntries'] = parseInt(maxLogs);
		if (this.QueryType) searchBody['QueryType'] = this.QueryType;
		if (this.State) searchBody['State'] = this.State;
		if (this.LicNum) searchBody['LicNum'] = this.LicNum;
		if (this.VehTagNum) searchBody['VehTagNum'] = this.VehTagNum;
		if (this.DefendantFirstName) searchBody['DefendantFirstName'] = this.DefendantFirstName;
		if (this.DefendantLastName) searchBody['DefendantLastName'] = this.DefendantLastName;
		if (this.DefendantGender) searchBody['DefendantGender'] = this.DefendantGender;
		if (this.DefendantDOB) searchBody['DefendantDOB'] = this.DefendantDOB;
		if (this.QueryLogNote) searchBody['QueryLogNote'] = this.QueryLogNote;

		return searchBody;
	}
}

// export interface QueryTemplate {
// 	queryId: UniqueIdentifier;
// 	tabNum: number;
// 	searchList: SearchList;
// 	// filters: string[];
// 	searchedAt: Date;
// 	results: SearchResultData;
// 	loading: boolean;
// 	loadingProgress?: SearchLoadingProgress;
// 	error?: string;
// 	showOffline?: boolean;
// 	newResults?: boolean;
// 	notificationIds?: string[];
// }

// export interface ResultDataTemplate {
// 	people?: SocketResponseList;
// 	vehicles?: SocketResponseList;
// 	carriers?: CarrierModel[];
// }

// export interface HistoryDataTemplate {
// 	searchData: SearchResultData;
// 	searchDate: Date;
// 	searchList: SearchList;
// 	//We don't care about the extra DB fields currently, so just handwave them.
// 	[propName: string]: any;
// }

export class UniqueIdentifier {
	guid: string;

	constructor() {
		let d = new Date().getTime();

		if (typeof performance !== 'undefined' && typeof performance.now === 'function') {
			d += performance.now(); //use high-precision timer if available
		}

		this.guid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
			let r = (d + Math.random() * 16) % 16 | 0;
			d = Math.floor(d / 16);
			return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
		});
	}
}

export class SearchQuery {
	queryId: UniqueIdentifier;
	searchList: SearchListObj[];
	searchedAt: Date;
	error: string;
	notificationIds: string[];

	constructor(queryObj: SearchQuery) {
		this.queryId = queryObj.queryId || new UniqueIdentifier();
		this.searchList = queryObj.searchList;
		this.searchedAt = queryObj.searchedAt || null;
		this.error = queryObj.error || null;
		this.notificationIds = queryObj.notificationIds || [];
	}
}

export class SearchTab {
	queryId: UniqueIdentifier;
	tabNumber: number;
	tabLabel: string;
	loading: boolean;
	loadingProgress?: SearchLoadingProgress;
	showOffline: boolean;
	newResults: boolean;
	newPeople: boolean;
	newVehicles: boolean;
	highlightColor: string;

	constructor(tab: SearchTab) {
		this.queryId = tab.queryId;
		this.tabNumber = tab.tabNumber;
		this.tabLabel = tab.tabLabel;
		this.loading = tab.loading;
		this.loadingProgress = tab.loadingProgress;
		this.showOffline = tab.showOffline;
		this.newResults = tab.newResults;
		this.newPeople = tab.newPeople;
		this.newVehicles = tab.newVehicles;
		this.highlightColor = tab.highlightColor;
	}
}

export class SearchResult {
	queryId: UniqueIdentifier;
	results: any[];
	// history: any[];
	resultCount: number;
	[propName: string]: any;

	constructor(newResult: SearchResult) {
		this.queryId = newResult.queryId;
		this.results = newResult.results;
		// this.history = newResult.history;
		this.resultCount = newResult.resultCount;
	}
}

// export class SearchResult {
// 	queryId: UniqueIdentifier;
// 	people: SocketResponseList;
// 	vehicles: SocketResponseList;
// 	carriers: CarrierModel[];

// 	constructor(result: SearchResult) {
// 		this.queryId = result.queryId;
// 		this.people = { responses: [], priorityResponse: new SocketResponse() };
// 		this.vehicles = { responses: [], priorityResponse: new SocketResponse() };
// 		this.carriers = result.carriers || [];
// 	}
// }

export class SearchHistoryData {
	searchData: SearchResult;
	searchDate: Date;
	searchList: SearchListObj[];
	searchType: string;

	constructor(history: SearchHistoryData) {
		// this.searchData = history.searchData || new SearchResult();
		this.searchDate = history.searchDate || null;
		this.searchList = history.searchList;
		this.searchType = history.searchType;
	}
}

export class SocketResponseList {
	responses: SocketResponse[];
	priorityResponse: SocketResponse;
}

export class SocketResponse {
	query: {
		fields: [{
			name: string;
			value: string;
		}];
		id: UniqueIdentifier;
		messageType: string;
	};
	searchObj: SearchListObj;
	responseFullMessage: string;
	responseType: string;
	responseSchemaVersion: string;
	responseSource: string;
	responseSourceType?: string;
	person?: PersonModel;
	vehicle?: VehicleModel;
}

// export class SearchTerms {
// 	carrier: {
// 		dotorname: string;
// 	};
// 	person: {
// 		name: string;
// 		dob: string;
// 		sex: string;
// 		oln: string;
// 		state: string;
// 	};
// 	vehicle: {
// 		tagnumber: string;
// 		vinnumber: string;
// 		state: string;
// 		type: string;
// 		tagyear: string;
// 	}

// 	constructor() {
// 		this.carrier = { dotorname: '' };
// 		this.person = { name: '', dob: '', sex: '', oln: '', state: 'GA' };
// 		this.vehicle = { tagnumber: '', vinnumber: '', state: 'GA', type: 'PC', tagyear: '' };
// 	}

// 	trim() {
// 		this.carrier.dotorname = this.carrier.dotorname.trim();
// 		this.person.name = this.person.name.trim();
// 		this.person.dob = this.person.dob.trim();
// 		this.person.sex = this.person.sex.trim();
// 		this.person.oln = this.person.oln.trim();
// 		this.vehicle.tagnumber = this.vehicle.tagnumber.trim();
// 		this.vehicle.vinnumber = this.vehicle.vinnumber.trim();
// 		this.vehicle.tagyear = this.vehicle.tagyear.trim();
// 	}
// }

export class SearchListObj {
	name: string;
	value: string;
	type: string;
}

// export class SearchTermList {
// 	queries: SearchListObj[];

// 	constructor(newQueries: SearchTerms) {
// 		this.queries = [];

// 		// if (terms.carrier.dotorname)
// 		// 	this.queries.push({ name: 'DOTOrName', type: 'carrier', value: terms.carrier.dotorname });

// 		// //check if any person terms were provided
// 		// if (terms.person.dob || terms.person.name || terms.person.oln || terms.person.sex) {
// 		// 	if (terms.person.state)
// 		// 		this.queries.push({ name: 'State', type: 'person', value: terms.person.state });
// 		// 	if (terms.person.name && terms.person.dob && terms.person.sex) {
// 		// 		this.queries.push({ name: 'Name', type: 'person', value: terms.person.name });
// 		// 		this.queries.push({ name: 'BirthDate', type: 'person', value: terms.person.dob });
// 		// 		this.queries.push({ name: 'SexCode', type: 'person', value: terms.person.sex });
// 		// 	}
// 		// 	else if (terms.person.oln)
// 		// 		this.queries.push({ name: 'OperatorLicenseNumber', type: 'person', value: terms.person.oln });
// 		// }

// 		// //check if any vehicle terms were provided
// 		// if (terms.vehicle.tagnumber || terms.vehicle.vinnumber) {
// 		// 	if (terms.vehicle.state)
// 		// 		this.queries.push({ name: 'State', type: 'vehicle', value: terms.vehicle.state });
// 		// 	if (terms.vehicle.type)
// 		// 		this.queries.push({ name: 'LicensePlateTypeCode', type: 'vehicle', value: terms.vehicle.type });
// 		// 	if (terms.vehicle.tagnumber) {
// 		// 		this.queries.push({ name: 'LicensePlateNumber', type: 'vehicle', value: terms.vehicle.tagnumber });
// 		// 		if (terms.vehicle.tagyear)
// 		// 			this.queries.push({ name: 'LicensePlateYear', type: 'vehicle', value: terms.vehicle.tagyear });
// 		// 	}
// 		// 	else if (terms.vehicle.vinnumber)
// 		// 		this.queries.push({ name: 'VehicleIdentificationNumber', type: 'vehicle', value: terms.vehicle.vinnumber });
// 		// }
// 	}

// 	static getCarrierSearchTerm(queries: SearchListObj[]): SearchListObj {
// 		return _.find(queries, ['name', 'DOTOrName']);
// 	}
// }

export class UserAgent {
	appVersion: string;
	nodeVersion: string;
	nodeV8: string;
	userAgent: string;
	electron: string;
	isElectronApp: boolean;
	environment: string;
}

export class ResultCounts {
	totalResults: number;
	totalCarriers: number;
	totalPeople: number;
	totalVehicles: number;

	constructor(counts: ResultCounts) {
		this.totalResults = counts.totalResults;
		this.totalCarriers = counts.totalCarriers;
		this.totalPeople = counts.totalPeople;
		this.totalVehicles = counts.totalVehicles;
	}
}

export class SearchCategory {
	name: string;
	displayName: string;
	dataSources?: string[];
	searchRoute?: string;
	categoryIcon?: string;
	privilegeLevel?: number;
	fields: GenericField[];
	fieldGroups: FieldGrouping[];

	constructor(newCat: SearchCategory) {
		this.name = newCat.name;
		this.displayName = newCat.displayName;
		this.dataSources = newCat.dataSources || [];
		this.searchRoute = newCat.searchRoute || '';
		this.categoryIcon = newCat.categoryIcon || '';
		this.privilegeLevel = newCat.privilegeLevel || 0;
		this.fields = newCat.fields;
		this.fieldGroups = newCat.fieldGroups;
	}
}

export class FieldGrouping {
	groupName: string;
	groupFields: GenericField[];

	constructor(newGroup: FieldGrouping) {
		this.groupName = newGroup.groupName;
		this.groupFields = newGroup.groupFields;
	}
}

export class GenericField {
	friendlyFieldName: string;
	internalFieldName: string;
	uniqueFieldName: string;
	type: string;
	referenceItems?: RefData[];
	defaultField?: boolean;
	defaultForCategory?: boolean;
	defaultValue?: any;
	fieldIcon?: string;
	value?: any;
	required?: boolean;
	[key: string]: any;
	// searchable: boolean;

	constructor(newField: GenericField) {
		this.friendlyFieldName = newField.friendlyFieldName;
		this.internalFieldName = newField.internalFieldName;
		this.uniqueFieldName = newField.uniqueFieldName;
		this.type = newField.type;
		this.referenceItems = newField.referenceItems || [];
		this.defaultField = newField.defaultField || false;
		this.defaultValue = newField.defaultValue || null;
		this.fieldIcon = newField.fieldIcon || '';
		this.value = newField.value || null;
		this.required = newField.required || false;
		// this.searchable = newField.searchable;
	}
}

export class FieldTypes {
	static readonly TEXT: string = 'text';
	static readonly REFERENCE: string = 'reference';
	static readonly DATE: string = 'date';
	static readonly TOGGLE: string = 'toggle';
}

export class OverviewCardField {
	fieldKey: string;
	fieldValue: string;

	constructor(field: any) {
		this.fieldKey = field.fieldKey;
		this.fieldValue = field.fieldValue;
	}
}

/* LETS Data Types */
export enum LETSDatasource {
	License = 'DL',
	DOC = 'DOC',
	Warrant = 'HotfileWarrant',
	NameMaster = 'NameMaster',
	ProtectionOrder = 'ProtectionOrder',
	SexOffender = 'SexOffender',
	Boat = 'Boat',
	Tag = 'Tag',
	AlaSafe = 'AlaSafe',
	VIN = 'VIN',
	Citation = 'Citation'
}

export class LETSRecentSearch {
	rawParams: string;
	parsedParams: any = {};
	sources: LETSDatasource[] = [];

	constructor(rawParams: string) {
		this.rawParams = rawParams;
		this.parseRawParams();
	}

	private parseRawParams() {
		let spaceSplit: string[] = _.split(this.rawParams, ' ');

		//Process the substrings containing the substring 'search:' to get the sources
		//Process all others as key:value pairs to be assembled into a flat object
		_.map(spaceSplit, (param: string) => {
			if (_.includes(param, 'search:'))
				this.sources.push(param.slice(7) as LETSDatasource);
			else {
				let keyVal: string[] = _.split(param, ':');
				let key = keyVal[0];
				let value = keyVal[1];
				_.assign(this.parsedParams, JSON.parse('{ "' + key + '" : "' + value + '" }'));
			}
		});
	}
}

export abstract class LETSSearch {
	params: any;

	constructor(params) {
		this.params = params;
	}
}

export class LETSDetailSearch extends LETSSearch {
	sources: LETSDatasource[];

	constructor(params, sources) {
		super(params);
		this.sources = sources;
	}
}

export class LETSSmartSearch extends LETSSearch {
	sources: LETSDatasource[];

	constructor(params, sources) {
		super(params);
		this.sources = sources;
	}
}

export interface LETSResult {
	getDetails(): any;
}

export interface LETSDetails {
	detailCategory: string;
	detailFields: any;
}

export class DriverLicenseResult implements LETSResult {
	licenseNumber: string;
	socialSecurityNumber: string;
	firstName: string;
	middleName: string;
	lastName: string;
	suffix: string;
	street: string;
	state: string;
	city: string;
	zip: string;
	dateOfBirth?: string;
	race: string;
	sex: string;
	countyNumber?: number;
	height?: number;
	weight?: number;
	eyeColor: string;
	hairColor: string;
	class: string;
	creationDate?: string;
	status?: number;
	licenseIssueDate?: string;
	updateDate?: string;
	county: string;
	age?: number;

	constructor(rawResult: any) {
		this.licenseNumber = rawResult.LicenseNo;
		this.socialSecurityNumber = rawResult.SSN;
		this.firstName = rawResult.FirstName;
		this.middleName = rawResult.MiddleName;
		this.lastName = rawResult.LastName;
		this.suffix = rawResult.Suffix;
		this.street = rawResult.Street;
		this.state = rawResult.State;
		this.city = rawResult.City;
		this.zip = rawResult.Zip;
		this.dateOfBirth = rawResult.DateOfBirth || null;
		this.race = rawResult.Race;
		this.sex = rawResult.Sex;
		this.countyNumber = rawResult.Co || null;
		this.height = rawResult.Height || null;
		this.weight = rawResult.Weight || null;
		this.eyeColor = rawResult.EyeColor;
		this.hairColor = rawResult.HairColor;
		this.class = rawResult.Class;
		this.creationDate = rawResult.CreateDate || null;
		this.status = rawResult.Status || null;
		this.licenseIssueDate = rawResult.DLIssueDate || null;
		this.updateDate = rawResult.UpdateDate || null;
		this.county = rawResult.County;
		this.age = rawResult.Age || null;
	}

	getDetails(): LETSDetails[] {
		return [
			{
				detailCategory: 'License Details',
				detailFields: {
					'License #': this.licenseNumber,
					'Class': this.class,
					'Status': this.status,
					'Age': this.age,
					'Date of Birth': this.dateOfBirth ? moment(new Date(this.dateOfBirth)).format('MM/DD/YYYY') : '',
					'Name': this.firstName + ' ' + this.middleName + ' ' + this.lastName + ' ' + this.suffix,
					'Address': this.street + ' ' + this.city + ', ' + this.state + ' ' + this.zip,
					'County': this.county,
					'Date Issued': this.licenseIssueDate ? moment(new Date(this.licenseIssueDate)).format('MM/DD/YYYY') : '',
					'Sex': this.sex,
					'Race': this.race,
					'Height': this.height,
					'Weight': this.weight,
					'Eye Color': this.eyeColor,
					'Hair Color': this.hairColor,
					'SSN': this.socialSecurityNumber,
					'Creation Date': this.creationDate ? moment(new Date(this.creationDate)).format('MM/DD/YYYY') : '',
					'Update Date': this.updateDate ? moment(new Date(this.updateDate)).format('MM/DD/YYYY') : ''
				}
			}
		];
	}

	static getTestData(): DriverLicenseResult {
		return new DriverLicenseResult({
			FirstName: 'First',
			LastName: 'Last',
			MiddleName: 'Middle',
			SSN: '000000000',
			Age: '30',
			Sex: 'F',
			Height: "6'0\"",
			Weight: '135 lbs',
			LicenseNo: '1234567',
			State: 'AL'
		});
	}
}

export class DOCResult implements LETSResult {
	inmateName: string;
	firstName: string;
	middleName: string;
	lastName: string;
	race?: string;
	sex?: string;
	dateOfBirth?: string;
	fbiNumber: string;
	ssn: string;
	age?: number;
	ais: string;
	admitDate?: string;
	status: string;
	releaseDate?: string;
	releaseType: string;
	term: string;
	sidNumber: string;

	constructor(rawResult: any) {
		this.inmateName = rawResult.InmateName;
		this.firstName = rawResult.FirstName;
		this.middleName = rawResult.MiddleName;
		this.lastName = rawResult.LastName;
		this.race = rawResult.Race || null;
		this.sex = rawResult.Sex || null;
		this.dateOfBirth = rawResult.Dob || null;
		this.fbiNumber = rawResult.FBINumber;
		this.ssn = rawResult.SSN;
		this.age = rawResult.Age || null;
		this.ais = rawResult.AIS;
		this.admitDate = rawResult.AdmitDate || null;
		this.status = rawResult.Status;
		this.releaseDate = rawResult.ReleaseDate || null;
		this.releaseType = rawResult.ReleaseType;
		this.term = rawResult.Term;
		this.sidNumber = rawResult.SIDNumber;
	}

	getDetails(): LETSDetails[] {
		return [
			{
				detailCategory: 'Inmate Details',
				detailFields: {
					'Name': this.inmateName,
					'Race': this.race,
					'Sex': this.sex,
					'Age': this.age,
					'Date of Birth': this.dateOfBirth ? moment(new Date(this.dateOfBirth)).format('MM/DD/YYYY') : '',
					'FBI Number': this.fbiNumber,
					'SSN': this.ssn,
					'AIS': this.ais,
					'Admit Date': this.admitDate ? moment(new Date(this.admitDate)).format('MM/DD/YYYY') : '',
					'Status': this.status,
					'Release Date': this.releaseDate ? moment(new Date(this.releaseDate)).format('MM/DD/YYYY') : '',
					'Release Type': this.releaseType,
					'Term': this.term,
					'SID': this.sidNumber
				}
			}
		];
	}

	static getTestData(): DOCResult {
		return new DOCResult({
			FirstName: 'First',
			LastName: 'Last',
			MiddleName: 'Middle',
			SSN: '000000000',
			Age: '30',
			Dob: '01/01/1980',
			Sex: 'F',
			AdmitDate: '01/01/2000',
			ReleaseDate: '02/01/2000',
			AIS: '12345678',
			FBINumber: '123456789'
		});
	}
}

export class AlaSafeResult implements LETSResult {
	id?: number;
	firstName: string;
	lastName: string;
	middleName: string;
	nickName: string;
	suffix: string;
	address1: string;
	address2: string;
	county: string;
	city: string;
	state: string;
	zip: string;
	phone: string;
	dlNumber: string;
	dlState: string;
	ssn: string;
	sex: string;
	race: string;
	raceOther: string;
	complexion: string;
	language: string;
	hairColor: string;
	eyeColor: string;
	dob: string;
	age?: number;
	weight?: number;
	height: string;
	photoID: string;

	constructor(rawResult: any) {
		this.id = rawResult.PatientID;
		this.firstName = rawResult.FirstName;
		this.lastName = rawResult.LastName;
		this.middleName = rawResult.MiddleName;
		this.nickName = rawResult.NickName;
		this.suffix = rawResult.Suffix;
		this.address1 = rawResult.Address1;
		this.address2 = rawResult.Address2;
		this.county = rawResult.County;
		this.city = rawResult.City;
		this.state = rawResult.State;
		this.zip = rawResult.ZIP;
		this.phone = rawResult.Phone;
		this.dlNumber = rawResult.DLNumber;
		this.dlState = rawResult.DLState;
		this.ssn = rawResult.SSN;
		this.sex = rawResult.Sex;
		this.race = rawResult.Race;
		this.raceOther = null;
		this.complexion = rawResult.Complexion;
		this.language = rawResult.PrimaryLanguage;
		this.hairColor = rawResult.HairColor;
		this.eyeColor = rawResult.EyeColor;
		this.dob = rawResult.DOB;
		this.age = Utilities.getAgeFromDOB(rawResult.DOB);
		this.weight = rawResult.Weight;
		this.height = rawResult.Height;
		this.photoID = rawResult.PhotoID;
	}

	getDetails(): LETSDetails[] {
		return [
			{
				detailCategory: 'AlaSafe Details',
				detailFields: {
					'Name': this.firstName + ' ' + this.middleName + ' ' + this.lastName + ' ' + this.suffix,
					'Nickname': this.nickName,
					'Address': this.address1 + ' ' + this.address2 + ' ' + this.city + ', ' + this.state + ' ' + this.zip,
					'County': this.county,
					'Phone #': this.phone,
					'License #': this.dlNumber,
					'License State': this.dlState,
					'SSN': this.ssn,
					'Sex': this.sex,
					'Race': this.race,
					'Complexion': this.complexion,
					'Primary Language': this.language,
					'Hair Color': this.hairColor,
					'Eye Color': this.eyeColor,
					'Date of Birth': moment(new Date(this.dob)).format('MM/DD/YYYY'),
					'Age': this.age,
					'Height': this.height,
					'Weight': this.weight
				}
			}
		];
	}

	static getTestData(): AlaSafeResult {
		return new AlaSafeResult({
			FirstName: 'First',
			LastName: 'Last',
			MiddleName: 'Middle',
			SSN: '000000000',
			DOB: '01/01/1980',
			Sex: 'F',
			Race: 'C'
		});
	}
}

export class BoatResult implements LETSResult {
	regNumber: string;
	typeRecord: string;
	decal: string;
	boatClass: string;
	boatLength?: number;
	ownerName: string;
	ownerLastName: string;
	ownerFirstName: string;
	address1: string;
	address2: string;
	cityState: string;
	zipCode: string;
	boatHullType: string;
	boatPropulsion: string;
	boatUsage: string;
	boatFuelType: string;
	cabinType: string;
	addressChange: string;
	dateIssued?: string;
	boatMake: string;
	boatYear?: number;
	hullSerial: string;
	boatMotorMake: string;
	motorSerial: string;
	engineHorsePower: string;
	countyRegistered: string;
	ownerDob?: string;
	dateExpire?: string;
	renewalDate?: string;
	registrationDate?: string;

	constructor(rawResult: any) {
		this.regNumber = rawResult.RegNumber;
		// this.typeRecord = rawResult.TypeRecord;
		this.decal = rawResult.Decal;
		this.boatClass = rawResult.BoatClass;
		this.boatLength = rawResult.BoatLength || null;
		this.ownerName = rawResult.OwnerName;
		this.ownerLastName = rawResult.OwnerLastName;
		this.ownerFirstName = rawResult.OwnerFirstName;
		this.address1 = rawResult.Address1;
		this.address2 = rawResult.Address2;
		this.cityState = rawResult.CityState;
		this.zipCode = rawResult.ZipCode;
		this.boatHullType = rawResult.BoatHullType;
		this.boatPropulsion = rawResult.BoatPropulsion;
		this.boatUsage = rawResult.BoatUsage;
		this.boatFuelType = rawResult.BoatFuelType;
		this.cabinType = rawResult.CabinType;
		this.dateIssued = rawResult.DateIssued || null;
		this.boatMake = rawResult.BoatMake;
		this.boatYear = rawResult.BoatYear || null;
		this.hullSerial = rawResult.HullSerial;
		this.boatMotorMake = rawResult.BoatMotorMake;
		this.motorSerial = rawResult.MotorSerial;
		this.engineHorsePower = rawResult.EngineHorsePower;
		this.countyRegistered = rawResult.CountyRegistered;
		this.ownerDob = rawResult.OwnerDOB || null;
		this.dateExpire = rawResult.DateExpire || null;
		this.renewalDate = rawResult.RenewalDate || null;
		this.registrationDate = rawResult.RegistrationDate || null;
	}

	getDetails(): LETSDetails[] {
		return [
			{
				detailCategory: 'Boat Details',
				detailFields: {
					'Registration #': this.regNumber,
					'Decal': this.decal,
					'Boat Class': this.boatClass,
					'Boat Length': this.boatLength,
					'Owner Name': this.ownerName,
					'Address': this.address1 + ' ' + this.address2 + ' ' + this.cityState + ' ' + this.zipCode,
					'Hull Type': this.boatHullType,
					'Propulsion': this.boatPropulsion,
					'Usage': this.boatUsage,
					'Fuel Type': this.boatFuelType,
					'Cabin Type': this.cabinType,
					'Date Issued': this.dateIssued ? moment(new Date(this.dateIssued)).format('MM/DD/YYYY') : '',
					'Make': this.boatMake,
					'Year': this.boatYear,
					'Hull Serial #': this.hullSerial,
					'Motor Make': this.boatMotorMake,
					'Motor Serial #': this.motorSerial,
					'Horsepower': this.engineHorsePower,
					'Registration County': this.countyRegistered,
					'Owner Date of Birth': this.ownerDob ? moment(new Date(this.ownerDob)).format('MM/DD/YYYY') : '',
					'Expiration Date': this.dateExpire ? moment(new Date(this.dateExpire)).format('MM/DD/YYYY') : '',
					'Renewal Date': this.renewalDate ? moment(new Date(this.renewalDate)).format('MM/DD/YYYY') : '',
					'Registration Date': this.registrationDate ? moment(new Date(this.registrationDate)).format('MM/DD/YYYY') : '',
				}
			}
		];
	}

	static getTestData(): BoatResult {
		return new BoatResult({
			OwnerName: 'Owner Name',
			RegNumber: '123456789',
			BoatYear: 1999,
			BoatMake: 'Brand',
			BoatClass: 'Class',
			BoatLength: 25
		});
	}
}

export class NameMasterResult implements LETSResult {
	id?: number;
	nameId: string;
	firstName: string;
	middleName: string;
	lastName: string;
	suffix: string;
	race: string;
	sex: string;
	dateOfBirth: string;
	age: number;
	socialSecurityNumber: string;
	licenseNumber: string;
	address: string;
	found: string;
	dais?: boolean;
	dawc?: boolean;
	darrt?: boolean;
	midas?: boolean;
	juv?: boolean;
	reason: string;
	merge?: boolean;

	constructor(rawResult: any) {
		this.id = rawResult.ID || null;
		this.nameId = rawResult.NameID;
		this.firstName = rawResult.FirstName;
		this.middleName = rawResult.MiddleName;
		this.lastName = rawResult.LastName;
		this.suffix = rawResult.SuffixName;
		this.race = rawResult.Race;
		this.sex = rawResult.Sex;
		this.dateOfBirth = rawResult.DOB;
		this.age = Utilities.getAgeFromDOB(rawResult.DOB);
		this.socialSecurityNumber = rawResult.SSN;
		this.licenseNumber = rawResult.DL;
		this.address = rawResult.Address;
		this.found = '';
		this.dais = rawResult.DAIS || null;
		this.darrt = rawResult.DARRT || null;
		this.midas = rawResult.MIDAS || null;
		this.juv = rawResult.JUV || null;
		this.reason = rawResult.Reason;
		this.merge = rawResult.Merge || null;
	}

	getDetails(): LETSDetails[] {
		return [
			{
				detailCategory: 'NameMaster Details',
				detailFields: {
					'Name': this.firstName + ' ' + this.middleName + ' ' + this.lastName + ' ' + this.suffix,
					'Race': this.race,
					'Sex': this.sex,
					'Age': this.age,
					'Date of Birth': moment(new Date(this.dateOfBirth)).format('MM/DD/YYYY'),
					'SSN': this.socialSecurityNumber,
					'License #': this.licenseNumber,
					'Address': this.address,
					'DAIS': this.dais,
					'DARRT': this.darrt,
					'MIDAS': this.midas,
					'JUV': this.juv,
					'Reason': this.reason,
					'Merge': this.merge
				}
			}
		];
	}

	static getTestData(): NameMasterResult {
		return new NameMasterResult({
			FirstName: 'First',
			MiddleName: 'Middle',
			LastName: 'Last',
			DL: '1234567',
			Race: 'A',
			Sex: 'M',
			DOB: '1985-10-06'
		});
	}
}

export class ProtectionOrderResult implements LETSResult {
	sessionKey: string;
	county: string;
	dateIssued: string;
	dateServed: string;
	defendant: string;
	plaintiff: string;
	expireDate: string;
	expireType: string;

	constructor(rawResult: any) {
		this.sessionKey = rawResult.SessionKey;
		this.county = rawResult.DV_COUNTY;
		this.dateIssued = rawResult.DV_ISSUE_DATE;
		this.dateServed = rawResult.DV_ISSUE_DATE;
		this.defendant = rawResult.DV_DEFEND_NAME;
		this.plaintiff = rawResult.DV_PLAINT_NAME;
		this.expireDate = rawResult.DV_EXPIRE_DATE;
		this.expireType = rawResult.DV_EXPIRE_TYPE;
	}

	getDetails(): LETSDetails[] {
		return [
			{
				detailCategory: 'Protection Order Details',
				detailFields: {
					'County': this.county,					
					'Date Issued': moment(new Date(this.dateIssued)).format('MM/DD/YYYY'),
					'Date Served': moment(new Date(this.dateServed)).format('MM/DD/YYYY'),
					'Defendant Name': this.defendant,
					'Plaintiff Name': this.plaintiff,
					'Expiration Date': moment(new Date(this.expireDate)).format('MM/DD/YYYY'),
					'Expiration Type': this.expireType
				}
			}
		];
	}

	static getTestData(): ProtectionOrderResult {
		return new ProtectionOrderResult({
			DV_DEFEND_NAME: 'Last, First Middle',
			DV_COUNTY: 'County',
			DV_ISSUE_DATE: '1985-10-06',
			DV_PLAINT_NAME: 'Plaintiff Name',
			DV_EXPIRE_DATE: '1990-01-01',
			DV_EXPIRE_TYPE: 'Expire Type'
		});
	}
}

export class SexOffenderResult implements LETSResult {
	offenderHeaderID: string;
	name: string;
	offense: string;
	street: string;
	city: string;
	state: string;
	zip: string;
	county: string;
	race: string;
	sex: string;
	regDate: string;
	victimAge?: number;
	victimSex: string;

	constructor(rawResult: any) {
		this.offenderHeaderID = rawResult.OffenderHeaderID;
		this.name = rawResult.NAME;
		this.offense = rawResult.OFFENSE_DESCRIPTION;
		this.street = rawResult.STREET;
		this.city = rawResult.CITY;
		this.state = rawResult.STATE;
		this.zip = rawResult.ZIP;
		this.county = rawResult.COUNTY;
		this.race = rawResult.RACE;
		this.sex = rawResult.SEX;
		this.victimAge = rawResult.VICTIM_AGE || null;
		this.victimSex = rawResult.VICTIM_SEX;
		this.regDate = rawResult.ORIGINAL_REGISTRATION_DATE;
	}

	getDetails(): LETSDetails[] {
		return [
			{
				detailCategory: 'Sex Offender Details',
				detailFields: {
					'Name': this.name,
					'Offense': this.offense,
					'Address': this.street + ' ' + this.city + ', ' + this.state + ' ' + this.zip,
					'County': this.county,
					'Race': this.race,
					'Sex': this.sex,
					'Registration Date': moment(new Date(this.regDate)).format('MM/DD/YYYY'),
					'Victim Age': this.victimAge,
					'Victim Sex': this.victimSex
				}
			}
		];
	}

	static getTestData(): SexOffenderResult {
		return new SexOffenderResult({
			NAME: 'Last, First Middle',
			COUNTY: 'County',
			RACE: 'W',
			SEX: 'M',
			VICTIM_AGE: 35,
			VICTIM_SEX: 'F'
		});
	}
}

export class HotfileWarrantResult implements LETSResult {
	sessionKey: string;
	idx: string;
	name: string;
	sid: string;
	dob: string;
	race: string;
	sex: string;
	warrantDate: string;
	description: string;

	constructor(rawResult: any) {
		this.sessionKey = rawResult.SessionKey;
		this.idx = rawResult.IDX;
		this.name = rawResult.Name;
		this.sid = rawResult.SID;
		this.dob = rawResult.DOB;
		this.race = rawResult.Race;
		this.sex = rawResult.Sex;
		this.warrantDate = rawResult.WarrantDate;
		this.description = rawResult.Description;
	}

	getDetails(): LETSDetails[] {
		return [
			{
				detailCategory: 'Warrant Details',
				detailFields: {
					'IDX': this.idx,
					'Name': this.name,
					'SID': this.sid,
					'Date of Birth': moment(new Date(this.dob)).format('MM/DD/YYYY'),
					'Race': this.race,
					'Sex': this.sex,
					'Warrant Date': moment(new Date(this.warrantDate)).format('MM/DD/YYYY'),
					'Description': this.description
				}
			}
		];
	}

	static getTestData(): HotfileWarrantResult {
		return new HotfileWarrantResult({
			Name: 'Last, First Middle',
			SID: '0123456789',
			DOB: '1985-10-06',
			Race: 'W',
			Sex: 'M',
			WarrantDate: '1999-01-02',
			Description: 'Test description'
		});
	}
}

export class TagResult implements LETSResult {
	sessionKey: string;
	tagNumber: string;
	registrant: string;
	licenseNumber: string;
	tagIssuedDate: string;
	tagExpirationDate: string;
	vehicle: string;
	vin: string;
	city: string;
	state: string;
	zip: string;
	county: string;
	expired: string;
	primaryColor: string;
	secondaryColor: string;

	constructor(rawResult: any) {
		this.sessionKey = rawResult.SessionKey;
		this.tagNumber = rawResult.TagNumber;
		this.registrant = rawResult.Registrant;
		this.licenseNumber = rawResult.DriversLicenseNumber;
		this.tagIssuedDate = rawResult.TagIssuedDate;
		this.tagExpirationDate = rawResult.TagExpirationDate;
		this.vehicle = rawResult.Vehicle;
		this.vin = rawResult.VIN;
		this.city = rawResult.City;
		this.state = rawResult.State;
		this.zip = rawResult.ZipCode;
		this.county = rawResult.County;
		this.expired = moment(rawResult.TagExpirationDate).isBefore(moment()) ? 'EXPIRED' : 'CURRENT';
		this.primaryColor = rawResult.PrimaryColor;
		this.secondaryColor = rawResult.SecondaryColor;
	}

	getDetails(): LETSDetails[] {
		return [
			{
				detailCategory: 'Tag Registration Details',
				detailFields: {
					'Tag #': this.tagNumber,
					'Registrant': this.registrant,
					'Lic #': this.licenseNumber,
					'Date Issued': moment(new Date(this.tagIssuedDate)).format('MM/DD/YYYY'),
					'Expiration Date': moment(new Date(this.tagExpirationDate)).format('MM/DD/YYYY'),
					'Vehicle': this.vehicle,
					'VIN': this.vin,
					'Address': this.city + ', ' + this.state + ' ' + this.zip,
					'County': this.county,
					'Status': this.expired,
					'Primary Color': this.primaryColor,
					'Secondary Color': this.secondaryColor
				}
			}
		];
	}

	static getTestData(): TagResult {
		return new TagResult({
			SessionKey: '',
			TagNumber: 'abc1234',
			Registrant: 'Last, First Middle',
			DriversLicenseNumber: '9888888',
			TagIssuedDate: '2005-06-07',
			TagExpirationDate: '2006-06-30',
			PrimaryColor: 'SIL',
			City: 'Birmingham',
			State: 'AL',
			ZipCode: '35223',
			County: 'Jefferson'
		});
	}
}

export class CitationResult implements LETSResult {
	ticketNum: string;
	dateIssued: string;
	docStatus: number;
	description: string;
	firstName: string;
	middleName: string;
	lastName: string;
	licNum: string;
	licState: string;
	ticketCounty: string;
	ticketCity: string;
	tagNum: string;
	tagState: string;

	constructor(rawResult: any) {
		this.ticketNum = rawResult.TicketNum;
		this.dateIssued = rawResult.DateIssued;
		this.docStatus = rawResult.DocStatus;
		this.description = rawResult.Description;
		this.firstName = rawResult.FirstName;
		this.middleName = rawResult.MiddleName;
		this.lastName = rawResult.LastName;
		this.licNum = rawResult.LicNum;
		this.licState = rawResult.LicState;
		this.ticketCounty = rawResult.TicketCounty;
		this.ticketCity = rawResult.TicketCity;
		this.tagNum = rawResult.TagNum;
		this.tagState = rawResult.TagState;
	}

	getDetails(): LETSDetails[] {
		return [
			{
				detailCategory: 'Citation Details',
				detailFields: {
					'Ticket #': this.ticketNum,
					'Date Issued': moment(new Date(this.dateIssued)).format('MM/DD/YYYY'),
					'Doc Status': this.docStatus,
					'Description': this.description,
					'Ticket County': this.ticketCounty,
					'Ticket City': this.ticketCity
				}
			},
			{
				detailCategory: 'Driver Details',
				detailFields: {
					'First Name': this.firstName,
					'Middle Name': this.middleName,
					'Last Name': this.lastName,
					'License #': this.licNum,
					'License State': this.licState,
					'Tag #': this.tagNum,
					'Tag State': this.tagState
				}
			}
		];
	}

	static getTestData(): CitationResult {
		return new CitationResult({
			TicketNum: '987654321',
			DateIssued: '2001-02-05',
			DocStatus: 36,
			Description: 'TEST',
			TicketCounty: 'County name',
			TicketCity: 'City name',
			TagNum: 'abc123',
			TagState: 'AL',
			FirstName: 'First',
			MiddleName: 'Middle',
			LastName: 'Last',
			LicNum: '9888888',
			LicState: 'AL'
		});
	}
}

export class Utilities {
	static getAgeFromDOB(dob: string) {
		let today = moment();
		let formattedDOB = moment(dob);
		return _.floor(today.diff(formattedDOB, 'years', true));
	}
}