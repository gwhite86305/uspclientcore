import { CommonModule } from '@angular/common';
import { NgModule, ModuleWithProviders, APP_INITIALIZER } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { SimpleNotificationsModule, NotificationsService } from 'angular2-notifications';
import { TagInputModule } from 'ngx-chips';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { CoreModule } from './core.module';
import { SearchBuilder } from './components/search-builder/search-builder.component';
import { CitationOverview } from './components/citation-overview/citation-overview.component';
import { ResultsPrintButton } from './components/results-print-button/results-print-button.component';
import { SearchTermBox } from './components/search-term-box/search-term-box.component';
import { ThreatBar } from './components/threat-bar/threat-bar.component';
import { QueryTab } from './components/query-tab/query-tab.component';
import { ResultTab } from './components/result-tab/result-tab.component';
import { CarrierOverview } from './components/carrier-overview/carrier-overview.component';
import { NgxResizeWatcherDirective } from './components/ngx-resize-watcher/ngx-resize-watcher.directive';
import { HistoryOverview } from './components/history-overview/history-overview.component';
import { CitationDetail } from './components/citation-detail/citation-detail.component';
import { CarrierDetail } from './components/carrier-detail/carrier-detail.component';
import { BasicInfoCard } from './components/carrier-detail/basic-info-card/basic-info-card.component';
import { CarrierInfoCard } from './components/carrier-detail/carrier-info-card/carrier-info-card.component';
import { GimcInfoCard } from './components/carrier-detail/gimc-info-card/gimc-info-card.component';
import { HhgInfoCard } from './components/carrier-detail/hhg-info-card/hhg-info-card.component';
import { PassengerInfoCard } from './components/carrier-detail/passenger-info-card/passenger-info-card.component';
import { UcrInfoCard } from './components/carrier-detail/ucr-info-card/ucr-info-card.component';
import { UcrDetailsInfoCard } from './components/carrier-detail/ucr-details-info-card/ucr-details-info-card.component';
// import { ApiStatusNotifier } from './components/network-notifier/api-status-notifier/api-status-notifier.component';
// import { NetworkNotifier } from './components/network-notifier/network-notifier.component';
// import { HistoryCard } from './components/history-card/history-card.component';
import { OverviewDetail } from './components/overview-detail/overview-detail.component';
import { PersonDetail } from './components/person-detail/person-detail.component';
import { RawResponseCard } from './components/raw-response-card/raw-response-card.component';
import { ResultTotalCard } from './components/result-total-card/result-total-card.component';
import { VehicleDetail } from './components/vehicle-detail/vehicle-detail.component';
import { DetailCard } from './components/detail-card/detail-card.component';
import { OverviewCard } from './components/overview-card/overview-card.component';
import { LicenseCard } from './components/license-card/license-card.component';
import { DOCCard } from './components/doc-card/doc-card.component';
import { AlaSafeCard } from './components/alasafe-card/alasafe-card.component';
import { BoatCard } from './components/boat-card/boat-card.component';
import { NameMasterCard } from './components/namemaster-card/namemaster-card.component';
import { ProtectionOrderCard } from './components/protection-order-card/protection-order-card.component';
import { SexOffenderCard } from './components/sex-offender-card/sex-offender-card.component';
import { WarrantCard } from './components/warrant-card/warrant-card.component';
import { DetailSearchCard } from './components/detail-search-card/detail-search-card.component';
import { RecentSearchCard } from './components/recent-search-card/recent-search-card.component';
import { CitationCard } from './components/citation-card/citation-card.component';
import { SmartSearchBar } from './components/smart-search-bar/smart-search-bar.component';
import { GenericFormBuilder } from './components/generic-form-builder/generic-form-builder.component';
import { DetailSearchDialog } from './components/detail-search-dialog/detail-search-dialog.component';
import { DetailSearchSlider } from'./components/detail-search-slider/detail-search-slider.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { FormatTransferStatusPipe } from './pipes/citation-format.pipes';
import { ResultsSidebarComponent } from './components/results-sidebar/results-sidebar.component';
// import { NetworkStatusService } from './services/network-status.service';

import { WebStorageModule } from 'ngx-store';

let declarations = [
	SearchBuilder,
	CitationOverview,
	ResultsPrintButton,
	SearchTermBox,
	ThreatBar,
	QueryTab,
	ResultTab,
	CarrierOverview,
	NgxResizeWatcherDirective,
	HistoryOverview,
	CitationDetail,
	CarrierDetail,
	BasicInfoCard,
	CarrierInfoCard,
	GimcInfoCard,
	HhgInfoCard,
	PassengerInfoCard,
	UcrInfoCard,
	UcrDetailsInfoCard,
	// ApiStatusNotifier,
	// NetworkNotifier,
	// HistoryCard,
	OverviewDetail,
	PersonDetail,
	RawResponseCard,
	ResultTotalCard,
	VehicleDetail,
	FormatTransferStatusPipe,
	ResultsSidebarComponent,		
	DetailCard,
	OverviewCard,
	LicenseCard,
	DOCCard,
	AlaSafeCard,
	BoatCard,
	NameMasterCard,
	ProtectionOrderCard,
	SexOffenderCard,
	WarrantCard,
	DetailSearchCard,
	RecentSearchCard,
	CitationCard,
	SmartSearchBar,
	GenericFormBuilder,
	DetailSearchDialog,
	DetailSearchSlider
];

// @dynamic
@NgModule({
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		RouterModule,
		CoreModule,
		ButtonsModule,
		BrowserAnimationsModule,
		TagInputModule,
		ModalModule,
		NgxDatatableModule,
		WebStorageModule,
		NgMultiSelectDropDownModule.forRoot(),
		SimpleNotificationsModule,
		PopoverModule.forRoot(),
		CollapseModule.forRoot(),
		BsDatepickerModule.forRoot()
	],
	providers: [],
	declarations: declarations,
	exports: declarations
})
export class SearchModule { }
