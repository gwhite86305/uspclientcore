import { NgModule, APP_INITIALIZER } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

import { CollapseModule } from 'ngx-bootstrap/collapse';
import { PopoverModule } from 'ngx-bootstrap/popover';

import { AccordionToggleButton } from './components/accordion-toggle-button/accordion-toggle-button.component';
import { CollapseTable } from './components/collapse-table/collapse-table.component';
import { DayNightToggle } from './components/day-night-toggle/day-night-toggle.component';
import { HighlightSearch } from './components/highlight-search/highlight-search.component';
import { LoadingAnimation } from './components/loading-animation/loading-animation.component';
import { NavHeader } from './components/nav-header/nav-header.component';
import { NavHeaderTemplate } from './components/nav-header-template/nav-header-template.component';
import { NpmBadge } from './components/npm-badge/npm-badge.component';
import { PrivacyScreen } from './components/privacy-screen/privacy-screen.component';
import { ReferenceDropdown } from './components/reference-dropdown/reference-dropdown.component';
import { RegistrationCard } from './components/registration-card/registration-card.component';
import { ValidateCheckmark } from './components/validate-checkmark/validate-checkmark.component';
import { ReleaseNotes } from './components/release-notes/release-notes.component';
import { SettingsMenu } from './components/settings-menu/settings-menu.component';
import { TourButton } from './components/tour-button/tour-button.component';
import { ValidationLabel } from './components/validation-label/validation-label.component';
import { ThemePicker } from './components/theme-picker/theme-picker.component';
// import { MenuToggle } from './components/menu-toggle/menu-toggle.component';
import { CollapsibleCardDeck } from './components/collapsible-card-deck/collapsible-card-deck.component';
import { NavSidebarTemplate } from './components/nav-sidebar-template/nav-sidebar-template.component';
import { HomeNav } from './components/home-nav/home-nav.component';
import { SearchBarToggle } from './components/search-bar-toggle/search-bar-toggle.component';

import { ConfigService } from './services/config.service';
import { ThemeService } from './services/theme.service';
import { StatusModelService } from './services/status-model.service';
import { TourService } from './services/tour.service';

import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';

import { FormatIfDatePipe, LocalDateTimeFromSQLPipe, LocalDateFromSQLPipe } from './pipes/date-format.pipes';

let declarations = [
	AccordionToggleButton,
	CollapseTable,
	DayNightToggle,
	HighlightSearch,
	LoadingAnimation,
	NavHeader,
	NavHeaderTemplate,
	NpmBadge,
	PrivacyScreen,
	ReferenceDropdown,
	RegistrationCard,
	ValidateCheckmark,
	ReleaseNotes,
	SettingsMenu,
	TourButton,
	ValidationLabel,
	ThemePicker,
	// MenuToggle,
	CollapsibleCardDeck,
	NavSidebarTemplate,
	HomeNav,
	SearchBarToggle,

	FormatIfDatePipe,
	LocalDateTimeFromSQLPipe,
	LocalDateFromSQLPipe
];

// @dynamic
@NgModule({
	imports: [
		CommonModule,
		HttpClientModule,
		RouterModule,
		BrowserAnimationsModule,
		FormsModule,
		NgMultiSelectDropDownModule.forRoot(),
		CollapseModule.forRoot(),
		PopoverModule.forRoot()
	],
	providers: [
		ConfigService,
		ThemeService,
		StatusModelService,
		TourService,
		{ provide: 
			APP_INITIALIZER, 
			useFactory: appInitializer, 
			deps: [
				ThemeService, 
				StatusModelService, 
				TourService, 
				ConfigService],
			multi: true },
	],
	declarations: declarations,
	exports: declarations
})
export class CoreModule { }

export function appInitializer() {
	return () => new Promise((resolve, reject) => {
		//Our config data must be loaded immediately so our APIs know which URLs to hit.
		ConfigService.load().then((configData) => {
			ThemeService.setup(configData.themeConfig);
			StatusModelService.setup(configData.statusModelConfig);
			TourService.setup(configData.ApplicationHeader, configData.tourConfig);
				
			resolve();
		});
	});
}
