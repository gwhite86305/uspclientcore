import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DriverLicenseResult, OverviewCardField, LETSResult } from '../../search-types';
import { Observable } from 'rxjs';
import * as momentNs from 'moment';
const moment = momentNs;
const _ = require('lodash');

@Component({
	selector: 'license-card',
	templateUrl: './license-card.component.html',
	styleUrls: ['./license-card.component.css']
})
export class LicenseCard implements OnInit {
	@Input()
	licenseData: DriverLicenseResult = DriverLicenseResult.getTestData();
	@Output()
	onShowDetails: EventEmitter<LETSResult> = new EventEmitter<LETSResult>();

	cardFields: OverviewCardField[];
	photoObservable: Observable<string> = Observable.create(observer => {
		observer.next('./assets/images/empty photo.jpg');
	});
	headerText: string;
	subHeaderText: string;

	constructor() { }

	ngOnInit() {
		//Format header and subheader fields
		this.headerText = this.licenseData.lastName + ', ' + this.licenseData.firstName + ' ' + this.licenseData.middleName;
		this.subHeaderText = 'LIC #' + this.licenseData.licenseNumber;

		//Format input fields
		let fieldArray = [];
		fieldArray.push({ fieldKey: 'Age', fieldValue: this.licenseData.age });
		fieldArray.push({ fieldKey: 'Sex', fieldValue: this.licenseData.sex });
		fieldArray.push({ fieldKey: 'Height', fieldValue: this.licenseData.height });
		fieldArray.push({ fieldKey: 'Weight', fieldValue: this.licenseData.weight });
		this.cardFields = fieldArray;
	}

	showDetails() {
		this.onShowDetails.emit(this.licenseData);
	}
}
