import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AlaSafeResult, OverviewCardField, LETSResult } from '../../search-types';
import { Observable } from 'rxjs';
import * as momentNs from 'moment';
const moment = momentNs;
const _ = require('lodash');

@Component({
  selector: 'alasafe-card',
  templateUrl: './alasafe-card.component.html',
  styleUrls: ['./alasafe-card.component.css']
})
export class AlaSafeCard implements OnInit {
	@Input()
	alaSafeData: AlaSafeResult = AlaSafeResult.getTestData();
	@Output()
	onShowDetails: EventEmitter<LETSResult> = new EventEmitter<LETSResult>();

	cardFields: OverviewCardField[];
	photoObservable: Observable<string> = Observable.create(observer => {
		observer.next('./assets/images/empty photo.jpg');
	});
	headerText: string;
	subHeaderText: string;

	constructor() { }

	ngOnInit() {
		//Format header and subheader fields
		this.headerText = this.alaSafeData.lastName + ',' + ' ' + this.alaSafeData.firstName + ' ' + this.alaSafeData.middleName;
		this.subHeaderText = 'SSN: ' + this.alaSafeData.ssn;

		//Format input fields
		let fieldArray = [];
		fieldArray.push({ fieldKey: 'Date of Birth', fieldValue: moment(new Date(this.alaSafeData.dob)).format('MM/DD/YYYY') });
		fieldArray.push({ fieldKey: 'Age', fieldValue: this.alaSafeData.age });
		fieldArray.push({ fieldKey: 'Race', fieldValue: this.alaSafeData.race });
		fieldArray.push({ fieldKey: 'Sex', fieldValue: this.alaSafeData.sex });
		this.cardFields = fieldArray;
	}

	showDetails() {
		this.onShowDetails.emit(this.alaSafeData);
	}

}
