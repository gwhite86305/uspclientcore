import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlasafeCardComponent } from './alasafe-card.component';

describe('AlasafeCardComponent', () => {
  let component: AlasafeCardComponent;
  let fixture: ComponentFixture<AlasafeCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AlasafeCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlasafeCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
