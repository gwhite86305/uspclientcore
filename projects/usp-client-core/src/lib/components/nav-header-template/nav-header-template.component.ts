import { Component, Input, Output, EventEmitter } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

// export interface NavItem {
// 	popoverText: string,
// 	navId: string,
// 	icon: string,
// 	navText: string,
// 	route: string,
// 	isProfile: boolean,
// 	isLogout: boolean
// }

@Component({
	selector: "nav-header-template",
	templateUrl: './nav-header-template.view.html',
	styleUrls: ['./nav-header-template.styles.css']
})
export class NavHeaderTemplate {
	@Input()
	id: string = '';
	@Input()
	defaultRoute: string;
	@Input()
	applicationHeader: string = '';
	@Input()
	version: string;

	constructor(private http: HttpClient) {

	}
}