import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CitationResult, OverviewCardField, LETSResult } from '../../search-types';
import { Observable } from 'rxjs';
import * as momentNs from 'moment';
const moment = momentNs;
const _ = require('lodash');

@Component({
  selector: 'citation-card',
  templateUrl: './citation-card.component.html',
  styleUrls: ['./citation-card.component.css']
})
export class CitationCard implements OnInit {
	@Input()
	citationData: CitationResult = CitationResult.getTestData();
	@Output()
	onShowDetails: EventEmitter<LETSResult> = new EventEmitter<LETSResult>();

	cardFields: OverviewCardField[];
	photoObservable: Observable<string> = Observable.create(observer => {
		observer.next('./assets/images/empty photo.jpg');
	});
	headerText: string;
	subHeaderText: string;

	constructor() { }

	ngOnInit() {
		//Format header and subheader fields
		this.headerText = this.citationData.lastName + ',' + ' ' + this.citationData.firstName + ' ' + this.citationData.middleName;
		this.subHeaderText = this.citationData.licState + ' LIC #' + this.citationData.licNum;

		//Format input fields
		let fieldArray = [];
		fieldArray.push({ fieldKey: 'Date Issued', fieldValue: moment(new Date(this.citationData.dateIssued)).format('MM/DD/YYYY') });
		fieldArray.push({ fieldKey: 'Tag #', fieldValue: this.citationData.tagNum });
		fieldArray.push({ fieldKey: 'Ticket City', fieldValue: this.citationData.ticketCity });
		fieldArray.push({ fieldKey: 'Tag State', fieldValue: this.citationData.tagState });		
		this.cardFields = fieldArray;
	}

	showDetails() {
		this.onShowDetails.emit(this.citationData);
	}
}
