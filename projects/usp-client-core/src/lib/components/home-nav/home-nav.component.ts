import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: '[home-nav]',
	templateUrl: './home-nav.component.html',
	styleUrls: ['./home-nav.component.css']
})
export class HomeNav implements OnInit {
	@Input()
	popoverPosition: string = 'bottom';

	constructor() { }

	ngOnInit() {
	}
}
