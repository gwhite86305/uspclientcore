import { Component, Input, OnInit, DoCheck, IterableDiffers } from '@angular/core';
import { SocketResponse } from '../../search-types';
const _ = require('lodash');

/**
 * 
 * @param
 */
@Component({
	selector: "raw-response-card",
	templateUrl: './raw-response-card.view.html',
})
export class RawResponseCard implements OnInit, DoCheck {
	@Input()
	responses: SocketResponse[];

	rawData: string = '';
	diffChecker: any;

	constructor(differs: IterableDiffers) {
		this.diffChecker = differs.find([]).create(null);
	}

	ngOnInit() {
		_.forEach(this.responses, (res: SocketResponse) => {
			this.rawData += res.responseFullMessage.replace(/\\r\\n/g, '\r\n');
		});
	}

	ngDoCheck() {
		let changes = this.diffChecker.diff(this.responses);
		if (changes) {
			_.forEach(this.responses, (res: SocketResponse) => {
				this.rawData += res.responseFullMessage.replace(/\\r\\n/g, '\r\n');
			});
		}
	}
}