import { Component, Input, EventEmitter, OnInit } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';
import * as momentNs from 'moment';
const moment = momentNs;
const _ = require('lodash');

import { SocketResponse } from '../../search-types';

/**
 * PersonDetail component -- Shows the detailed data for a selected person.
 * @param model (model) The PersonModel object containing the person details.
 */
@Component({
	selector: "person-detail",
	templateUrl: './person-detail.view.html',
	styleUrls: ['./person-detail.styles.css'],
})
export class PersonDetail implements OnInit {
	@Input()
	responses: SocketResponse[];
	@Input()
	model: SocketResponse;

	tableExpandEmitter: EventEmitter<any> = new EventEmitter();
	tableToggleEmitter: EventEmitter<any> = new EventEmitter();
	headerText: string = '';
	cardOpenStatus: boolean = true;
	isPanelStatusIconRequired: boolean = false;

	constructor(private notifService: NotificationsService) {

	}

	ngOnInit() {
		this.headerText = 'Results for :: ';
		_.forEach(this.model.query.fields, (field) => {
			this.headerText += '{ ' + field.name + ': ' + field.value + ' }, ';
		});
	}

	getHeightString() {
		return this.parseHeightString(this.model.person.heightFeet, this.model.person.heightInches);
	}

	parseHeightString(feet, inches) {
		if (feet && feet != 'N/A') {
			inches = (inches && inches != 'N/A') ? inches : 0;
			return feet + '\' ' + inches + '"';
		}
		else
			return 'N/A';
	}

	getSourcesForField(fieldName) {
		let sources = this.responses;
		return _.reduce(sources, (results: any[], source: SocketResponse) => {
			let sourceKeyVal = {
				name: source.responseSource,
				value: eval('source.person.' + fieldName)
			};
			results.push(sourceKeyVal);
			return results;
		}, []);
	}

	getSourcesForHeightField() {
		let sources = this.responses;
		return _.reduce(sources, (results: any[], source: SocketResponse) => {
			let sourceKeyVal = {
				name: source.responseSource,
				value: this.parseHeightString(source.person.heightFeet, source.person.heightInches)
			};
			results.push(sourceKeyVal);
			return results;
		}, []);
	}

	toggleSourceTables() {
		this.tableToggleEmitter.emit();
	}

	openAllCards(cards: any[]) {
		this.tableExpandEmitter.emit();

		_.forEach(cards, (card) => {
			card.open();
		});
		this.notifService.remove();
	}
}
