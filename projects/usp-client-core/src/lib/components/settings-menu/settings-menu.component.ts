import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
const $ = require('jquery');

@Component({ 
	selector: '[settings-menu]',
	templateUrl: './settings-menu.view.html',
})
export class SettingsMenu implements OnInit {
	@Input()
	hideExport: boolean = false;
	@Input()
	hideReleaseNotes: boolean = false;
	@Output()
	onFontSizeChanged: EventEmitter<number> = new EventEmitter<number>();
	@Output()
	onExportLogs: EventEmitter<any> = new EventEmitter<any>();
	@Output()
	onShowReleaseNotes: EventEmitter<any> = new EventEmitter<any>();

	fontSlider: number;
	volumeSlider: number;

    constructor() {

	}

	ngOnInit() {
		let cachedFontSize = localStorage.getItem('AtlasFontSize');
		this.fontSlider = cachedFontSize ? Number(cachedFontSize) : 16;
		$('body').css('font-size', this.fontSlider + 'px');
		this.onFontSizeChanged.emit(this.fontSlider);

		let cachedVolume = localStorage.getItem('AtlasVolume');
		this.volumeSlider = cachedVolume ? Number(cachedVolume) : 0.6;
	}
	
	fontSizeChanged(event) {
		localStorage.setItem('AtlasFontSize', String(this.fontSlider));
		$('body').css('font-size', this.fontSlider + 'px');
		this.onFontSizeChanged.emit(this.fontSlider);
	}

	volumeChanged(event) {
		localStorage.setItem('AtlasVolume', String(this.volumeSlider));
	}

	export() {
		this.onExportLogs.emit();
	}

	showReleaseNotes() {
		this.onShowReleaseNotes.emit();
	}
}