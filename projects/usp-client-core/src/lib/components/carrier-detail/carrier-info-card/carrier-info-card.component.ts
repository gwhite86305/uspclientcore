import { Component, Input, ViewChild } from '@angular/core';
import { CarrierModel } from '../../../search-types';
import { RegistrationCard } from '../../../components/registration-card/registration-card.component';

/**
* 
* @param 
*/
@Component({
	selector: "carrier-info-card",
	// styles: [require('./carrier-info-card.styles.css')],
	templateUrl: './carrier-info-card.view.html'
})
export class CarrierInfoCard {	
	@Input()
	model: CarrierModel;
	@Input()
	searchTerm: string = "";
	@ViewChild(RegistrationCard)
	card;

	constructor() {

	}

	open() {
		this.card.open();
	}
}
