import { Component, Input, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { CarrierModel } from '../../search-types';

import { BasicInfoCard } from './basic-info-card/basic-info-card.component';
import { CarrierInfoCard } from './carrier-info-card/carrier-info-card.component';
import { GimcInfoCard } from './gimc-info-card/gimc-info-card.component';
import { HhgInfoCard } from './hhg-info-card/hhg-info-card.component';
import { PassengerInfoCard } from './passenger-info-card/passenger-info-card.component';
import { UcrDetailsInfoCard } from './ucr-details-info-card/ucr-details-info-card.component';
import { UcrInfoCard } from './ucr-info-card/ucr-info-card.component';
import { StatusModelService, StatusConfigSection } from '../../services/status-model.service';

const _ = require('lodash');

/**
 * CarrierDetail component -- Shows the detailed data for a selected carrier.
 * @param model (model) The CarrierModel object containing the carrier details.
 */
@Component({
	selector: "carrier-detail",
	templateUrl: './carrier-detail.view.html',
	encapsulation: ViewEncapsulation.None, //NOTE:  This is a temporary solution to our info-card components somehow not receiving carrier-detail styles.
	styleUrls: ['./carrier-detail.styles.css'],
})
export class CarrierDetail implements OnInit {
	@Input()
	model: CarrierModel;
	@Input()
	searchTerm: string = "";
	@ViewChild(CarrierInfoCard)
	private carrierInfoCard;
	@ViewChild(BasicInfoCard)
	private basicInfoCard;
	@ViewChild(GimcInfoCard)
	private gimcInfoCard;
	@ViewChild(HhgInfoCard)
	private hhgInfoCard;
	@ViewChild(PassengerInfoCard)
	private passengerInfoCard;
	@ViewChild(UcrDetailsInfoCard)
	private ucrDetailsInfoCard;
	@ViewChild(UcrInfoCard)
	private ucrInfoCard;

	// Loaded from status config file, and passed in view so that view re-renders when config finishes loading.
	carrierDetailStatusLabelClass: StatusConfigSection;
	cardOpenStatus = false;

	constructor(private _router: Router) {
	}

	ngOnInit() {
		this.carrierDetailStatusLabelClass = StatusModelService.carrierDetailStatusLabelClass;
	}

	openAllCards(cards: any[]) {
		this.carrierInfoCard ? this.carrierInfoCard.open() : null;
		this.basicInfoCard ? this.basicInfoCard.open() : null;
		this.gimcInfoCard ? this.gimcInfoCard.open() : null;
		this.hhgInfoCard ? this.hhgInfoCard.open() : null;
		this.passengerInfoCard ? this.passengerInfoCard.open() : null;
		this.ucrDetailsInfoCard ? this.ucrDetailsInfoCard.open() : null;
		this.ucrInfoCard ? this.ucrInfoCard.open() : null;
		// this.notifService.remove();
	}

	/** Reduce over all the BASICs to determine if they're valid overall */
	basicsAreValid(basics, serviceReady): string | boolean {
		if (!serviceReady) return 'U';

		// Map over each basic and select the .hasExceededThreshold property into an array
		var exceededs = basics.map(b => StatusModelService.asStatusObject(b.hasExceededThreshold).status);

		if (exceededs.filter(x => this.isUnknown(x)).length > 0) {
			return 'U'; // If any of them are unknown, return unknown
		}
		// Map all of them into booleans, then reduce over these to make sure they're all valid.
		return exceededs.map(b => this.isTrue(b)).reduce((a, b) => a && b, true);
	}

	// Wrapper methods for status styling service
	isTrue(model) {
		return StatusModelService.isTrue(model);
	}

	isFalse(model) {
		return StatusModelService.isFalse(model);
	}

	isUnknown(model) {
		return StatusModelService.isUnknown(model);
	}

	validateClass(model, carrierDetailStatusLabelClass) {
		if (!carrierDetailStatusLabelClass) return '';

		model = StatusModelService.asStatusObject(model)
		return carrierDetailStatusLabelClass[model.status] || carrierDetailStatusLabelClass["unknown"];
	}
}
