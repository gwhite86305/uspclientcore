import { Component, Input, ViewChild } from '@angular/core';
import { CarrierModel } from '../../../search-types';
import { RegistrationCard } from '../../../components/registration-card/registration-card.component';

@Component({
	selector: "ucr-info-card",
	templateUrl: './ucr-info-card.view.html'
})
export class UcrInfoCard {	
	@Input()
	model: CarrierModel;
	@Input()
	startOpen: boolean = false;
	@Input()
	overallValid: boolean = false;
	@ViewChild(RegistrationCard)
	private card;

	constructor() {
	}

	open() {
		this.card.open();
	}
}
