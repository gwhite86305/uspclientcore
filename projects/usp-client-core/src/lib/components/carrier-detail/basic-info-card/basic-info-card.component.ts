import { Component, Input, ViewChild, OnInit } from '@angular/core';
import { CarrierModel } from '../../../search-types';
import { StatusModelService, StatusConfigSection } from '../../../services/status-model.service';
import { RegistrationCard } from '../../../components/registration-card/registration-card.component';

/**
* 
* @param 
*/
@Component({
	selector: "basic-info-card",
	templateUrl: './basic-info-card.view.html'
})
export class BasicInfoCard implements OnInit {
	@Input()
	model: CarrierModel;
	@Input()
	startOpen: boolean = false;
	@Input()
	overallValid: boolean = false;

	@ViewChild(RegistrationCard)
	card;

	carrierDetailStatusLabelClass: StatusConfigSection;

	constructor(private statusService: StatusModelService) {

	}

	ngOnInit() {
		this.carrierDetailStatusLabelClass = StatusModelService.carrierDetailStatusLabelClass;	
	}

	/** Reduce over all the BASICs to determine if they're valid overall */
	basicsAreValid(basics, serviceReady): string | boolean {
		if (!serviceReady) return 'U';

		// Map over each basic and select the .hasExceededThreshold property into an array
		var exceededs = basics.map(b => StatusModelService.asStatusObject(b.hasExceededThreshold).status);

		if (exceededs.filter(x => this.isUnknown(x)).length > 0) {
			return 'U'; // If any of them are unknown, return unknown
		}
		// Map all of them into booleans, then reduce over these to make sure they're all valid.
		return exceededs.map(b => this.isTrue(b)).reduce((a, b) => a && b, true);
	}

	open() {
		this.card.open();
	}

	// Wrapper methods for status styling service
	isTrue(model) {
		return StatusModelService.isTrue(model);
	}
	
	isFalse(model) {
		return StatusModelService.isFalse(model);
	}

	isUnknown(model) {
		return StatusModelService.isUnknown(model);
	}

	validateClass(model, carrierDetailStatusLabelClass) {
		if (!carrierDetailStatusLabelClass) return '';

		model = StatusModelService.asStatusObject(model)
		return carrierDetailStatusLabelClass[model.status] || carrierDetailStatusLabelClass["unknown"];
	}
}
