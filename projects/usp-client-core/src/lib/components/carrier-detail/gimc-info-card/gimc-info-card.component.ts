import { Component, Input, ViewChild } from '@angular/core';
import { CarrierModel } from '../../../search-types';
import { RegistrationCard } from '../../../components/registration-card/registration-card.component';

@Component({
	selector: "gimc-info-card",
	templateUrl: './gimc-info-card.view.html'
})
export class GimcInfoCard {	
	@Input()
	model: CarrierModel;
	@Input()
	startOpen: boolean = false;
	@ViewChild(RegistrationCard)
	private card;

	constructor() {

	}

	open() {
		this.card.open();
	}
}
