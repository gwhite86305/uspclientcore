import { Component, Input, ViewChild } from '@angular/core';
import { CarrierModel } from '../../../search-types';
import { RegistrationCard } from '../../../components/registration-card/registration-card.component';

/**
* 
* @param 
*/
@Component({
	selector: "hhg-info-card",
	templateUrl: './hhg-info-card.view.html'
})
export class HhgInfoCard {	
	@Input()
	model: CarrierModel;
	@Input()
	startOpen: boolean = false;
	@ViewChild(RegistrationCard)
	private card;

	constructor() {

	}

	open() {
		this.card.open();
	}
}
