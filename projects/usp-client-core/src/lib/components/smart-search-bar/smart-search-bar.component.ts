import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LETSSmartSearch, LETSDatasource } from '../../search-types';
const _ = require('lodash');

@Component({
	selector: 'smart-search-bar',
	templateUrl: './smart-search-bar.component.html',
	styleUrls: ['./smart-search-bar.component.css']
})
export class SmartSearchBar implements OnInit {
	@Input()
	toggleSearchBar: EventEmitter<any> = new EventEmitter<any>();
	@Output()
	onSearch: EventEmitter<LETSSmartSearch> = new EventEmitter<LETSSmartSearch>();

	hidden: boolean = false;
	smartSearchParams: any;
	sources: LETSDatasource[];
	rawParamStr: string;
	showParsing: boolean = false;
	paramDetected: boolean = false;

	constructor() { }

	ngOnInit() {
		this.toggleSearchBar.subscribe(() => {
			this.hidden = !this.hidden;
		});
	}

	inputChange(newVal: string) {
		this.paramDetected = newVal.length == 9 ? true : false;
		this.showParsing = newVal ? true : false;
	}

	search() {
		let smartSearch = new LETSSmartSearch(this.smartSearchParams, this.sources);
		this.onSearch.emit(smartSearch);
	}
}
