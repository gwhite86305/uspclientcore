import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SexOffenderResult, OverviewCardField, LETSResult } from '../../search-types';
import { Observable } from 'rxjs';
import * as momentNs from 'moment';
const moment = momentNs;
const _ = require('lodash');

@Component({
  selector: 'sex-offender-card',
  templateUrl: './sex-offender-card.component.html',
  styleUrls: ['./sex-offender-card.component.css']
})
export class SexOffenderCard implements OnInit {
	@Input()
	sexOffenderData: SexOffenderResult = SexOffenderResult.getTestData();
	@Output()
	onShowDetails: EventEmitter<LETSResult> = new EventEmitter<LETSResult>();

	cardFields: OverviewCardField[];
	photoObservable: Observable<string> = Observable.create(observer => {
		observer.next('./assets/images/empty photo.jpg');
	});
	headerText: string;
	subHeaderText: string;

	constructor() { }

	ngOnInit() {
		//Format header and subheader fields
		this.headerText = this.sexOffenderData.name;
		this.subHeaderText = 'County: ' + this.sexOffenderData.county;

		//Format input fields
		let fieldArray = [];
		fieldArray.push({ fieldKey: 'Race', fieldValue: this.sexOffenderData.race });
		fieldArray.push({ fieldKey: 'Sex', fieldValue: this.sexOffenderData.sex });
		fieldArray.push({ fieldKey: 'Victim Age', fieldValue: this.sexOffenderData.victimAge });
		fieldArray.push({ fieldKey: 'Victim Sex', fieldValue: this.sexOffenderData.victimSex });
		this.cardFields = fieldArray;
	}

	showDetails() {
		this.onShowDetails.emit(this.sexOffenderData);
	}
}
