import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SexOffenderCardComponent } from './sex-offender-card.component';

describe('SexOffenderCardComponent', () => {
  let component: SexOffenderCardComponent;
  let fixture: ComponentFixture<SexOffenderCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SexOffenderCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SexOffenderCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
