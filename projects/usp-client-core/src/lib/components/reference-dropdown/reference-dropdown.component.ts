import { Component, Input, Output, EventEmitter } from '@angular/core';
import { RefData } from '../../core-types';

@Component({
	selector: "reference-dropdown",
	templateUrl: './reference-dropdown.view.html'
})
export class ReferenceDropdown {
	@Input()
	id: string = '';
	@Input()
	refItems: RefData[];
	@Input()
	initialValue: any;
	@Input()
	disabled: boolean = false;
	@Input()
	label: string = '';
	@Output()
	onChange: EventEmitter<any> = new EventEmitter<any>();

	constructor() {

	}

	dropdownChanged(event) {
		this.onChange.emit(event.target.value);
	}
}
