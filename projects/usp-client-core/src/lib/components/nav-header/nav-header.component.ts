import { Component, Input, Output, EventEmitter } from "@angular/core";
import { HttpClient } from '@angular/common/http';

export interface NavItem {
	popoverText: string,
	navId: string,
	icon: string,
	navText: string,
	route: string,
	isProfile: boolean,
	isLogout: boolean
}

@Component({
	selector: "nav-header",
	templateUrl: './nav-header.view.html',
	styleUrls: ['./nav-header.styles.css']
})
export class NavHeader {
	@Input()
	id: string = '';
	@Input()
	defaultRoute: string = '';
	@Input()
	applicationHeader: string = '';
	@Input()
	userName: string = '';
	@Output()
	logout: EventEmitter<any> = new EventEmitter<any>();

	navs: NavItem[] = [];

	constructor(private http: HttpClient) {
		this.http.get('config/nav-items.json')
			.subscribe((navs: NavItem[]) => {
				this.navs = navs;
			});
	}
}