import { Component } from '@angular/core';
import { ThemeService, Theme } from '../../services/theme.service';
const $ = require('jquery');

@Component({ 
	selector: '[theme-picker]',
	templateUrl: './theme-picker.view.html',
	styleUrls: ['./theme-picker.styles.css']
})
export class ThemePicker {
	themes: Theme[] = [];

    constructor() {
		this.themes = ThemeService.themes;
	}

	pickTheme(themeName: string) {
		ThemeService.setThemeByName(themeName);
	}
}