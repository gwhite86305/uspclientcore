import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailSearchSliderComponent } from './detail-search-slider.component';

describe('DetailSearchSliderComponent', () => {
  let component: DetailSearchSliderComponent;
  let fixture: ComponentFixture<DetailSearchSliderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailSearchSliderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailSearchSliderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
