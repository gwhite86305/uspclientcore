import { Component, OnInit, Input, Output, EventEmitter, ViewChild, TemplateRef } from '@angular/core';
import { SearchCategory, LETSDetailSearch } from '../../search-types';

@Component({
	selector: 'detail-search-slider',
	templateUrl: './detail-search-slider.component.html',
	styleUrls: ['./detail-search-slider.component.css']
})
export class DetailSearchSlider implements OnInit {
	@Input()
	toggleSlider: EventEmitter<SearchCategory> = new EventEmitter<SearchCategory>();
	@Output()
	onSearch: EventEmitter<LETSDetailSearch> = new EventEmitter<LETSDetailSearch>();

	category: SearchCategory;

	constructor() {

	}

	ngOnInit() {
		this.toggleSlider.subscribe((category) => {
			if (category) {
				this.category = category;
			}
		});
	}

	search(params: any) {
		let detailSearch = new LETSDetailSearch(params, this.category.dataSources);
		this.onSearch.emit(detailSearch);
	}

	close() {
		this.category = null;
	}
}
