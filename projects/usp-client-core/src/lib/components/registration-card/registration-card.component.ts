import { Component, Input, HostListener, OnChanges, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { StatusModel, StatusModelService } from '../../services/status-model.service';
/**
 * CarrierDetail component -- Shows the detailed data for a selected carrier.
 * @param model (model) The CarrierModel object containing the carrier details.
 */
@Component({
	selector: "registration-card",
	templateUrl: './registration-card.view.html',
	styleUrls: ['./registration-card.style.css'],
})
export class RegistrationCard implements OnChanges {
	@Input()
	id: string = '';
	@Input()
	title: string;
	@Input()
	overallValid: boolean = null;
	@Input()
	startOpen = true;
	@Input()
	model: StatusModel;
	@Input()
	isPanelStatusIconRequired: boolean = true;

	toggledOpen: boolean = null;

	constructor(private _router: Router, private statusService: StatusModelService) {

	}

	ngOnChanges(changes: SimpleChanges) {
		if (changes['startOpen'] && changes['startOpen'].previousValue != changes['startOpen'].currentValue) {
			this.toggledOpen = null;
		}
	}

	open() {
		this.toggledOpen = true;
	}

	shouldBeOpen(startOpen, toggledOpen) {
		return toggledOpen === null ? startOpen : toggledOpen;
	}

	@HostListener('window:keydown', ['$event'])
	handleKeyboardEvent(event: KeyboardEvent) {
		if ((event.ctrlKey || event.metaKey)) {
			switch (event.keyCode) {
				case 79: // ctrl + o
					this.toggledOpen = true;
					event.preventDefault();
					break;
				case 80: // ctrl + p
					this.toggledOpen = false;
					event.preventDefault();
					break;
			}
		}
	}
}
