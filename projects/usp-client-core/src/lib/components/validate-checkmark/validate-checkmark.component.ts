import { Component, Input, OnChanges } from '@angular/core';
import { StatusModelService, StatusModel } from '../../services/status-model.service';

/**
 * ValidateCheckmark component -- A tiny component that conditionally displays a validation-related icon.
 * @param model (model) The boolean that determines which icon to show.
 * @param swapGood (swapGood) - Boolean value to swap whether "Yes" is good or "No" is good, i.e. green.
 */
@Component({
	selector: "validate-checkmark",
	templateUrl: './validate-checkmark.view.html',
	styleUrls: ['./validate-checkmark.styles.css']
})
export class ValidateCheckmark implements OnChanges {

	@Input()
	id: string = '';
	@Input()
	model: StatusModel;
	@Input()
	hideMessage: boolean = false;

	containerClass: string = "";
	iconClass: string = "";
	message: string = "";

	constructor() {

	}

	onUpdates() {
		let ICON_CLASSES = StatusModelService.checkmarkIconClass;
		let CONTAINER_CLASSES = StatusModelService.checkmarkContainerClass;

		var model = StatusModelService.asStatusObject(this.model);
		this.iconClass = ICON_CLASSES[model.status] || ICON_CLASSES["unknown"];
		this.containerClass = CONTAINER_CLASSES[model.status] || CONTAINER_CLASSES["unknown"];
		this.message = model.message;
	}

	ngOnChanges(changes: any) {
		if (changes.model) {
			this.onUpdates();
		}
	}
}
