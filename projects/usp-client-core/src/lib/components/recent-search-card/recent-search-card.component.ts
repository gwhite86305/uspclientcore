import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LETSRecentSearch } from '../../search-types';

@Component({
	selector: 'recent-search-card',
	templateUrl: './recent-search-card.component.html',
	styleUrls: ['./recent-search-card.component.css']
})
export class RecentSearchCard implements OnInit {
	@Input()
	recentSearches: LETSRecentSearch[];

	@Output()
	onRerunSearch: EventEmitter<any> = new EventEmitter<any>();

	constructor() { }

	ngOnInit() {
	}

	rerunSearch(recentSearch: LETSRecentSearch) {
		this.onRerunSearch.emit(recentSearch);
	}
}