import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentSearchCard } from './recent-search-card.component';

describe('RecentSearchCardComponent', () => {
	let component: RecentSearchCard;
	let fixture: ComponentFixture<RecentSearchCard>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
		declarations: [RecentSearchCard ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
	  fixture = TestBed.createComponent(RecentSearchCard);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
