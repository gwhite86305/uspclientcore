import { Component, Directive, Input, Output, EventEmitter } from '@angular/core';
const _ = require('lodash');
const $ = require('jquery');

import { SearchListObj, CarrierModel } from '../../search-types';

const MAX_LOG_ENTRIES = 100;

@Component({
	selector: "carrier-overview",
	templateUrl: './carrier-overview.view.html',
	styleUrls: ['./carrier-overview.styles.css']
})
export class CarrierOverview {
	_ = _;
	pageNum: number = 0;
	latestWidth: number;

	@Input()
	searchTerms: SearchListObj[];
	@Input()
	searchDate: Date;
	@Input()
	carriers: CarrierModel[];
	@Input()
	loading: boolean = false;
	@Input()
	selectedResultTab: string | number;
	@Output()
	selectedResultTabChange: EventEmitter<string | number> = new EventEmitter<string | number>();

	constructor() {

	}

	ensureDate(d: any) {
		// When search history gets serialized, dates get turned into strings. Make sure this is a date and not a string
		return new Date(d);
	}

	selectResultTab(index: string | number) {
		this.selectedResultTab = index;
		this.selectedResultTabChange.emit(index);
	}
}