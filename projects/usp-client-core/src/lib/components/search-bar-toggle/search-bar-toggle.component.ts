import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
	selector: '[search-bar-toggle]',
	templateUrl: './search-bar-toggle.component.html',
	styleUrls: ['./search-bar-toggle.component.css']
})
export class SearchBarToggle implements OnInit {
	@Input()
	popoverPosition: string = 'bottom';
	@Output()
	onToggle: EventEmitter<any> = new EventEmitter<any>();

	showSearchBar: boolean = true;

	constructor() { }

	ngOnInit() {
	}

}
