import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchBarToggleComponent } from './search-bar-toggle.component';

describe('SearchBarToggleComponent', () => {
  let component: SearchBarToggleComponent;
  let fixture: ComponentFixture<SearchBarToggleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchBarToggleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchBarToggleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
