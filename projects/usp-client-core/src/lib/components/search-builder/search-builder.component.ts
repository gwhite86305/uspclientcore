import { Component, Input, Output, EventEmitter, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormBuilder, NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { ModalDirective } from 'ngx-bootstrap/modal';
import * as momentNs from 'moment';
const moment = momentNs;
const _ = require('lodash');
const $ = require('jquery');

import { SearchCategory, GenericField, FieldGrouping, FieldTypes, NetworkStatus, SearchListObj } from '../../search-types';

export interface AutoCompleteModel {
	value: any;
	display: string;
	group: boolean;
	category: string;
	iconSrc?: string;
}

@Component({
	selector: "search-builder",
	templateUrl: './search-builder.view.html',
	styleUrls: ['./search-builder.styles.css'],
})
export class SearchBuilder implements OnInit, OnDestroy {
	@Input()
	dateFormat: string = 'mm/dd/yyyy';
	@Input()
	categories: SearchCategory[];
	@Input()
	onResultsClose: EventEmitter<any>;
	@Input()
	maxSearchCategories: number;
	@Input()
	privilegeLevel: number = 0;
	@Output()
	onSearch: EventEmitter<any> = new EventEmitter<any>();

	@ViewChild('searchBuilderModal') 
	searchBuilderModal: ModalDirective;

	fieldTypes = FieldTypes;	
	isMini: boolean = false;
	fields: AutoCompleteModel[] = [];
	selectedFields: AutoCompleteModel[] = [];
	hideMultiselect: boolean = true;
	hasNetwork: NetworkStatus = {};
	networkSub: Subscription;
	resultsCloseSub: Subscription;
	toggleValues = [];

	constructor(private http: HttpClient, private formBuilder: FormBuilder) {
		// this.networkSub = networkService.networkAvailable$.subscribe(
		// 	networkAvailable => {
		// 		this.hasNetwork = networkAvailable;
		// 	}
		// );
		// networkService.checkNetwork();
		this.hasNetwork = { internet: true, vpn: true, api: true, loggedin: true };		
	}

	ngOnInit() {
		// this.myDatePickerOptions.dateFormat = this.dateFormat;
		//Subscribe to the results close event provided by the parent component so we can respond to it.
		this.resultsCloseSub = this.onResultsClose.subscribe((event) => this.expandSearchBuilder());

		_.map(this.categories, (category: SearchCategory) => {
			//Check category privilege level against user's privilege level to see if we should use the category.
			let effectivePrivilege = category.privilegeLevel ? category.privilegeLevel : 0;
			if (effectivePrivilege > this.privilegeLevel)
				return;

			_.map(category.fields, (field: GenericField) => {
				field.fieldIcon = category.categoryIcon;
				let newModel = { display: field.friendlyFieldName, value: field, group: false, category: category.name, iconSrc: category.categoryIcon  };
				this.fields.push(newModel);

				if (field.defaultField) //Default fields should be added to the tag list automatically.
					this.selectedFields.push(newModel);
			});
			_.map(category.fieldGroups, (group: FieldGrouping) => {
				_.map(group.groupFields, (field: GenericField) => {
					field.fieldIcon = category.categoryIcon;
				});
				this.fields.push({ display: group.groupName, value: group.groupFields, group: true, category: category.name, iconSrc: category.categoryIcon });
			});
		});
	}

	ngOnDestroy() {
		// this.networkSub.unsubscribe();
	}

	fieldSelected(model: AutoCompleteModel) {
		//If the field being added is the first of its category, we need to automatically add any category default fields.
		console.info('Field has been selected', model);
		let isFirstInCategory: boolean = _.filter(this.selectedFields, (existingModel: AutoCompleteModel) => {
			return existingModel.category == model.category;
		}).length == 1;

		if (isFirstInCategory) {
			let categoryDefaults: AutoCompleteModel[] = _.filter(this.fields, (existingModel: AutoCompleteModel) => {
				return existingModel.category == model.category 
					&& !existingModel.group 
					&& (existingModel.value as GenericField).defaultForCategory
					&& !_.isEqual(existingModel, model);
			});

			this.selectedFields = _.concat(this.selectedFields, categoryDefaults);
		}
	}

	fieldRemoved(model: AutoCompleteModel) {
		// console.info('Field has been removed', model);
		// let isLastInCategory: boolean = _.filter(this.selectedFields, (existingModel: AutoCompleteModel) => {
		// 	return existingModel.category == model.category;
		// }).length == 0;

		// if (isLastInCategory) {
		// 	let categoryDefaults: AutoCompleteModel[] = _.filter(this.fields, (existingModel: AutoCompleteModel) => {
		// 		return existingModel.category == model.category && !existingModel.group && (existingModel.value as GenericField).defaultForCategory;
		// 	});

		// 	this.selectedFields = _.difference(this.selectedFields, categoryDefaults);
		// }
	}

	getFieldNameFromGroup(fields: GenericField[]): string {
		return _.reduce(fields, (groupName: string, field: GenericField) => {
			if (!groupName)
				return field.friendlyFieldName;
			else
				return groupName + '/' + field.friendlyFieldName;
		}, '');
	}

	getFieldsByType(type: string): GenericField[] {
		let fields: any[] = _.map(this.selectedFields, (model: AutoCompleteModel) => {
			return model.value;
		});
		let flatFields: GenericField[] = _.flatten(fields);
		return _.filter(flatFields, (field: GenericField) => {
			//Initialize the field's value if it has a default provided
			field.value = field.defaultValue ? field.defaultValue : '';
			return field.type == type;
		});
	}

	search(searchForm?: NgForm) {
		let formValues: any = searchForm.value;
		let isValid: boolean = searchForm.valid;
		let searchList: SearchListObj[] = [];

		_.map(this.selectedFields, (model: AutoCompleteModel) => {
			if (!model.group) {
				searchList.push(this.convertFieldToSearchTerm(model.value, model.category, formValues));
			}
			else {
				let groupedFields: GenericField[] = model.value;
				_.map(groupedFields, (field: GenericField) => {
					searchList.push(this.convertFieldToSearchTerm(field, model.category, formValues));
				});
			}
		});

		this.onSearch.emit(searchList);
		
		this.selectedFields = [];
		searchForm.reset();

		//We always want to show the minified view of the component on searching and hide the modal. 
		this.isMini = true;
		this.searchBuilderModal.hide();
	}

	expandSearchBuilder() {
		this.isMini = false;
	}

	private convertFieldToSearchTerm(field: GenericField, category: string, formValues: any) {
		let fieldValue = '';
		let fieldType = field.type;

		if (fieldType == FieldTypes.TEXT || fieldType == FieldTypes.REFERENCE || fieldType == FieldTypes.TOGGLE)
			fieldValue = formValues[field.uniqueFieldName] ? formValues[field.uniqueFieldName].trim() : '';		
		else if (fieldType == FieldTypes.DATE)
			fieldValue = formValues[field.uniqueFieldName] ? moment(formValues[field.uniqueFieldName]).format('YYYY-MM-DD') : '';

		return {
			name: field.internalFieldName,
			value: fieldValue,
			type: category
		};
	}
}