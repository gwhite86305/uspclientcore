import { Component, ElementRef, Input, Renderer } from '@angular/core';
import { SearchLoadingProgress } from '../../core-types';

declare var $: any;

/**
 * LoadingAnim component -- Shows the custom loading animation.
 */
@Component({
    selector: 'loading-animation',
    templateUrl: './loading-animation.view.html',
    styleUrls: ['./loading-animation.styles.css']
})
export class LoadingAnimation {
	@Input()
	id: string = '';
    @Input()
    label: string = "Loading";
    @Input()
    mini = false;
    @Input()
	miniLoadingProgress: SearchLoadingProgress = SearchLoadingProgress.noResults;
	@Input()
	loaderSrc: string;

    constructor(private _e: ElementRef, private _renderer: Renderer) {

    }

    getMiniLoaderTooltip() {
        switch (this.miniLoadingProgress) {
            case SearchLoadingProgress.noResults:
                return 'Search data not yet available...';
            case SearchLoadingProgress.partialResults:
                return 'Partial search data available, full results pending...';
            default:
                return '';
        }
    }

    getMiniLoaderColor() {
        switch (this.miniLoadingProgress) {
            case SearchLoadingProgress.noResults:
                return 'text-warning';
            case SearchLoadingProgress.partialResults:
                return 'text-success';
            default:
                return '';
        }
    }
}
