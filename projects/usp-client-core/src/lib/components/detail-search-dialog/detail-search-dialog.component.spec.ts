import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailSearchDialogComponent } from './detail-search-dialog.component';

describe('DetailSearchDialogComponent', () => {
  let component: DetailSearchDialogComponent;
  let fixture: ComponentFixture<DetailSearchDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailSearchDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailSearchDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
