import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SearchCategory } from '../../search-types';

@Component({
	selector: '[detail-search-dialog]',
	templateUrl: './detail-search-dialog.component.html',
	styleUrls: ['./detail-search-dialog.component.css']
})
export class DetailSearchDialog implements OnInit {
	@Input()
	categories: SearchCategory[];
	@Output()
	onSearchSelected: EventEmitter<SearchCategory> = new EventEmitter<SearchCategory>();

	constructor() { }

	ngOnInit() {
	}
}
