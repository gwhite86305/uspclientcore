import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
	selector: 'nav-sidebar-template',
	templateUrl: './nav-sidebar-template.component.html',
	styleUrls: ['./nav-sidebar-template.component.css']
})
export class NavSidebarTemplate implements OnInit {
	@Input()
	appHeader: string = '';

	constructor() { }

	ngOnInit() {
	}

}
