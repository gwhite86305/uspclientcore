import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NavSidebarTemplateComponent } from './nav-sidebar-template.component';

describe('NavSidebarTemplateComponent', () => {
  let component: NavSidebarTemplateComponent;
  let fixture: ComponentFixture<NavSidebarTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NavSidebarTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavSidebarTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
