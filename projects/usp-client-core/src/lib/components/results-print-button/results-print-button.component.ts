import { Component, Output, EventEmitter } from '@angular/core';

@Component({
	selector: "results-print-button",
	templateUrl: './results-print-button.view.html'
})
export class ResultsPrintButton {	
	@Output()
	onPrint: EventEmitter<any> = new EventEmitter();

	constructor() {

	}

	printToPdf() {
		this.onPrint.emit();
	}
}
