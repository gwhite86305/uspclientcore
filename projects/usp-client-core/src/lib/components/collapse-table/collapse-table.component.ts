import { Component, Input, EventEmitter, OnInit } from '@angular/core';
import * as momentNs from 'moment';
const moment = momentNs;

/**
 * VehicleDetail component -- Shows the detailed data for a selected vehicle.
 * @param model (model) The VehicleModel object containing the vehicle details.
 */
@Component({
	selector: "collapse-table",
	templateUrl: './collapse-table.view.html',
	styles: [`
	  .highlight {
		opacity: 0.6;
		padding: 1px;
		padding-left: 5px;
		padding-right: 5px;
		display: inline-block;
  	}`]
})
export class CollapseTable implements OnInit {	
	@Input()
	id: string = '';
	@Input()
	fieldLabel: string;
	@Input()
	fieldName: string;
	@Input()
	fieldValue: string;
	@Input()
	sources: any[];
	@Input()
	expandEvent: EventEmitter<any>;
	@Input()
	toggleEvent: EventEmitter<any>;

	collapsed: boolean = true;

	constructor() {
		
	}

	ngOnInit() {
		if (this.expandEvent)
			this.expandEvent.subscribe(() => this.expand());
		if (this.toggleEvent)
			this.toggleEvent.subscribe(() => this.toggle());
	}

	toggle() {
		this.collapsed = !this.collapsed;
	}

	expand() {
		this.collapsed = false;
	}

	collapse() {
		this.collapsed = true;
	}
}