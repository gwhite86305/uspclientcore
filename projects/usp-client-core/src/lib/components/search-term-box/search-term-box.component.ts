import { Component, Input } from '@angular/core';
const _ = require('lodash');

import { SearchListObj } from '../../search-types';

@Component({
	selector: "search-term-box",
	templateUrl: './search-term-box.view.html',
	styleUrls: ['./search-term-box.styles.css']
})
export class SearchTermBox {	
	@Input()
	searchTerms: SearchListObj[];

	collapsed: boolean = true;

	constructor() {
		
	}
}