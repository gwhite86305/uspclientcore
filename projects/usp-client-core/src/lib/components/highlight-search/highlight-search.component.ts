import { Component, Input } from '@angular/core';

@Component({
	selector: "highlight-search",
	templateUrl: './highlight-search.view.html',
	styleUrls: ['./highlight-search.styles.css'],
})
export class HighlightSearch {
	@Input()
	id: string = '';
	@Input()
	model: string;
	@Input()
	searchTerm: string;

	constructor() {
	}

	splitSections(model, searchTerm) {
		if (!model || !searchTerm) {
			return [{ highlight: false, value: model }];
		}
		model = model + "";
		searchTerm = searchTerm + "";
		var sections = [];
		while (true) {
			var nextIndex = model.toLowerCase().indexOf(searchTerm.toLowerCase());

			if (nextIndex < 0 || !searchTerm) {
				sections.push({ highlight: false, value: model });
				break;
			} else {
				var normalSection = model.substr(0, nextIndex);
				var highlightSection = model.substr(nextIndex, searchTerm.length);
				model = model.substr(nextIndex + searchTerm.length);

				if (normalSection) {
					sections.push({ highlight: false, value: normalSection });
				}
				sections.push({ highlight: true, value: highlightSection });
			}
		}

		return sections;
	}
}
