import { Component, Input } from '@angular/core';
import { ReleaseNote } from '../../core-types';
const $ = require('jquery');

@Component({ 
	selector: 'release-notes',
	templateUrl: './release-notes.view.html',
})
export class ReleaseNotes {

	@Input()
	releaseNotes: ReleaseNote[];

	constructor() {

	}
}