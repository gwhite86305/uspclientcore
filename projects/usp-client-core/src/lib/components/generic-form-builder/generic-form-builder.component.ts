import { Component, Input, Output, EventEmitter, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormBuilder, NgForm } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Subscription } from 'rxjs';
import * as momentNs from 'moment';
const moment = momentNs;
const _ = require('lodash');
const $ = require('jquery');

import { SearchCategory, GenericField, FieldGrouping, FieldTypes } from '../../search-types';

export interface GenericCategoryModel {
	value: any;
	display: string;
	group: boolean;
	category: string;
	iconSrc?: string;
}

@Component({
	selector: 'generic-form-builder',
	templateUrl: './generic-form-builder.component.html',
	styleUrls: ['./generic-form-builder.component.css']
})
export class GenericFormBuilder implements OnInit {
	@Input()
	dateFormat: string = 'mm/dd/yyyy';
	@Input()
	category: SearchCategory;
	@Input()
	privilegeLevel: number = 0;
	@Input()
	showCloseButton: boolean = true;
	@Output()
	onSearch: EventEmitter<any> = new EventEmitter<any>();
	@Output()
	onClose: EventEmitter<any> = new EventEmitter<any>();

	fieldTypes = FieldTypes;
	fields: GenericCategoryModel[] = [];
	toggleValues = [];
	_ = _;

	constructor(private formBuilder: FormBuilder) {

	}

	ngOnInit() {
		let effectivePrivilege = this.category.privilegeLevel ? this.category.privilegeLevel : 0;
		if (effectivePrivilege > this.privilegeLevel)
			return;
		
		_.map(this.category.fieldGroups, (group: FieldGrouping) => {
			_.map(group.groupFields, (field: GenericField) => {
				field.fieldIcon = this.category.categoryIcon;
			});
			this.fields.push({ display: group.groupName, value: group.groupFields, group: true, category: this.category.name, iconSrc: this.category.categoryIcon });
		});
	}

	getFieldNameFromGroup(fields: GenericField[]): string {
		return _.reduce(fields, (groupName: string, field: GenericField) => {
			if (!groupName)
				return field.friendlyFieldName;
			else
				return groupName + '/' + field.friendlyFieldName;
		}, '');
	}

	getFieldsByType(type: string): GenericField[] {
		let fields: any[] = _.map(this.fields, (model: GenericCategoryModel) => {
			return model.value;
		});
		let flatFields: GenericField[] = _.flatten(fields);
		return _.filter(flatFields, (field: GenericField) => {
			//Initialize the field's value if it has a default provided
			field.value = field.defaultValue ? field.defaultValue : '';
			return field.type == type;
		});
	}

	search(searchForm?: NgForm) {
		let formValues: any = searchForm.value;
		let isValid: boolean = searchForm.valid;
		let searchParams: any = {};

		_.map(this.fields, (model: GenericCategoryModel) => {
			if (!model.group) {
				_.assign(searchParams, this.convertFieldToSearchTerm(model.value, model.category, formValues));
			}
			else {
				let groupedFields: GenericField[] = model.value;
				_.map(groupedFields, (field: GenericField) => {
					_.assign(searchParams, this.convertFieldToSearchTerm(field, model.category, formValues));
				});
			}
		});

		this.onSearch.emit(searchParams);
		searchForm.reset();
	}

	private convertFieldToSearchTerm(field: GenericField, category: string, formValues: any) {
		let fieldValue = '';
		let fieldType = field.type;

		if (fieldType == FieldTypes.TEXT || fieldType == FieldTypes.REFERENCE || fieldType == FieldTypes.TOGGLE)
			fieldValue = formValues[field.uniqueFieldName] ? formValues[field.uniqueFieldName].trim() : '';
		else if (fieldType == FieldTypes.DATE)
			fieldValue = formValues[field.uniqueFieldName] ? moment(formValues[field.uniqueFieldName]).format('YYYY-MM-DD') : '';

		let returnObj = {};
		returnObj[field.internalFieldName] = fieldValue;
		return returnObj;
	}
}
