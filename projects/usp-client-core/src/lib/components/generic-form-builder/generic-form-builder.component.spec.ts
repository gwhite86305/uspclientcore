import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GenericFormBuilder } from './generic-form-builder.component';

describe('FormBuilderComponent', () => {
	let component: GenericFormBuilder;
	let fixture: ComponentFixture<GenericFormBuilder>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
		declarations: [GenericFormBuilder ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
	  fixture = TestBed.createComponent(GenericFormBuilder);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
