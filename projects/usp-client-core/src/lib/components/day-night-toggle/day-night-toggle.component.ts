import { Component, HostListener, OnInit } from '@angular/core';
import { ThemeService } from '../../services/theme.service';

@Component({
	selector: "[day-night-toggle]",
	templateUrl: './day-night-toggle.view.html'
})
export class DayNightToggle implements OnInit {
	dayMode: boolean = true;

	constructor() {

	}

	ngOnInit() {
		if (ThemeService.isDayTheme())
			this.dayMode = true;
		else
			this.dayMode = false;
	}

	@HostListener('click', ['$event.target'])
	onToggle(btn) {
		this.dayMode = !this.dayMode;
		ThemeService.toggleDayNight(this.dayMode);
	}
}
