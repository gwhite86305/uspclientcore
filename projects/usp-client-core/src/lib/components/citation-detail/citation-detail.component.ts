import { Component, Input, Output, EventEmitter } from '@angular/core';
const _ = require('lodash');

import { CitationModel } from '../../search-types';

@Component({
	selector: "citation-detail",
	templateUrl: './citation-detail.view.html',
	styleUrls: ['./citation-detail.styles.css'],
})
export class CitationDetail {
	@Input()
	model: CitationModel;
	@Input()
	searchTerm: string = "";
	@Output()
	onPrint: EventEmitter<any> = new EventEmitter<any>();

	constructor() {

	}
}