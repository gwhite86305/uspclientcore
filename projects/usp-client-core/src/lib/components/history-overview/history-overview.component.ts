import { Component, Input, Output, EventEmitter, OnInit, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import * as momentNs from 'moment';
const moment = momentNs;
const _ = require('lodash');

import { SearchListObj, AtlasQueryLogModel } from '../../search-types';
import { ThemeService } from '../../services/theme.service';

const MAX_LOG_ENTRIES = 100;

@Component({
	selector: "history-overview",
	templateUrl: './history-overview.view.html',
	styleUrls: ['./history-overview.styles.css'],
})
export class HistoryOverview implements OnInit {
	private latestWidth: number;
	private cachedHistoryData: AtlasQueryLogModel[] = [];

	@Input()
	searchTerms: SearchListObj[];
	@Input()
	historyData: AtlasQueryLogModel[];
	@Input()
	loading: boolean = false;
	@Input()
	selectedResultTab: string | number;
	@Input()
	getHistoryMethod: Function; //Returns an observable of history results
	@Output()
	launchNewQuery: EventEmitter<SearchListObj[]> = new EventEmitter<SearchListObj[]>();

	@ViewChild(DatatableComponent) table: DatatableComponent;

	filter: string = '';
	pageNum: number = 0;
	maxHistoryItems: number = MAX_LOG_ENTRIES;
	themeMode: string = 'day';

	constructor() {

	}

	ngOnInit() {
		this.cachedHistoryData = this.historyData;

		//Setup component to respond to theme changes and pull the initial theme mode value
		ThemeService.onThemeChange.subscribe((mode: string) => this.themeMode = mode);
		this.themeMode = ThemeService.isDayTheme() ? 'day' : 'night';
	}

	serializeQueryLog(queryLog: AtlasQueryLogModel): string {
		let serializedQueryLog = '';

		switch (queryLog.QueryType) {
			case 'vehicle':
				serializedQueryLog = queryLog.State.toUpperCase() + ', ' + queryLog.VehTagNum.toUpperCase();
				break;
			case 'person':
				serializedQueryLog = queryLog.DefendantLastName.toUpperCase() + ', ' +
					queryLog.DefendantFirstName.toUpperCase() + ' (' +
					queryLog.DefendantGender.toUpperCase() + ') ' +
					moment(queryLog.DefendantDOB).format('MM/DD/YYYY');
				// this.datePipe.transform(queryLog.DefendantDOB);
				break;
			case 'dln':
				serializedQueryLog = queryLog.State.toUpperCase() + ', ' + queryLog.LicNum.toUpperCase();
				break;
			default:
				break;
		}

		return serializedQueryLog;
	}

	filterResults(event) {
		let filter: string = event.target.value.toLowerCase();
		this.table.offset = 0;

		if (!filter) {
			this.historyData = this.cachedHistoryData;
		}
		else {
			this.historyData = _.filter(this.cachedHistoryData, (queryLog: AtlasQueryLogModel) => {
				let formattedDateString = queryLog.DateSearched ? moment(queryLog.DateSearched).format('M/D/YYYY, h:mm A').toLowerCase() : '';
				let queryType = queryLog.QueryType ? queryLog.QueryType.toLowerCase() : '';
				let serializedQueryLog = this.serializeQueryLog(queryLog).toLowerCase();
				
				if (formattedDateString.includes(filter) ||	queryType.includes(filter) || serializedQueryLog.includes(filter))
					return true;				
				else
					return false;
			});
		}
	}

	runNewQuery(queryLog: AtlasQueryLogModel) {
		let newQuery: SearchListObj[] = [];

		switch (queryLog.QueryType) {
			case 'vehicle':
				newQuery.push({ name: 'VehTagState', value: queryLog.State, type: 'vehicle' });
				newQuery.push({ name: 'VehTagNum', value: queryLog.VehTagNum, type: 'vehicle' });
				break;
			case 'person':
				newQuery.push({ name: 'DefendantFirstName', value: queryLog.DefendantFirstName, type: 'person' });
				newQuery.push({ name: 'DefendantLastName', value: queryLog.DefendantLastName, type: 'person' });
				newQuery.push({ name: 'DefendantGender', value: queryLog.DefendantGender, type: 'person' });
				newQuery.push({ name: 'DefendantDOB', value: queryLog.DefendantDOB, type: 'person' });
				break;
			case 'dln':
				newQuery.push({ name: 'LicState', value: queryLog.State, type: 'license' });
				newQuery.push({ name: 'LicNum', value: queryLog.LicNum, type: 'license' });
				break;
			default:
				return;
		}

		this.launchNewQuery.emit(newQuery);
	}

	canLoadMoreHistoryResults(): boolean {
		if (this.maxHistoryItems <= this.historyData.length)
			return true;
		else
			return false;
	}

	loadMoreHistoryResults() {
		this.loading = true;
		this.maxHistoryItems += MAX_LOG_ENTRIES;
		let maxLogsSearchTerm: SearchListObj = _.find(this.searchTerms, ['name', 'MaxLogEntries']);

		if (!maxLogsSearchTerm) {
			//Haven't put MaxLogEntries into search terms yet, so push it to the array after increasing the max items.			
			this.searchTerms.push({ name: 'MaxLogEntries', value: this.maxHistoryItems.toString(), type: 'vehicle' });
		}
		else {
			//Just increase the max items for the existing MaxLogEntries term.
			maxLogsSearchTerm.value = (parseInt(maxLogsSearchTerm.value) + MAX_LOG_ENTRIES).toString();
		}

		this.getHistoryMethod(this.searchTerms).subscribe(historyData => {
			this.cachedHistoryData = historyData;
			this.historyData = _.cloneDeep(historyData);
			this.loading = false;
		});
	}
}