import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ConfigService } from '../../services/config.service';
import { ResultCategory } from '../../search-types';
import * as _ from 'lodash';

@Component({
	selector: 'results-sidebar',
	templateUrl: './results-sidebar.component.html',
	styleUrls: ['./results-sidebar.component.css']
})
export class ResultsSidebarComponent implements OnInit {
	openStyles = {
		position: 'fixed', 
		padding: '0px',
		border: '1px solid gray',
		marginTop: '0px',
		height: '100vh'
	};

	opened: boolean = false;
	resultCategories: ResultCategory[] = [];


	@Input()
	toggle: EventEmitter<any> = new EventEmitter<any>();
	@Input()
	position: string;
	@Input()
	appendToElement: any;
	@Input()
	marginTop: string;

	constructor(private configService: ConfigService) {
		this.configService.configStream.subscribe((configData: any) => {
			this.resultCategories = _.map(configData.resultCategoryConfig, (newCat: any) => new ResultCategory(newCat));
		});
	}

	ngOnInit() {
		this.toggle.subscribe(() => this.toggleSidebar());
		if (this.position == 'left' || this.position == 'right')
			this.openStyles.marginTop = this.marginTop || '0px';
		else
			this.openStyles.height = 'auto';
	}

	toggleSidebar() {
		this.opened = !this.opened;
	}
}
