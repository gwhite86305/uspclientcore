import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DOCResult, OverviewCardField, LETSResult } from '../../search-types';
import { Observable } from 'rxjs';
import * as momentNs from 'moment';
const moment = momentNs;
const _ = require('lodash');

@Component({
	selector: 'doc-card',
	templateUrl: './doc-card.component.html',
	styleUrls: ['./doc-card.component.css']
})
export class DOCCard implements OnInit {
	@Input()
	docData: DOCResult = DOCResult.getTestData();
	@Output()
	onShowDetails: EventEmitter<LETSResult> = new EventEmitter<LETSResult>();

	cardFields: OverviewCardField[];
	photoObservable: Observable<string> = Observable.create(observer => {
		observer.next('./assets/images/empty photo.jpg');
	});
	headerText: string;
	subHeaderText: string;

	constructor() { }

	ngOnInit() {
		//Format header and subheader fields
		this.headerText = this.docData.lastName + ', ' + this.docData.firstName + ' ' + this.docData.middleName;
		this.subHeaderText = 'SSN: ' + this.docData.ssn;

		//Format input fields
		let fieldArray = [];
		fieldArray.push({ fieldKey: 'Date of Birth', fieldValue: moment(new Date(this.docData.dateOfBirth)).format('MM/DD/YYYY') });
		fieldArray.push({ fieldKey: 'Age', fieldValue: this.docData.age });
		fieldArray.push({ fieldKey: 'Date Admitted', fieldValue: moment(new Date(this.docData.admitDate)).format('MM/DD/YYYY') });
		fieldArray.push({ fieldKey: 'Date Released', fieldValue: moment(new Date(this.docData.releaseDate)).format('MM/DD/YYYY') });
		this.cardFields = fieldArray;
	}

	showDetails() {
		this.onShowDetails.emit(this.docData);
	}
}
