import { Component, Input } from '@angular/core';

@Component({
	selector: "validation-label",
	styleUrls: ['./validation-label.styles.css'],
	templateUrl: './validation-label.view.html'
})
export class ValidationLabel {	
	@Input()
	id: string = '';
	@Input()
	text: string = "";
	@Input()
	isValid: boolean = false;
	@Input()
	labelClass: string = '';

	constructor() {

	}
}
