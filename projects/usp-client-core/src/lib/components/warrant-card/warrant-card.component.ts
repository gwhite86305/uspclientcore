import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { HotfileWarrantResult, OverviewCardField, LETSResult } from '../../search-types';
import { Observable } from 'rxjs';
import * as momentNs from 'moment';
const moment = momentNs;
const _ = require('lodash');

@Component({
  selector: 'warrant-card',
  templateUrl: './warrant-card.component.html',
  styleUrls: ['./warrant-card.component.css']
})
export class WarrantCard implements OnInit {
	@Input()
	warrantData: HotfileWarrantResult = HotfileWarrantResult.getTestData();
	@Output()
	onShowDetails: EventEmitter<LETSResult> = new EventEmitter<LETSResult>();

	cardFields: OverviewCardField[];
	photoObservable: Observable<string> = Observable.create(observer => {
		observer.next('./assets/images/empty photo.jpg');
	});
	headerText: string;
	subHeaderText: string;

	constructor() { }

	ngOnInit() {
		//Format header and subheader fields
		this.headerText = this.warrantData.name;
		this.subHeaderText = 'SID: ' + this.warrantData.sid;

		//Format input fields
		let fieldArray = [];
		fieldArray.push({ fieldKey: 'Date of Birth', fieldValue: moment(new Date(this.warrantData.dob)).format('MM/DD/YYYY') });
		fieldArray.push({ fieldKey: 'Warrant Date', fieldValue: moment(new Date(this.warrantData.warrantDate)).format('MM/DD/YYYY') });
		fieldArray.push({ fieldKey: 'Race', fieldValue: this.warrantData.race });
		fieldArray.push({ fieldKey: 'Sex', fieldValue: this.warrantData.sex });		
		this.cardFields = fieldArray;
	}

	showDetails() {
		this.onShowDetails.emit(this.warrantData);
	}
}
