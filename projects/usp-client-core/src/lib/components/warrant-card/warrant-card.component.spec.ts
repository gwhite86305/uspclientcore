import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WarrantCardComponent } from './warrant-card.component';

describe('WarrantCardComponent', () => {
  let component: WarrantCardComponent;
  let fixture: ComponentFixture<WarrantCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WarrantCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WarrantCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
