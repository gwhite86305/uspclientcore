import { Component, Input } from '@angular/core';

/**
* NpmBadge component -- Shows a small NPM style badge with content based on the following Inputs:
* @param badgelabel (badgelabel) The right-hand label of the badge.
* @param badgevalue (badgevalue) The left-hand content of the badge.
* @param badgeclass (badgeclass) Shorthand for one of the classes defined in the ngClass directive.
*/
@Component({
	selector: "npm-badge",
	styleUrls: ['./npm-badge.style.css'],
	templateUrl: './npm-badge.view.html'
})
export class NpmBadge {
	@Input()
	id: string = '';
	@Input()
	badgelabel: string = "";
	@Input()
	badgevalue: string = "";
	@Input()
	badgeclass: string = "";

	constructor() {
	}
}
