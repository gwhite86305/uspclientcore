import { Component, Input, Output, EventEmitter } from '@angular/core';
const _ = require('lodash');

@Component({
	selector: "result-total-card",
	templateUrl: './result-total-card.view.html',
	styleUrls: ['./result-total-card.styles.css']
})
export class ResultTotalCard {	
	@Input()
	resultCategory: string;
	@Input()
	resultTotal: number;
	@Input()
	categoryImageSrc: string;
	@Output()
	onClick: EventEmitter<any> = new EventEmitter<any>();

	constructor() {
		
	}
}