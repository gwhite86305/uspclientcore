import { Component, OnInit, Input } from '@angular/core';

@Component({
	selector: 'collapsible-card-deck',
	templateUrl: './collapsible-card-deck.component.html',
	styleUrls: ['./collapsible-card-deck.component.css']
})
export class CollapsibleCardDeck implements OnInit {
	@Input()
	deckName: string;

	collapsed: boolean = false;

	constructor() { }

	ngOnInit() {
	}

}
