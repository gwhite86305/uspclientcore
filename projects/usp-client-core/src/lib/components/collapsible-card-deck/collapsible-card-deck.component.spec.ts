import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CollapsibleCardDeckComponent } from './collapsible-card-deck.component';

describe('CollapsibleCardDeckComponent', () => {
  let component: CollapsibleCardDeckComponent;
  let fixture: ComponentFixture<CollapsibleCardDeckComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CollapsibleCardDeckComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CollapsibleCardDeckComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
