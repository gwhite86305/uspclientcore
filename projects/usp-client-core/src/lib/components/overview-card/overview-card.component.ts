import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';
import { OverviewCardField } from '../../search-types';
const _ = require('lodash');

@Component({
	selector: 'overview-card',
	templateUrl: './overview-card.component.html',
	styleUrls: ['./overview-card.component.css']
})
export class OverviewCard implements OnInit {
	@Input()
	getPhoto: Observable<any>;
	@Input()
	cardTitle: string = '';
	@Input()
	cardSubtitle: string = '';
	@Input()
	cardFields: OverviewCardField[] = [];
	@Input()
	isCompact: boolean = true;

	@Output()
	onPin: EventEmitter<any> = new EventEmitter<any>();
	@Output()
	onDetails: EventEmitter<any> = new EventEmitter<any>();

	photoSrc: string;
	rows: OverviewCardField[] = [];

	constructor() {

	}

	ngOnInit() {
		if (this.getPhoto)
			this.getPhoto.subscribe((photoSrc) => this.photoSrc = photoSrc);
		// if (this.isCompact)
		this.rows = _.chunk(this.cardFields, 2);
		// else
		// 	this.rows = this.cardFields;
	}
}
