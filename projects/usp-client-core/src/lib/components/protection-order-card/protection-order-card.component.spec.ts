import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProtectionOrderCardComponent } from './protection-order-card.component';

describe('ProtectionOrderCardComponent', () => {
  let component: ProtectionOrderCardComponent;
  let fixture: ComponentFixture<ProtectionOrderCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProtectionOrderCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProtectionOrderCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
