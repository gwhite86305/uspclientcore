import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ProtectionOrderResult, OverviewCardField, LETSResult } from '../../search-types';
import { Observable } from 'rxjs';
import * as momentNs from 'moment';
const moment = momentNs;
const _ = require('lodash');

@Component({
  selector: 'protection-order-card',
  templateUrl: './protection-order-card.component.html',
  styleUrls: ['./protection-order-card.component.css']
})
export class ProtectionOrderCard implements OnInit {
	@Input()
	protectionOrderData: ProtectionOrderResult = ProtectionOrderResult.getTestData();
	@Output()
	onShowDetails: EventEmitter<LETSResult> = new EventEmitter<LETSResult>();

	cardFields: OverviewCardField[];
	photoObservable: Observable<string> = Observable.create(observer => {
		observer.next('./assets/images/empty photo.jpg');
	});
	headerText: string;
	subHeaderText: string;

	constructor() { }

	ngOnInit() {
		//Format header and subheader fields
		this.headerText = this.protectionOrderData.defendant;
		this.subHeaderText = 'County: ' + this.protectionOrderData.county;

		//Format input fields
		let fieldArray = [];
		fieldArray.push({ fieldKey: 'Date Issued', fieldValue: moment(new Date(this.protectionOrderData.dateIssued)).format('MM/DD/YYYY') });
		fieldArray.push({ fieldKey: 'Date Served', fieldValue: moment(new Date(this.protectionOrderData.dateIssued)).format('MM/DD/YYYY') });
		fieldArray.push({ fieldKey: 'Plaintiff', fieldValue: this.protectionOrderData.plaintiff });
		fieldArray.push({ fieldKey: 'Expiration Date', fieldValue: moment(new Date(this.protectionOrderData.expireDate)).format('MM/DD/YYYY') });
		this.cardFields = fieldArray;
	}

	showDetails() {
		this.onShowDetails.emit(this.protectionOrderData);
	}
}
