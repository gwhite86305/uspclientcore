import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailSearchCardComponent } from './detail-search-card.component';

describe('DetailSearchCardComponent', () => {
  let component: DetailSearchCardComponent;
  let fixture: ComponentFixture<DetailSearchCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailSearchCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailSearchCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
