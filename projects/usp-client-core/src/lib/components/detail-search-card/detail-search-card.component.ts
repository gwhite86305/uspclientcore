import { LocalStorage, WebstorableArray  } from 'ngx-store';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter, OnDestroy } from '@angular/core';
import { SearchCategory } from '../../search-types';
import { IMultiSelectOption } from 'angular-2-dropdown-multiselect';

@Component({
	
	selector: 'detail-search-card',
	templateUrl: './detail-search-card.component.html',
	styleUrls: ['./detail-search-card.component.css']
})
export class DetailSearchCard implements OnInit {

    isHidden: boolean = true;

    myForm:FormGroup;
    detailCard:FormGroup;
    disabled: boolean = false;
    ShowFilter: boolean = true;
    limitSelection: boolean = false;
    searchCategories: any[] = [];
    selectedItems: any[] = [];
    dropdownSettings: any = {};
	
	@Input()
	categories: SearchCategory[];
	@Output()
	onSearchSelected: EventEmitter<SearchCategory> = new EventEmitter<SearchCategory>();

	optionsModel: number[];
    myOptions: IMultiSelectOption[];
 
    constructor(private fb: FormBuilder) { 
    }

    ngAfterViewInit() {
        console.log("ngAfterViewInit called");
    }    

    ngOnInit() {
        this.searchCategories = [
            { item_id: 1, item_text: 'Person' },
            { item_id: 2, item_text: 'License' },
            { item_id: 3, item_text: 'Name Master' },
            { item_id: 4, item_text: 'Sex Offender' },
            { item_id: 5, item_text: 'DOC' },
            { item_id: 6, item_text: 'Warrant' },
            { item_id: 7, item_text: 'Protection Order' },
            { item_id: 8, item_text: 'Vehicle' },
            { item_id: 9, item_text: 'VIN' },
            { item_id: 10, item_text: 'Boat' },
            { item_id: 11, item_text: 'AlaSafe' },
            { item_id: 12, item_text: 'Citation' },
        ];
        this.selectedItems = [{ item_id: 4, item_text: 'Sex Offender' }, { item_id: 6, item_text: 'Warrant' }];
        this.dropdownSettings = {
            singleSelection: false,
            idField: 'item_id',
            textField: 'item_text',
            selectAllText: 'Select All',
            unSelectAllText: 'UnSelect All',
            itemsShowLimit: 8,
            allowSearchFilter: this.ShowFilter
        };
        this.myForm = this.fb.group({
            searchItem: [this.selectedItems]
        });
    }
    ngOnDestroy() { 
        console.log("OnDestroy called");
    //     @LocalStorage() searchSettings: Array<any>  = this.dropdownSettings;
     }


    onItemSelect(item: any) {
        console.log('onItemSelect', item);
    }
    onSelectAll(items: any) {
        console.log('onSelectAll', items);
    }
    toogleShowFilter() {
        this.ShowFilter = !this.ShowFilter;
        this.dropdownSettings = Object.assign({}, this.dropdownSettings, { allowSearchFilter: this.ShowFilter });
    }

    handleLimitSelection() {
        if (this.limitSelection) {
            this.dropdownSettings = Object.assign({}, this.dropdownSettings, { limitSelection: 2 });
        } else {
            this.dropdownSettings = Object.assign({}, this.dropdownSettings, { limitSelection: null });
        }
    }
    toggleShowHide() {
     this.isHidden = !this.isHidden;   
     //     @LocalStorage() searchSettings: Array<any>  = this.dropdownSettings;
    }

}	
	