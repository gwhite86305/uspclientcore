import { Component, Input, Output, EventEmitter, OnInit, OnChanges, SimpleChanges } from '@angular/core';
const _ = require('lodash');
import { StatusModelService, StatusModel, StatusConfigSection } from '../../services/status-model.service';

@Component({
	selector: "result-tab",
	templateUrl: './result-tab.view.html',
	styleUrls: ['./result-tab.styles.css'],
})
export class ResultTab implements OnInit, OnChanges {	
	@Input()
	selected: boolean;
	@Input()
	headerLabel: string = 'Overview';
	@Input()
	subHeaderLabel: string = 'Results';
	@Input()
	subHeaderValue: string = '';
	@Input()
	newResults: boolean;
	@Output()
	newResultsChange: EventEmitter<boolean> = new EventEmitter<boolean>();
	@Output()
	onClick: EventEmitter<any> = new EventEmitter<any>();

	private searchresultsStatusClass: StatusConfigSection;

	constructor() {

	}

	ngOnInit() {
		this.searchresultsStatusClass = StatusModelService.searchresultsStatusClass;
	}

	ngOnChanges(changes: SimpleChanges) {
		let currentValue = _.get(changes, 'selected.currentValue', false);
		let previousValue = _.get(changes, 'selected.previousValue', false);

		if(currentValue && currentValue != previousValue) {
			this.newResults = false;
		}
	}

	statusClass(model: StatusModel) {
		if (!this.searchresultsStatusClass)
			return '';

		model = StatusModelService.asStatusObject(model);
		return this.searchresultsStatusClass[model.status] || this.searchresultsStatusClass["unknown"];
	}
}