import { Component, Input } from '@angular/core';
import { TourService } from '../../services/tour.service';

@Component({
	selector: "[tour-button]",
	templateUrl: './tour-button.view.html'
})
export class TourButton {
	constructor() {

	}

	startTour() {
		TourService.forceStart();
	}
}
