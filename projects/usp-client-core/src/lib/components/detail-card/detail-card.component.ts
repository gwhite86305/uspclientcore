import { Component, OnInit, Input } from '@angular/core';
import * as _ from 'lodash';

@Component({
	selector: 'detail-card',
	templateUrl: './detail-card.component.html',
	styleUrls: ['./detail-card.component.css']
})
export class DetailCard implements OnInit {
	@Input()
	headerText: string;
	@Input()
	data: any;

	isCollapsed: boolean = false;
	formattedData: any[] = [];

	constructor() { }

	ngOnInit() {
		_.forOwn(this.data, (value, key) => {
			this.formattedData.push({ key: key, value: value });
		});
	}
}
