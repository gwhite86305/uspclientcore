import { Component, Input, Output, EventEmitter } from '@angular/core';
const _ = require('lodash');

import { SearchTab } from '../../search-types';

@Component({
	selector: "li[query-tab]",
	templateUrl: './query-tab.view.html',
	styleUrls: ['./query-tab.styles.css'],
})
export class QueryTab {
	@Input()
	tab: SearchTab;
	@Input()
	selected: boolean = false;
	@Input()
	highlightClass: string = 'info';
	@Input()
	disabled: boolean = false;
	@Output()
	onClick: EventEmitter<any> = new EventEmitter<any>();
	@Output()
	onClose: EventEmitter<any> = new EventEmitter<any>();

	constructor() {

	}
}