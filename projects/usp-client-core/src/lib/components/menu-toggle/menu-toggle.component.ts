import { Component, OnInit, Input, Output, EventEmitter, HostListener } from '@angular/core';

@Component({
	selector: '[menu-toggle]',
	templateUrl: './menu-toggle.component.html',
	styleUrls: ['./menu-toggle.component.css']
})
export class MenuToggle implements OnInit {
	@Input()
	menuAvailable: boolean = false;
	@Output()
	onToggle: EventEmitter<any> = new EventEmitter<any>();

	constructor() { }

	ngOnInit() {
	}

	@HostListener('click', ['$event.target'])
	toggle() {
		this.onToggle.emit();
	}
}
