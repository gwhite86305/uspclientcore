import { Component, HostListener } from '@angular/core';
import { trigger, state, style,	animate, transition } from '@angular/animations';

@Component({ 
	selector: '[privacy-screen]',
	templateUrl: './privacy-screen.view.html',
	styleUrls: ['./privacy-screen.styles.css'],
	animations: [
		trigger('privacyState', [
			state('inactive', style({
				height: '0%'
			})),
			state('active', style({
				height: '98%'
			})),
			transition('inactive => active', animate('250ms ease-in')),
			transition('active => inactive', animate('250ms ease-out'))
		])
	]
})
export class PrivacyScreen {
	showScreen: boolean = false;

	@HostListener('click', ['$event.target'])
	onToggle(btn) {
		this.showScreen = !this.showScreen;
	}

    constructor() {

    }
}