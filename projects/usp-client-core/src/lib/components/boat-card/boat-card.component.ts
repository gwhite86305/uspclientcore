import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { BoatResult, OverviewCardField, LETSResult } from '../../search-types';
import { Observable } from 'rxjs';
import * as momentNs from 'moment';
const moment = momentNs;
const _ = require('lodash');

@Component({
  selector: 'boat-card',
  templateUrl: './boat-card.component.html',
  styleUrls: ['./boat-card.component.css']
})
export class BoatCard implements OnInit {
	@Input()
	boatData: BoatResult = BoatResult.getTestData();
	@Output()
	onShowDetails: EventEmitter<LETSResult> = new EventEmitter<LETSResult>();

	cardFields: OverviewCardField[];
	photoObservable: Observable<string> = Observable.create(observer => {
		observer.next('./assets/images/empty photo.jpg');
	});
	headerText: string;
	subHeaderText: string;

	constructor() { }

	ngOnInit() {
		//Format header and subheader fields
		this.headerText = this.boatData.ownerName;
		this.subHeaderText = 'Reg #: ' + this.boatData.regNumber;

		//Format input fields
		let fieldArray = [];
		fieldArray.push({ fieldKey: 'Boat Class', fieldValue: this.boatData.boatClass });
		fieldArray.push({ fieldKey: 'Boat Make', fieldValue: this.boatData.boatMake });
		fieldArray.push({ fieldKey: 'Boat Length', fieldValue: this.boatData.boatLength });
		fieldArray.push({ fieldKey: 'Boat Year', fieldValue: this.boatData.boatYear });
		this.cardFields = fieldArray;
	}

	showDetails() {
		this.onShowDetails.emit(this.boatData);
	}
}
