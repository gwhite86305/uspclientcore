import { Component, Input, Output, EventEmitter, OnInit, OnChanges, SimpleChanges, ViewChild } from '@angular/core';
import { DatatableComponent } from '@swimlane/ngx-datatable';
import * as momentNs from 'moment';
const moment = momentNs;
const _ = require('lodash');
const $ = require('jquery');
const scrollTo = require('jquery.scrollto');

import { SearchListObj, CitationModel, AtlasQueryLogModel } from '../../search-types';
import { ThemeService } from '../../services/theme.service';
import { FormatTransferStatusPipe } from '../../pipes/citation-format.pipes';

const MAX_LOG_ENTRIES = 100;

@Component({
	selector: "citation-overview",
	templateUrl: './citation-overview.view.html',
	styleUrls: ['./citation-overview.styles.css'],
	providers: [FormatTransferStatusPipe]
})
export class CitationOverview implements OnInit, OnChanges {
	_ = _;
	cachedHistoryData: AtlasQueryLogModel[] = [];	

	@Input()
	searchTerms: SearchListObj[];
	@Input()
	searchDate: Date;
	@Input()
	citations: CitationModel[];
	@Input()
	totalCitations: number;
	@Input()
	loading: boolean = false;
	@Input()
	selectedResultTab: string | number;
	@Input()
	getHistoryMethod: Function; //Returns an observable of history results
	@Input()
	showHistory: boolean = false;

	@Output()
	launchNewQuery: EventEmitter<SearchListObj[]> = new EventEmitter<SearchListObj[]>();
	@Output()
	selectedResultTabChange: EventEmitter<string | number> = new EventEmitter<string | number>();
	@Output()
	onPage: EventEmitter<any> = new EventEmitter<any>();
	@Output()
	onSort: EventEmitter<any> = new EventEmitter<any>();
	@Output()
	onPrint: EventEmitter<any> = new EventEmitter<any>();
	@Output()
	onQueryLogUpdate: EventEmitter<any> = new EventEmitter<any>();

	@ViewChild(DatatableComponent) historyTable: DatatableComponent;
	historyFilter: string = '';
	pageNum: number = 0;
	showHistoryNoteTable: boolean = false;
	historyData: AtlasQueryLogModel[] = [];
	queryLogNote: string = '';
	maxHistoryItems: number = MAX_LOG_ENTRIES;
	themeMode: string = 'day';
	latestWidth: number;
	transferStatusPipe: FormatTransferStatusPipe;

	constructor(_transferStatusPipe: FormatTransferStatusPipe) {
		this.transferStatusPipe = _transferStatusPipe;
	}

	ngOnInit() {
		//Remember page number for tab switching scenarios
		let lastPageNum = _.find(this.searchTerms, ['name', 'PageNo']);
		this.pageNum = lastPageNum ? Number(lastPageNum.value - 1) : 0;

		//Setup component to respond to theme changes and pull the initial theme mode value
		ThemeService.onThemeChange.subscribe((mode: string) => this.themeModeChanged(mode));
		this.themeMode = ThemeService.isDayTheme() ? 'day' : 'night';
	}

	ngOnChanges(changes: SimpleChanges) {
		let searchDateChange = changes['searchDate'];

		if (searchDateChange && searchDateChange.currentValue != searchDateChange.previousValue) {
			let lastPageNum = _.find(this.searchTerms, ['name', 'PageNo']);
			this.pageNum = lastPageNum ? Number(lastPageNum.value - 1) : 0;
		}
	}

	// ngOnDestroy() {
	// 	ThemeService.onThemeChange.unsubscribe();
	// }

	themeModeChanged(newMode: string) {
		this.themeMode = newMode;
	}

	page(event) {
		this.scrollSnap();
		this.onPage.emit(event);
	}

	sort(event) {
		this.scrollSnap();
		this.onSort.emit(event);
	}

	private scrollSnap() {
		let win = $(window);
		let viewport = {
			top: win.scrollTop(),
			left: win.scrollLeft(),
			right: win.scrollLeft() + win.width(),
			bottom: win.scrollTop() + win.height()
		};

		let componentBounds = $('#citationOverviewTable').offset();
		componentBounds.right = componentBounds.left + $('#citationOverviewTable').outerWidth();
		componentBounds.bottom = componentBounds.top + $('#citationOverviewTable').outerHeight();

		if ((viewport.right <= componentBounds.left || viewport.left > componentBounds.left) || (viewport.bottom <= componentBounds.top || viewport.top > componentBounds.top))
			scrollTo('#citationOverviewContainer');
	}

	ensureDate(d) {
		// When search history gets serialized, dates get turned into strings. Make sure this is a date and not a string
		return new Date(d);
	}

	selectResultTab(index: string | number) {
		this.selectedResultTab = index;
		this.selectedResultTabChange.emit(index);
	}

	runNewQuery(citation: CitationModel, queryType: string) {
		let newQuery: SearchListObj[] = [];

		switch (queryType) {
			case 'vehicle':
				newQuery = [{ name: 'VehTagNum', value: citation.VehTagNum, type: 'vehicle' },
				{ name: 'VehTagState', value: citation.VehTagState, type: 'vehicle' }];
				break;
			case 'person':
				newQuery = [{ name: 'DefendantLastName', value: citation.DefendantLastName, type: 'person' },
				{ name: 'DefendantFirstName', value: citation.DefendantFirstName, type: 'person' },
				{ name: 'DefendantGender', value: citation.DefendantGender, type: 'person' },
				{ name: 'DefendantDOB', value: citation.DefendantDOB, type: 'person' }];
				break;
			case 'license':
				newQuery = [{ name: 'LicNum', value: citation.LicNum, type: 'license' },
				{ name: 'LicState', value: citation.LicState, type: 'license' }]
				break;
			default:
				newQuery = [];
				break;
		}

		this.launchNewQuery.emit(newQuery);
	}

	toggleHistoryView() {
		if (!this.showHistoryNoteTable) {
			this.loading = true;
			this.showHistoryNoteTable = true;

			let newQuery: SearchListObj[] = [];
			let state: string = _.get(_.find(this.searchTerms, ['name', 'VehTagState']), 'value', '');
			let tag: string = _.get(_.find(this.searchTerms, ['name', 'VehTagNum']), 'value', '');

			newQuery.push({ name: 'State', value: state, type: 'vehicle' },
				{ name: 'VehTagNum', value: tag, type: 'vehicle' },
				{ name: 'MaxLogEntries', value: this.maxHistoryItems.toString(), type: 'vehicle' });

			this.getHistoryMethod(newQuery).subscribe(historyData => {
				this.cachedHistoryData = historyData;
				this.historyData = _.cloneDeep(historyData);
				this.loading = false;
			});
		}
		else {
			this.showHistoryNoteTable = false;
			this.historyData = [];
			this.cachedHistoryData = [];
			this.maxHistoryItems = MAX_LOG_ENTRIES;
		}
	}

	filterHistoryResults(event) {
		let filter: string = event.target.value.toLowerCase();
		this.historyTable.offset = 0;

		if (!filter) {
			this.historyData = this.cachedHistoryData;
		}
		else {
			this.historyData = _.filter(this.cachedHistoryData, (queryLog: AtlasQueryLogModel) => {
				let formattedDateString = queryLog.DateSearched ? moment(queryLog.DateSearched).format('M/D/YYYY, h:mm A').toLowerCase() : '';
				let serializedQueryLog = this.serializeQueryLog(queryLog).toLowerCase();
				let agencyName = queryLog.AgencyName ? queryLog.AgencyName.toLowerCase() : '';
				let note = queryLog.QueryLogNote ? queryLog.QueryLogNote.toLowerCase() : '';

				if (formattedDateString.includes(filter) ||	serializedQueryLog.includes(filter) ||
					agencyName.includes(filter) || note.includes(filter))
						return true;
				else
					return false;
			});
		}
	}

	serializeQueryLog(queryLog: AtlasQueryLogModel): string {
		let serializedQueryLog = '';

		switch (queryLog.QueryType) {
			case 'vehicle':
				serializedQueryLog = queryLog.State.toUpperCase() + ', ' + queryLog.VehTagNum.toUpperCase();
				break;
			case 'person':
				serializedQueryLog = queryLog.DefendantLastName.toUpperCase() + ', ' +
					queryLog.DefendantFirstName.toUpperCase() + ' (' +
					queryLog.DefendantGender.toUpperCase() + ') ' +
					moment(queryLog.DefendantDOB).format('MM/DD/YYYY');
				// this.dateFromSQLPipe.transform(queryLog.DefendantDOB);
				break;
			case 'dln':
				serializedQueryLog = queryLog.State.toUpperCase() + ', ' + queryLog.LicNum.toUpperCase();
				break;
			default:
				break;
		}

		return serializedQueryLog;
	}

	submitQueryLogNote() {
		let formattedNote = this.queryLogNote.trim();
		let queryLog = new AtlasQueryLogModel();
		Object.assign(queryLog, this.cachedHistoryData[0]);

		if (!formattedNote || !queryLog)
			return;

		queryLog.QueryLogNote = formattedNote;
		this.onQueryLogUpdate.emit(queryLog);

		//Update the note in the full result set first, then update it in the potentially filtered result set if possible.
		this.cachedHistoryData[0].QueryLogNote = formattedNote;
		let displayRow = _.find(this.historyData, ['QueryLogID', this.cachedHistoryData[0].QueryLogID]);
		if (displayRow) displayRow.QueryLogNote = formattedNote;
		this.historyData = [...this.historyData];
		this.queryLogNote = '';
	}

	canLoadMoreHistoryResults(): boolean {
		if (this.maxHistoryItems <= this.historyData.length)
			return true;
		else
			return false;
	}

	loadMoreHistoryResults() {
		this.loading = true;
		this.maxHistoryItems += MAX_LOG_ENTRIES;
		let newQuery: SearchListObj[] = [];
		let state: string = _.get(_.find(this.searchTerms, ['name', 'VehTagState']), 'value', '');
		let tag: string = _.get(_.find(this.searchTerms, ['name', 'VehTagNum']), 'value', '');

		newQuery.push({ name: 'State', value: state, type: 'vehicle' },
			{ name: 'VehTagNum', value: tag, type: 'vehicle' },
			{ name: 'MaxLogEntries', value: this.maxHistoryItems.toString(), type: 'vehicle' });

		this.getHistoryMethod(newQuery).subscribe(historyData => {
			this.cachedHistoryData = historyData;
			this.historyData = _.cloneDeep(historyData);
			this.loading = false;
		});
	}
}