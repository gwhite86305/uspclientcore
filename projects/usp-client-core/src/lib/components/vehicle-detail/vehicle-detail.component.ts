import { Component, Input, EventEmitter, OnInit } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';
const _ = require('lodash');
const $ = require('jquery');

import { SocketResponse } from '../../search-types';

/**
 * VehicleDetail component -- Shows the detailed data for a selected vehicle.
 * @param model (model) The VehicleModel object containing the vehicle details.
 */
@Component({
	selector: "vehicle-detail",
	templateUrl: './vehicle-detail.view.html',
	styleUrls: ['./vehicle-detail.styles.css'],
})
export class VehicleDetail implements OnInit {
	@Input()
	responses: SocketResponse[];
	@Input()
	model: SocketResponse;

	tableExpandEmitter: EventEmitter<any> = new EventEmitter();
	tableToggleEmitter: EventEmitter<any> = new EventEmitter();
	headerText: string = '';
	cardOpenStatus: boolean = true;
	isPanelStatusIconRequired: boolean = false;

	constructor(private notifService: NotificationsService) {

	}

	ngOnInit() {
		this.headerText = 'Results for :: ';
		_.forEach(this.model.query.fields, (field) => {
			this.headerText += '{ ' + field.name + ': ' + field.value + ' }, ';
		});
	}

	getSourcesForField(fieldName) {
		let sources = this.responses;
		return _.reduce(sources, (results: any[], source: SocketResponse) => {
			let sourceKeyVal = {
				name: source.responseSource,
				value: eval('source.vehicle.' + fieldName)
			};
			results.push(sourceKeyVal);
			return results;
		}, []);
	}

	toggleSourceTables() {
		this.tableToggleEmitter.emit();
	}

	openAllCards(cards: any[]) {
		this.tableExpandEmitter.emit();

		_.forEach(cards, (card) => {
			card.open();
		});
		this.notifService.remove();
	}
}
