import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { ThreatLevel, ThreatBadge } from '../../search-types';
const $ = require('jquery');
const _ = require('lodash');

@Component({
	selector: 'threat-bar',
	templateUrl: './threat-bar.view.html',
	styleUrls: ['./threat-bar.styles.css']
})
export class ThreatBar implements OnInit, OnDestroy {
	ThreatLevel = ThreatLevel;

	@Input()
	public threatLevel: ThreatLevel = ThreatLevel.Default;
	@Input()
	public threatBadges: ThreatBadge[] = [];
	@Input()
	public playThreatAudio: Subject<any> = new Subject<any>();
	@Input()
	public loading: boolean = false;
	@Input()
	public volume: number = 0.6;
	@Input()
	public audioDir: string;

	constructor() {

	}

	ngOnInit() {
		this.playThreatAudio.subscribe((threatLevel) => this.playAudio(threatLevel));
	}

	ngOnDestroy() {
		this.playThreatAudio.unsubscribe();
	}

	padCounter(count: number): string {
		let paddedCounter = count.toString();
		if (paddedCounter.length == 2)
			return paddedCounter;
		else if (paddedCounter.length == 1)
			return '0' + paddedCounter;
	}

	playAudio(threatLevel: ThreatLevel) {
		let audioSrc = this.audioDir;
		switch (threatLevel) {
			case ThreatLevel.None:
				audioSrc += 'green.wav';
				break;
			case ThreatLevel.Low:
				audioSrc += 'yellow.wav';
				break;
			case ThreatLevel.High:
				audioSrc += 'red.wav';
				break;
			default:
				return;
		}

		this.volume = localStorage.getItem('AtlasVolume') ? parseFloat(localStorage.getItem('AtlasVolume')) : 0.6;

		let audio = new Audio(audioSrc);
		audio.volume = this.volume;
		audio.play();
	}
}

