import { Component, Input, Output, EventEmitter } from '@angular/core';
import * as momentNs from 'moment';
const moment = momentNs;
const _ = require('lodash');

import { ResultCounts, OVERVIEW, CARRIERS, PEOPLE, VEHICLES, SearchListObj } from '../../search-types';

@Component({
	selector: "overview-detail",
	templateUrl: './overview-detail.view.html',
	styleUrls: ['./overview-detail.styles.css'],
})
export class OverviewDetail {	
	OVERVIEW = OVERVIEW;
	CARRIERS = CARRIERS;
	PEOPLE = PEOPLE;
	VEHICLES = VEHICLES;

	@Input()
	searchTerms: SearchListObj[];
	@Input()
	totals: ResultCounts;
	@Input()
	searchDate: Date;
	@Input()
	selectedResultTab: string | number;
	@Output()
	selectedResultTabChange: EventEmitter<string | number> = new EventEmitter<string | number>();

	constructor() {
		
	}

	ensureDate(d) {
		// When search history gets serialized, dates get turned into strings. Make sure this is a date and not a string
		return new Date(d);
	}

	selectResultTab(index: string | number) {
		// Ensure 
		// switch(index) {
		// 	case 'PEOPLE':
		// 		if (this.totals.totalPeople == 0)
		// 			return;
		// 		break;
		// 	case 'VEHICLES':
		// 		if (this.totals.totalVehicles = 0)
		// 			return;
		// 		break;
		// 	case 0:
		// 		if (this.totals.totalCarriers = 0)
		// 			return;
		// 		break;
		// 	default:
		// 		return;
		// }

		this.selectedResultTab = index; 
		this.selectedResultTabChange.emit(index);
	}
}