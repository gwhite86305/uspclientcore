import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NamemasterCardComponent } from './namemaster-card.component';

describe('NamemasterCardComponent', () => {
  let component: NamemasterCardComponent;
  let fixture: ComponentFixture<NamemasterCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NamemasterCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NamemasterCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
