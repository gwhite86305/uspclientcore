import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NameMasterResult, OverviewCardField, LETSResult } from '../../search-types';
import { Observable } from 'rxjs';
import * as momentNs from 'moment';
const moment = momentNs;
const _ = require('lodash');

@Component({
  selector: 'namemaster-card',
  templateUrl: './namemaster-card.component.html',
  styleUrls: ['./namemaster-card.component.css']
})
export class NameMasterCard implements OnInit {
	@Input()
	nameMasterData: NameMasterResult = NameMasterResult.getTestData();
	@Output()
	onShowDetails: EventEmitter<LETSResult> = new EventEmitter<LETSResult>();

	cardFields: OverviewCardField[];
	photoObservable: Observable<string> = Observable.create(observer => {
		observer.next('./assets/images/empty photo.jpg');
	});
	headerText: string;
	subHeaderText: string;

	constructor() { }

	ngOnInit() {
		//Format header and subheader fields
		this.headerText = this.nameMasterData.lastName + ',' + ' ' + this.nameMasterData.firstName + ' ' + this.nameMasterData.middleName;
		this.subHeaderText = 'LIC #' + this.nameMasterData.licenseNumber;

		//Format input fields
		let fieldArray = [];
		fieldArray.push({ fieldKey: 'Race', fieldValue: this.nameMasterData.race });
		fieldArray.push({ fieldKey: 'Sex', fieldValue: this.nameMasterData.sex });
		fieldArray.push({ fieldKey: 'Age', fieldValue: this.nameMasterData.age });
		fieldArray.push({ fieldKey: 'Date of Birth', fieldValue: moment(new Date(this.nameMasterData.dateOfBirth)).format('MM/DD/YYYY') });
		this.cardFields = fieldArray;
	}

	showDetails() {
		this.onShowDetails.emit(this.nameMasterData);
	}
}
