import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
	selector: "accordion-toggle-button",
	templateUrl: './accordion-toggle-button.view.html'
})
export class AccordionToggleButton {
	@Input()
	id: string = '';
	@Output()
	onToggle: EventEmitter<any> = new EventEmitter();

	constructor() {

	}

	toggleAccordions() {
		this.onToggle.emit();
	}
}
