import { Pipe, PipeTransform } from '@angular/core';
import * as momentNs from 'moment';
const moment = momentNs;

@Pipe({name: 'toLocalDateTimeFromSQL'})
export class LocalDateTimeFromSQLPipe implements PipeTransform {
	transform(value: string): string {
		if (!value) 
			return '';

		let nonUtcValue = value.replace('Z', '');
		if (moment(nonUtcValue).isValid()) 
			return moment(nonUtcValue).format('M/D/YYYY h:mm A');		
	}
}
@Pipe({name: 'toLocalDateFromSQL'})
export class LocalDateFromSQLPipe implements PipeTransform {
	transform(value: string): string {
		if (!value) 
			return '';

		let nonUtcValue = value.replace('Z', '');
		if (moment(nonUtcValue).isValid())
			return moment(nonUtcValue).format('M/D/YYYY');		
	}
}

@Pipe({name: 'formatIfDate'})
export class FormatIfDatePipe implements PipeTransform {
	transform(value: string): string {
		if (moment(value, moment.ISO_8601).isValid()) {
			//Input was a valid ISO datestring, so format it.
			return moment(value).format('M/D/YYYY');
		}
		else {
			//Non-date input; simply pass through.
			return value;
		}
	}
}