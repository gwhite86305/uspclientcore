import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
const _ = require('lodash');

// export class DocStatus {
// 	StatusNum: number;
// 	StatusName: string;
// }

// @Pipe({name: 'toDocStatusString'})
// export class DocStatusFromCodePipe implements PipeTransform {
// 	transform(value: string, mappings: DocStatus[]): string {
// 		let status = _.find(mappings, ['StatusNum', value]);
// 		return status ? value + ' - ' + status.StatusName : value;
// 	}
// }

@Pipe({name: 'toFormattedTransferStatus'})
export class FormatTransferStatusPipe implements PipeTransform {
	transform(value: string): string {
		let status = '';

		switch(value) {
			case 'COURTREADY':
				status = 'Court Ready';
				break;
			case 'ATCOURT':
				status = 'At Court';
				break;
			case 'VOID':
				status = 'Void';
				break;
			case 'REVIEW':
				status = 'Review';
				break;
		}

		return status;
	}
}