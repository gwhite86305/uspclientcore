import { Injectable, EventEmitter } from '@angular/core';
const _ = require('lodash');

const STORAGE_CURRENT_THEME = "currentTheme";
export class Theme {
	name: string;
	src: string;
	bg: string;
	primary: string;
	secondary: string;
	success: string;
	info: string;
	warning: string;
	danger: string;
	link: string;
	font: string;
}

/**
 * ThemeService (Injectable) class -- Handles switching of the various application themes.
 */
// @dynamic
@Injectable()
export class ThemeService {
	private static dayTheme: string;
	private static nightTheme: string;

	public static themes: Theme[];
	public static defaultTheme: string;
	public static currentTheme: string;
	public static onThemeChange: EventEmitter<string> = new EventEmitter<string>();

	constructor() {

	}

	static setup(configData: any) {
		ThemeService.dayTheme = configData.dayTheme;
		ThemeService.nightTheme = configData.nightTheme;

		ThemeService.defaultTheme = configData.defaultTheme;
		ThemeService.themes = configData.themes;

		let localTheme = localStorage.getItem(STORAGE_CURRENT_THEME);
		if (ThemeService.getThemeByName(localTheme))
			ThemeService.currentTheme = localStorage.getItem(STORAGE_CURRENT_THEME);

		if (!ThemeService.currentTheme)
			ThemeService.currentTheme = ThemeService.defaultTheme;

		ThemeService.setThemeByName(ThemeService.currentTheme);
	}

	static getThemeByName(name: string): Theme {
		return _.find(ThemeService.themes, ['name', name]);
	}

	static setThemeByName(name) {
		let theme = ThemeService.getThemeByName(name);

		if (!theme) {
			console.log("Tried to set invalid theme: ", name);
			return false;
		}

		ThemeService.currentTheme = name;
		document.getElementById("theme-css")['href'] = theme.src;
		localStorage.setItem(STORAGE_CURRENT_THEME, name);
		return true;
	}

	static toggleDayNight(dayMode: boolean) {
		let mode = 'day';

		if (dayMode) {
			ThemeService.setThemeByName(ThemeService.dayTheme);
			mode = 'day';
		}
		else {
			ThemeService.setThemeByName(ThemeService.nightTheme);
			mode = 'night';
		}

		this.onThemeChange.emit(mode);
	}

	static isDayTheme(): boolean {
		if (ThemeService.currentTheme != ThemeService.nightTheme)
			return true;
		else
			return false;
	}
}
