import { Injectable } from '@angular/core';
const _ = require('lodash');
const hopscotch = require('hopscotch');

/**
 * TourService (Injectable) class -- Handles the tours on first load.
 */
// @dynamic
@Injectable()
export class TourService {
	private static steps: any[] = [];

	constructor() {

	}

	public static setup(appHeader: string, configData: any) {
		TourService.steps = configData;
	}

	public static forceStart() {
		if (TourService.steps)
			hopscotch.startTour(TourService.steps);
	}
}
