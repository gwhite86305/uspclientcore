import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ReplaySubject, Subject, Observable } from 'rxjs';
import { ConfigService } from '../../../../usp-client-core/src/public_api';
import { ServiceStatusAll, NetworkStatus, ServiceStatusModel } from '../search-types';

// @dynamic
@Injectable()
export class NetworkStatusService {
	private static statusStream: Subject<ServiceStatusAll>;
	public statusStream: Subject<ServiceStatusAll>;
	private apiStatusSource = new Subject<NetworkStatus>();
	private internetStatusSource = new Subject<NetworkStatus>();
	private networkStatusSource: Observable<NetworkStatus>;
	public networkAvailable$: ReplaySubject<NetworkStatus>;
	apiUrl: string;

	constructor(private http: HttpClient, private config: ConfigService) {
		// if (!NetworkStatusService.statusStream)
		// 	NetworkStatusService.statusStream = new Subject<ServiceStatusAll>();

		// this.statusStream = NetworkStatusService.statusStream;
		// this.networkAvailable$ = this.networkStatusSource = new ReplaySubject<NetworkStatus>(1);
		// Observable.combineLatest(this.apiStatusSource, this.internetStatusSource, (s1, s2) => Object.assign({}, s2, s1))
		// 	.subscribe(x => this.networkAvailable$.next(x));

		// this.config.configStream.subscribe((config: any) => {
		// 	this.apiUrl = config.apiUrl;
			// this.checkStatus(); // begin updating the status stream
			// setInterval(this.checkStatus.bind(this), 15000);
			// setInterval(this.checkNetwork.bind(this), 15000);
			// setInterval(this.checkInternet.bind(this), 15000);
		// });
	}

	public getStatusStreamFor(section: string) {
		// return this.statusStream.map(status => status[section]);
	}

	private checkInternet() {
		// this.internetStatusSource.next({ internet: navigator.onLine });
	}

	/**
    * CheckNetwork does the actual API call and broadcasts the network status change depending on the response.
    * Can be called by components that consume this service to get an immediate network status update.
    * CheckNetwork will do nothing if the user does not have a sessionId, and it will force an automatic logout when the user's session has expired.
    */
	checkNetwork() {
		// try {
		// 	this.authService.accessTokenStream.first()
		// 		.subscribe(accessToken => {
		// 			var opts = this.authService.getRequestOptionsForAPI(accessToken);

		// 			this.http.get(this.apiUrl + 'auth/status', opts)
		// 				.subscribe((data: any) => {
		// 					if (data.loggedin) {
		// 						this.apiStatusSource.next(this.loggedInStatus());
		// 					}
		// 					else {
		// 						this.authService.logout();
		// 					}
		// 				}, (err) => {
		// 					if (err && err.status == 404) {
		// 						this.apiStatusSource.next(true);
		// 					}
		// 					else if (err && err.status == 0) {
		// 						// net::ERR_NAME_NOT_RESOLVED error
		// 						this.apiStatusSource.next(this.noVPNStatus());
		// 					}
		// 					else {
		// 						console.error('Could not reach API');
		// 						this.apiStatusSource.next(this.noAPIStatus());
		// 					}
		// 				});
		// 		});
		// }
		// catch (err) {
		// 	console.error('Exception encountered while checking network status :: ' + err.message);
		// 	this.apiStatusSource.next(this.noVPNStatus());
		// }
	}

	private checkStatus() {
		// this.authService.accessTokenStream.first()
		// 	.subscribe(accessToken => {
		// 		var opts = this.authService.getRequestOptionsForAPI(accessToken);
		// 		opts.withCredentials = true;
		// 		this.http.get(this.apiUrl + 'status', opts)
		// 			.subscribe((res: any) => {
		// 				NetworkStatusService.statusStream.next(res);
		// 			}, err => {
		// 				NetworkStatusService.statusStream.next({
		// 					API: {
		// 						state: {
		// 							status: 'warning',
		// 							message: 'Not Connected'
		// 						}
		// 					}
		// 				});
		// 			})
		// 	});
	}

	private loggedInStatus(): NetworkStatus {
		return {
			vpn: true,
			api: true,
			loggedin: true,
		};
	}

	private noVPNStatus(): NetworkStatus {
		return {
			vpn: false,
			api: false,
			loggedin: false,
		};
	}

	private noAPIStatus(): NetworkStatus {
		return {
			vpn: true,
			api: false,
			loggedin: false,
		};
	}
}
