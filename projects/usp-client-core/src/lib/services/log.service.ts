import { Injectable, Inject, ErrorHandler, forwardRef } from '@angular/core';
import { ConfigService } from './config.service';

export interface LoggingErrorHandlerOptions {
	rethrowError: boolean;
	unwrapError: boolean;
}
export var LOGGING_ERROR_HANDLER_OPTIONS: LoggingErrorHandlerOptions = {
	rethrowError: false,
	unwrapError: false
};

@Injectable()
export class LogService extends ErrorHandler {
	private apiUrl;
	private options: LoggingErrorHandlerOptions;

	constructor( @Inject(LOGGING_ERROR_HANDLER_OPTIONS) options: LoggingErrorHandlerOptions, private config: ConfigService) {
		super();
		this.options = options;

		try {
			config.configStream.subscribe(config => {
				this.apiUrl = config.apiUrl;
			}, () => { });
		} catch (e) { }
	}

	public handleError(error: any): void {
		// make http call to server to post
		var errstr = this.errorToString(error);
		this._oldschoolPost(this.apiUrl + "errorlog", errstr);

		super.handleError(error);
	}

	private _oldschoolPost(url: string, bodyString: string) {
		// Use raw XHR in case the error is causing angular's http module to also not work!
		var xhr = ((<any>window).XMLHttpRequest) ? new XMLHttpRequest() : new activeXObject("Microsoft.XMLHTTP");
		var data: any;

		if (typeof (FormData) == 'undefined') {
			var boundary = '---------------------------' + (new Date).getTime();
			data = "--" + boundary + "\r\n";
			data += 'Content-Disposition: form-data; name="data"\r\n\r\n';
			data += bodyString + "\r\n";
			data += "--" + boundary + "--\r\n";

			xhr.open('post', url, true);
			xhr.setRequestHeader('Content-Type', 'multipart/form-data; boundary=' + boundary);
		} else {
			data = new FormData();
			data.append("data", bodyString);
			xhr.open('post', url, true);
		}
		xhr.send(data);
	}

	private errorToString(error): string {
		var originalError = this._findOriginalError(error);
		var originalStack = this._findOriginalStack(error);
		var context = this._findContext(error);

		var errorString = "";

		errorString += "EXCEPTION: " + this._extractMessage(error) + "\n";
		if (originalError) {
			errorString += "ORIGINAL EXCEPTION: " + this._extractMessage(originalError) + "\n";
		}
		if (originalStack) {
			errorString += 'ORIGINAL STACKTRACE:' + "\n";
			errorString += originalStack + "\n";
		}
		if (context) {
			errorString += 'ERROR CONTEXT:' + "\n";
			errorString += context + "\n";
		}

		return errorString;
	}

	private _extractMessage(error) {
		return error instanceof Error ? error.message : error.toString();
	}

	private _findContext(error) {
		if (error) {
			return error.context ? error.context :
				this._findContext(error.originalError);
		} else {
			return null;
		}
	}
	private _findOriginalError(error) {
		var e = error.originalError;
		while (e && e.originalError) {
			e = e.originalError;
		}
		return e;
	}
	private _findOriginalStack(error) {
		if (!(error instanceof Error))
			return null;
		var e = error;
		var stack = e.stack;
		while (e instanceof Error && e['originalError']) {
			e = e['originalError'];
			if (e instanceof Error && e.stack) {
				stack = e.stack;
			}
		}
		return stack;
	}
}

export var LOGGING_ERROR_HANDLER_PROVIDERS = [
	{
		provide: LOGGING_ERROR_HANDLER_OPTIONS,
		useValue: LOGGING_ERROR_HANDLER_OPTIONS
	},
	{
		provide: ErrorHandler,
		useClass: LogService
	}
];

declare var activeXObject: any;
