let ipcRenderer;
try {
	// Atempt to load the ipcRenderer, if we're in electron..
	ipcRenderer = (<any>window).require('electron').ipcRenderer;
} catch (e) { }

/**
 * ScreenCapturerService class -- Handles printing the current page to PDF.
 */
// @dynamic
export class ScreenCaptureService {
	// Whether or not electron is being used, so that we can use electron printToPdf
	public static canCapture = !!ipcRenderer;

	constructor() { }

	public static capture() {
		if (!ipcRenderer)
			return; // Not electron.

		// Most of the code is handled in the electron thread, so just send a signal to that to save to pdf!
		ipcRenderer.send('save-as-pdf', {});
	}
}
