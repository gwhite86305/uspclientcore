import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { ReplaySubject, Observable } from 'rxjs';

export interface StatusModel {
	status: "okay" | "warning" | "unknown" | "pending"; // approximately
	message: string;
}

export class StatusConfigSection {
	[status: string]: string;
}

export class StatusConfig {
	statusDefaultMessage: StatusConfigSection;
	carrierDetailStatusLabelClass: StatusConfigSection;
	checkmarkIconClass: StatusConfigSection;
	checkmarkContainerClass: StatusConfigSection;
	searchresultsStatusClass: StatusConfigSection;
	searchresultsStatusPanelClass: StatusConfigSection;
}

export type ValidConfigSections = "statusDefaultMessage" | "carrierDetailStatusLabelClass" | "checkmarkIconClass" | "checkmarkContainerClass" | "searchresultsStatusClass" | "searchresultsStatusPanelClass";

// @dynamic
@Injectable()
export class StatusModelService {
	// public static configStream: ReplaySubject<StatusConfig>;
	public static statusDefaultMessage: StatusConfigSection = <any>{};
	public static carrierDetailStatusLabelClass: StatusConfigSection = <any>{};
	public static checkmarkIconClass: StatusConfigSection = <any>{};
	public static checkmarkContainerClass: StatusConfigSection = <any>{};
	public static searchresultsStatusClass: StatusConfigSection = <any>{};
	public static searchresultsStatusPanelClass: StatusConfigSection = <any>{};

	constructor(private http: HttpClient) {
		// if (!StatusModelService.configStream) {
		// 	StatusModelService.configStream = new ReplaySubject<any>(1);
		// 	this.http.get("config/status-config.json")
		// 		.subscribe((res: any) => {
		// 			StatusModelService.configStream.next(res);

		// 			// Load static config information:
		// 			StatusModelService.statusDefaultMessage = res.statusDefaultMessage;
		// 			StatusModelService.carrierDetailStatusLabelClass = res.carrierDetailStatusLabelClass;
		// 			StatusModelService.checkmarkIconClass = res.checkmarkIconClass;
		// 			StatusModelService.checkmarkContainerClass = res.checkmarkContainerClass;
		// 			StatusModelService.searchresultsStatusClass = res.searchresultsStatusClass;
		// 			StatusModelService.searchresultsStatusPanelClass = res.searchresultsStatusPanelClass;
		// 		})
		// }
		// StatusModelService.configStream = StatusModelService.configStream;
	}

	public static setup(configData: any) {
		// Load static config information:
		StatusModelService.statusDefaultMessage = configData.statusDefaultMessage;
		StatusModelService.carrierDetailStatusLabelClass = configData.carrierDetailStatusLabelClass;
		StatusModelService.checkmarkIconClass = configData.checkmarkIconClass;
		StatusModelService.checkmarkContainerClass = configData.checkmarkContainerClass;
		StatusModelService.searchresultsStatusClass = configData.searchresultsStatusClass;
		StatusModelService.searchresultsStatusPanelClass = configData.searchresultsStatusPanelClass;
	}

	// public getConfigStreamFor(section: ValidConfigSections): Observable<any> {
	// 	return StatusModelService.configStream.map(config => config[section]);
	// }

	/**
	 * The non-stream methods of StatusModelService class require onReady to be finished BEFORE using!!!!
	 */
	// public static onReady(): Observable<any> {
	// 	return StatusModelService.configStream;
	// }

	public static getDefaultMessage(status: string): string {
		return StatusModelService.statusDefaultMessage[status] || StatusModelService.statusDefaultMessage["unknown"];
	}

	public static getDefaultObjForStatus(status: string): StatusModel {
		if (typeof StatusModelService.statusDefaultMessage[status] === "undefined") {
			status = "unknown";
		}

		return <any>{
			status,
			message: StatusModelService.getDefaultMessage(status),
		};
	}

	public static asStatusObject(mystery: any): StatusModel {
		if (mystery === true || mystery === "Y") {
			return StatusModelService.getDefaultObjForStatus("okay");

		} else if (mystery === false || mystery === "N") {
			return StatusModelService.getDefaultObjForStatus("warning");

		} else if (mystery === null || typeof mystery === "undefined" || mystery === "U") {
			return StatusModelService.getDefaultObjForStatus("unknown");

		} else if (typeof mystery === "string") {
			return StatusModelService.getDefaultObjForStatus(mystery);

		} else {
			return Object.assign({}, mystery);
		}
	}

	public static isTrue(model) {
		if (StatusModelService.asStatusObject(model).status === "okay") {
			return true;
		} else {
			return false;
		}
	}

	public static isFalse(model) {
		if (StatusModelService.asStatusObject(model).status === "warning") {
			return true;
		} else {
			return false;
		}
	}

	public static isUnknown(model) {
		if (StatusModelService.asStatusObject(model).status === "unknown") {
			return true;
		} else {
			return false;
		}
	}
}
