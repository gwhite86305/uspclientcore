import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from 'rxjs';

export class CoreConfig {
	themeConfig: {
		bsThemes: { [prop: string]: string };
		mdThemes: { [prop: string]: string };
		bsDayTheme: string;
		bsNightTheme: string;
		mdDayTheme: string;
		mdNightTheme: string;
		defaultBSTheme: string;
		defaultMDTheme: string;
		UIFramework: string;
	};
}

// @dynamic
@Injectable()
export class ConfigService {
	private _copyright: string
	private static configStream: Observable<any>;
	public configStream: Observable<any>;

	constructor(private http: HttpClient) {
		if (!ConfigService.configStream) {
			ConfigService.configStream = this.http.get("usp-client-core/core-config.json");
		}
		this.configStream = ConfigService.configStream;
	}

	static load(): Promise<any> {
		return new Promise((resolve, reject) => {
			ConfigService.configStream
				.subscribe((data) => {
					resolve(data);
				});
		});
	}
};
