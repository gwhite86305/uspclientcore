# USPClientCore
 
USPClientCore is now built on Angular 7 and Bootstrap 4!  Detailed documentation of the available components and services is available [here]().

If further information is required, please contact the library's author at yuri.sero@ua.edu for assistance.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 6.0.3.
