import { Component } from '@angular/core';

@Component({
  selector: 'usp-client-core-test',
  template: `<router-outlet></router-outlet>`
})
export class AppComponent {
	constructor () {

	}
}
