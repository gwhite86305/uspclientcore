import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
import { DatePipe } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap';
import { DialogModule } from 'primeng/dialog';
import { AppComponent } from './app.component';
import { CoreTest } from './core-test/core-test.component';
import { SearchTest } from './search-test/search-test.component';
import { SearchResults } from './search-test/search-results/search-results.component';
import { SearchService } from './search-test/search.service';
import { MobileTest } from './mobile-test/mobile-test.component';
import { MobileDetails } from './mobile-test/mobile-details/mobile-details.component';
import { MobileSearchService } from './mobile-test/mobile-search.service';
import { MobileResults } from './mobile-test/mobile-results/mobile-results.component';
import { MobileSearch } from './mobile-test/mobile-search/mobile-search.component';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { CoreModule, SearchModule } from '../../projects/usp-client-core/src/public_api';

const appRoutes: Routes = [
	{ path: '', redirectTo: 'mobile-test', pathMatch: 'full' },
	{ path: "core-test", component: CoreTest },
	{ path: 'search-test', component: SearchTest },
	{ path: 'mobile-test', component: MobileTest },
	{ path: 'mobile-search/:category', component: MobileSearch },
	{ path: 'mobile-results', component: MobileResults },
	{ path: 'mobile-details', component: MobileDetails }
];

@NgModule({
	declarations: [
		AppComponent,
		CoreTest,
		SearchTest,
		SearchResults,
		MobileTest,
		MobileDetails,
		MobileResults,
		MobileSearch
	],
	imports: [
		BrowserModule,
		CoreModule,
		SearchModule,
		ModalModule.forRoot(),
		RouterModule.forRoot(appRoutes),
		DialogModule
	],
	providers: [SearchService, MobileSearchService, DatePipe],
	bootstrap: [AppComponent]
})
export class AppModule { }
