import { Injectable, Inject, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { ReplaySubject, Subject, Observable } from 'rxjs';
import * as moment from 'moment';
const _ = require('lodash');

import { SearchLoadingProgress } from '../../../projects/usp-client-core/src/public_api';
import { SearchQuery, SearchTab, SearchResult, UniqueIdentifier, SearchListObj, SearchHistoryData, AtlasQueryLogModel, CitationModel } from '../../../projects/usp-client-core/src/public_api';

/**
 * SearchService (Injectable) class -- Handles making API and database calls to retrieve search data.
 */
@Injectable()
export class SearchService {

	history: string[] = [];
	searchApiUrl: string;
	historyApiUrl: string;
	accessToken: string;
	private historydata;
	public static onSearch: EventEmitter<any> = new EventEmitter();
	public static onSearchHistory: EventEmitter<any> = new EventEmitter();
	public static onResultsClose: EventEmitter<any> = new EventEmitter();
	public historyStream: Subject<any>;

	constructor(private http: HttpClient) {

	}

	search(searchTerms: SearchListObj[], tab: SearchTab, queryType: string, getThreats: boolean = false): Promise<SearchResult> {
		return new Promise((resolve, reject) => {
			// if (!this.searchApiUrl) {
			// 	console.error("API URL not loaded from config yet");
			// 	reject(new Error("API URL not loaded from config yet"));
			// }

			var tempResult = new SearchResult({
				queryId: tab.queryId,
				results: [],
				resultCount: 0
			});

			this.correctForDateRange(searchTerms);

			switch (queryType) {
				case 'history':
					this.getSearchHistory(searchTerms, tab, tempResult).then((result: SearchResult) => resolve(result), (err) => reject(err));
					break;
				case 'carrier':
					this.getCarriers(searchTerms, tab, tempResult).then((result: SearchResult) => resolve(result), (err) => reject(err));
					break;
				default:
					this.getCitations(searchTerms, tab, tempResult, getThreats).then((result: SearchResult) => resolve(result), (err) => reject(err));
					break;
			}
		});
	}

	rawSearch(searchTerms: SearchListObj[], queryType: string): Promise<any> {
		return new Promise((resolve, reject) => {

			this.correctForDateRange(searchTerms);

			switch (queryType) {
				case 'history':
					this.getRawHistory(searchTerms).subscribe(historyData => {
						resolve(historyData.result);
					}, (err: any) => {
						console.error(err);
						reject();
					});
					break;
				case 'carrier':
					break;
				default:
					this.getRawCitations(searchTerms).subscribe(data => {
						let citationData = data.result;
						if (!_.has(citationData, 'citations') || citationData.citations.length == 0) {
							resolve([]);
						}
						citationData.citations = this.formatSQLDateTimes(citationData.citations);
						resolve(citationData);
					}, (err: any) => {
						console.error(err);
						reject();
					});
			}
		});
	}

	getRawCitations(searchTerms: SearchListObj[]): Observable<any> {
		let filters = _.reduce(searchTerms, (filterObj: any, searchTerm: SearchListObj) => {
			filterObj[searchTerm.name] = searchTerm.value;
			return filterObj;
		}, {});
		filters['GetThreats'] = false;

		if (filters['SearchType'] && filters['SearchType'] == 'citations') {
			//Special case where we want to run a citation query on the officer's ID, a date range, and a keyword.
			filters = _.omit(filters, 'SearchType');
		}

		return this.http.get('assets/test-data/mock-citations.json');
	}

	getRawHistory(searchTerms: SearchListObj[]): Observable<any> {
		let startDate = _.get(_.find(searchTerms, ['name', 'StartDate']), 'value', null);
		let endDate = _.get(_.find(searchTerms, ['name', 'EndDate']), 'value', null);
		let keyword = _.get(_.find(searchTerms, ['name', 'Keyword']), 'value', null);
		let maxLogEntries = _.get(_.find(searchTerms, ['name', 'MaxLogEntries']), 'value', null);

		let queryLog: AtlasQueryLogModel = new AtlasQueryLogModel();
		queryLog.parseFromSearchTerms(searchTerms);

		return this.http.get('assets/test-data/mock-history.json');
	}

	getCitations(searchTerms: SearchListObj[], tab: SearchTab, tempResult: SearchResult, getThreats: boolean): Promise<SearchResult> {
		return new Promise((resolve, reject) => {
			let filters = _.reduce(searchTerms, (filterObj: any, searchTerm: SearchListObj) => {
				filterObj[searchTerm.name] = searchTerm.value;
				return filterObj;
			}, {});
			filters['GetThreats'] = getThreats;

			if (filters['SearchType'] && filters['SearchType'] == 'citations') {
				//Special case where we want to run a citation query on the officer's ID, a date range, and a keyword.
				filters = _.omit(filters, 'SearchType');
			}

			let logQuery: boolean = _.find(searchTerms, (term: SearchListObj) => {
				switch (term.name) {
					case 'PageNo':
					case 'SortColumn':
					case 'SortOrder':
						return true;
					default:
						return false;
				}
			}) ? false : true;

			this.http.get('assets/test-data/mock-citations.json')
				.subscribe((data: any) => {
					let citationData = data.result;
					if (!_.has(citationData, 'citations') || citationData.citations.length == 0) {
						tempResult.results = [];
						tempResult.resultCount = 0;
						tempResult.charges = [];
						tempResult.chargeExceptions = { ExpiredLicenses: 0, NoLicenses: 0, RecentDUIs: 0 };

						tab.loading = false;
						tab.loadingProgress = SearchLoadingProgress.fullResults;
						resolve(tempResult);
						return;
					}
					citationData.citations = this.formatSQLDateTimes(citationData.citations);

					tempResult.results = citationData.citations;
					tempResult.resultCount = citationData.filteredRowsCount;
					tempResult.charges = citationData.charges;
					tempResult.chargeExceptions = citationData.chargeExceptions;

					tab.loading = false;
					tab.loadingProgress = SearchLoadingProgress.fullResults;
					resolve(tempResult);
				}, (err: any) => {
					console.error(err);
					reject(err);
				});
		});
	}

	getSearchHistory(searchTerms: SearchListObj[], tab: SearchTab, tempResult: SearchResult,): Promise<SearchResult> {
		return new Promise((resolve, reject) => {
			let startDate = _.get(_.find(searchTerms, ['name', 'StartDate']), 'value', null);
			let endDate = _.get(_.find(searchTerms, ['name', 'EndDate']), 'value', null);
			let keyword = _.get(_.find(searchTerms, ['name', 'Keyword']), 'value', null);
			let maxLogEntries = _.get(_.find(searchTerms, ['name', 'MaxLogEntries']), 'value', null);

			let queryLog: AtlasQueryLogModel = new AtlasQueryLogModel();
			queryLog.parseFromSearchTerms(searchTerms);
			queryLog = _.omit(queryLog, 'QueryType');

			this.http.get('assets/test-data/mock-history.json')
				.subscribe((historyData: any) => {
					if (!historyData.result || historyData.result.length == 0) {
						tab.loading = false;
						tab.loadingProgress = SearchLoadingProgress.fullResults;
						resolve(new SearchResult({ queryId: tab.queryId, results: [], resultCount: 0 }));
						return;
					}
					let actualResult = new SearchResult({ queryId: tab.queryId, results: historyData.result, resultCount: historyData.result.length });
					tempResult.results = actualResult.results;
					tempResult.resultCount = actualResult.resultCount;
					tab.loading = false;
					tab.loadingProgress = SearchLoadingProgress.fullResults;
					resolve(tempResult);
				}, (err: any) => {
					console.error(err);
					reject(err);
				});
		});
	}

	getCarriers(searchTerms: SearchListObj[], tab: SearchTab, tempResult: SearchResult): Promise<SearchResult> {
		return new Promise((resolve, reject) => {
			this.http.get('assets/test-data/mock-carriers.json')
				.subscribe((data: any) => {
					let actualResult = new SearchResult({ queryId: tab.queryId, results: data.carriers, resultCount: data.carriers.length });
					tempResult.results = actualResult.results;
					tempResult.resultCount = actualResult.resultCount;
					tab.loading = false;
					tab.loadingProgress = SearchLoadingProgress.fullResults;
					resolve(tempResult);
				}, (err: any) => {
					console.error(err);
					reject(err);
				});
		});
	}

	//eCite datetimes coming from SQL look like UTC but are intended to be interpreted as local time.
	// We're processing them here so that we don't have to have a special pipe every single time we need to display the dates.
	private formatSQLDateTimes(data: CitationModel[]) {
		_.forEach(data, (row: CitationModel) => {
			row.DateIssued = _.get(row, 'DateIssued', '').replace('Z', '');
			row.DefendantDOB = _.get(row, 'DefendantDOB', '').replace('Z', '');
			row.CourtDate = _.get(row, 'CourtDate', '').replace('Z', '');
		});
		return data;
	}

	private correctForDateRange(searchTerms: SearchListObj[]) {
		let endDate = _.find(searchTerms, ['name', 'EndDate']);
		if (endDate)
			endDate.value = moment(endDate.value, 'YYYY-MM-DD').add(1, 'days').format('YYYY-MM-DD');
		let dateIssuedUpper = _.find(searchTerms, ['name', 'DateIssuedUpper']);
		if (dateIssuedUpper)
			dateIssuedUpper.value = moment(dateIssuedUpper.value, 'YYYY-MM-DD').add(1, 'days').format('YYYY-MM-DD');
	}
}
