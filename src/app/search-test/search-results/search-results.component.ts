import { Component, Input, Output, EventEmitter, OnInit, OnDestroy, HostListener, OnChanges, SimpleChanges, ViewChild, TemplateRef } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Subscription, Observable, Subject, from } from 'rxjs';
import * as moment from 'moment';
// import { NotificationsService } from 'angular2-notifications';
const _ = require('lodash');
// const FileSaver = require('file-saver');

import {
	SearchQuery, SearchTab, SearchResult, ResultCounts, SearchListObj, CitationModel, CitationCharge, ChargeExceptions,
	SearchHistoryData, UniqueIdentifier, OVERVIEW, AtlasQueryLogModel, ThreatBadge, ThreatLevel,
} from '../../../../projects/usp-client-core/src/public_api';
import {
	LoadingAnimation, SearchLoadingProgress, ConfigService,
	StatusModelService, StatusModel, StatusConfigSection
} from '../../../../projects/usp-client-core/src/public_api';
import { SearchService } from '../search.service';

//Vehicles charge codes
const drugCharges = [];
const violenceCharges = [];
const vehicleAlcoholCharges = [];
const personAlcoholCharges = [];
const murderCharges = [];
const firearmCharges = [];
const expiredTagCharges = [];
const noLicenseCharges = [];
const suspendedLicenseCharges = [];
const seatbeltCharges = [];
const speedingCharges = [];

/**
 * SearchResults component -- Retrieves search results from the SearchService and formats them in a tabular display.
 */
@Component({
	selector: "search-results",
	styles: [require('./search-results.styles.css')],
	template: require('./search-results.view.html'),
})
export class SearchResults implements OnInit {
	OVERVIEW = OVERVIEW;
	// bigTourButton = false;

	queries: SearchQuery[] = [];
	tabs: SearchTab[] = [];
	results: SearchResult[] = [];
	historyData: SearchHistoryData[] = [];

	selectedQueryTab: SearchTab;
	selectedQuery: SearchQuery;
	selectedResult: SearchResult;
	selectedResultTab: string | number = OVERVIEW;
	selectedQueryType: string;
	selectedResultThreatBadges: ThreatBadge[];
	selectedResultThreatLevel: ThreatLevel;

	subscription: Subscription;
	closeSubscription: Subscription;
	threatAudioEvent: Subject<any> = new Subject<any>();

	applicationHeader: string;
	reportGenUrl: string;
	searchInProgress: boolean = false;
	isInterdictionUser: boolean = false;

	private threatBadges: ThreatBadge[] = [];
	private cardOpenStatus = false;
	private searchresultsStatusClass: StatusConfigSection;

	constructor(private _router: Router, private searchService: SearchService, private config: ConfigService,
		private statusService: StatusModelService, private http: HttpClient, private datePipe: DatePipe) {
		try {
			// if (!localStorage['tour_end']) {
			// 	this.bigTourButton = true;
			// }

			// this.config.load().then((config: any) => {
			// 	this.applicationHeader = config.ApplicationHeader;
			// 	this.reportGenUrl = config.reportGenUrl;

			// 	tourService.setupTour();
			// });

			this.config.configStream.subscribe((configData: any) => {
					this.threatBadges = configData.threatBadgeConfig;
				}, (errRes: Response) => {
					console.error(errRes);
				});

			// AuthService.currentUserStream.subscribe((user) => {
			// 	this.isInterdictionUser = user.IsInterdictionUser;
			// });
		}
		catch (ex) {
			console.error(ex);
			// this.errorService.handleError(new AtlasError('Error while instantiating search results component',
			// 	'constructor::SearchResults',
			// 	ex));
		}
	}

    /**
     * Retrieves cached queries plus which tab was last selected.  Subscribes to search events and broadcasts the number of open tabs.
     */
	ngOnInit() {
		try {
			this.subscription = SearchService.onSearch.subscribe((event) => this.onSearch(event));
			// this.historySubscription = SearchService.onSearchHistory.subscribe((event) => this.onSearchHistory(event));
			this.closeSubscription = SearchService.onResultsClose.subscribe((event) => this.onResultsClose(event));

			// this.statusService.configStream
			// 	.subscribe((config) => {
			// 		this.searchresultsStatusClass = config.searchresultsStatusClass;
			// 	});
		}
		catch (ex) {
			console.error(ex);
		}
	}

	// ngOnDestroy() {
	// 	try {
	// 		this.subscription.unsubscribe();
	// 	}
	// 	catch (ex) {
	// 		this.errorService.handleError(new AtlasError('Error while destroying search results component',
	// 			'ngOnDestroy::SearchResults',
	// 			ex));
	// 	}
	// }

	getTabById(queryId: UniqueIdentifier): SearchTab {
		try {
			return _.find(this.tabs, ['queryId', queryId]);
		}
		catch (ex) {
			console.error(ex);
		}
	}

	getQueryById(queryId: UniqueIdentifier): SearchQuery {
		try {
			return _.find(this.queries, ['queryId', queryId]);
		}
		catch (ex) {
			console.error(ex);
		}
	}

	getResultById(queryId: UniqueIdentifier): SearchResult {
		try {
			return _.find(this.results, ['queryId', queryId]);
		}
		catch (ex) {
			console.error(ex);
		}
	}

	serializeQuery(searchList: SearchListObj[], type: string): string {
		try {
			let label = '';
			type = type ? type : searchList[0].type;

			switch (type) {
				case 'person':
					let firstName = _.get(_.find(searchList, ['name', 'DefendantFirstName']), 'value', '').toUpperCase();
					let lastName = _.get(_.find(searchList, ['name', 'DefendantLastName']), 'value', '').toUpperCase();
					let sex = _.get(_.find(searchList, ['name', 'DefendantGender']), 'value', '').toUpperCase();
					let dob = _.get(_.find(searchList, ['name', 'DefendantDOB']), 'value', '');
					dob = dob ? this.datePipe.transform(dob, 'y-MM-dd') : '';
					label = lastName + ', ' + firstName + ' (' + sex + ') ' + dob;
					break;
				case 'vehicle':
					let vehicleState = _.get(_.find(searchList, ['name', 'VehTagState']), 'value', '');
					let tagNum = _.get(_.find(searchList, ['name', 'VehTagNum']), 'value', '');
					label = vehicleState + ', ' + tagNum;
					break;
				case 'license':
					let licenseState = _.get(_.find(searchList, ['name', 'LicState']), 'value', '');
					let licNum = _.get(_.find(searchList, ['name', 'LicNum']), 'value', '');
					label = licenseState + ', ' + licNum;
					break;
				case 'carrier':
					let dot = _.get(_.find(searchList, ['name', 'DOTOrName']), 'value', '');
					label = 'DOT/Carrier Name : ' + dot;
					break;
				case 'officer':
					let lowerDate = _.get(_.find(searchList, ['name', 'DateIssuedLower']), 'value', '');
					let upperDate = _.get(_.find(searchList, ['name', 'DateIssuedUpper']), 'value', '');
					lowerDate = lowerDate ? moment(lowerDate, 'YYYY-MM-DD').format('M/D/YYYY') : '';
					upperDate = upperDate ? moment(upperDate, 'YYYY-MM-DD').format('M/D/YYYY') : '';
					label = 'Searches ' + lowerDate + ' - ' + upperDate;
					break;
				case 'history':
					let startDate = _.get(_.find(searchList, ['name', 'StartDate']), 'value', '');
					let endDate = _.get(_.find(searchList, ['name', 'EndDate']), 'value', '');
					startDate = startDate ? moment(startDate, 'YYYY-MM-DD').format('M/D/YYYY') : '';
					endDate = endDate ? moment(endDate, 'YYYY-MM-DD').format('M/D/YYYY') : '';
					label = 'Searches ' + startDate + ' - ' + endDate;
					break;
			}

			return label;
		}
		catch (ex) {
			console.error(ex);
		}
	}

	backToHome() {
		try {
			this._router.navigate(['default/']);
		}
		catch (ex) {
			console.error(ex);
		}
	}

	// startTour() {
	// 	try {
	// 		this.bigTourButton = false;
	// 		this.tourService.forceStart();
	// 	}
	// 	catch (ex) {
	// 		console.error(ex);
	// 	}
	// }

	showCitations(): boolean {
		switch (this.selectedQueryType) {
			case 'person':
			case 'vehicle':
			case 'license':
			case 'officer':
				return true;
			default:
				return false;
		}
	}

	showCarriers(): boolean {
		return this.selectedQueryType == 'carrier';
	}

	showHistory(): boolean {
		return this.selectedQueryType == 'history';
	}

    /**
     * Main search event handler.
     * @param event An object representing the query in the format { query: string, filters: string[] }
     */
	onSearch(searchTerms: SearchListObj[]) {
		try {
			if (searchTerms[0].type == 'history') {
				//We have to do some annoying handling for past citation queries; otherwise we'd have to split them up on the search builder
				// which we don't want to do since the form is exactly the same.
				let historyType = _.find(searchTerms, ['name', 'SearchType']);
				this.selectedQueryType = historyType.value == 'citations' ? 'officer' : 'history';

				if (this.selectedQueryType == 'officer') {
					_.forEach(searchTerms, (term: SearchListObj) => {
						term.type = 'officer';
					});

					let startDate = _.find(searchTerms, ['name', 'StartDate']);
					let endDate = _.find(searchTerms, ['name', 'EndDate']);
					startDate.name = 'DateIssuedLower';
					endDate.name = 'DateIssuedUpper';
				}
			}
			else {
				this.selectedQueryType = searchTerms[0].type;
			}
			this.selectedQuery = this.insertLoadingTab(searchTerms, this.selectedQueryType);
			this.selectedQueryTab = this.getTabById(this.selectedQuery.queryId);

			this.searchService.search(searchTerms, this.selectedQueryTab, this.selectedQueryType, true)
				.then((result: SearchResult) => {
					if (this.showCitations()) {
						result.threatLevel = this.getThreatLevel(result.results);
						result.threatBadges = this.getCitationThreats(result.charges, result.chargeExceptions);
						this.threatAudioEvent.next(result.threatLevel);
					}
					else {
						result.threatLevel = -1;
						result.threatBadges = [];
					}

					//Only certain properties should be carried over here; simply overwriting the object risks losing NCIC data since it can come at any time.
					this.results.push(result);
					this.selectedResult = result;
					this.selectedResultTab = OVERVIEW;
				})
				.catch((err) => {
					this.selectedQueryTab.loading = false;
					if (err.status) {
						if (err.status == 401)
							this.selectedQuery.error = 'Your login session has expired.  Please login again then retry your search.';
						else
							this.selectedQuery.error = `API failed with error code ${err.status}.`;
					} else if (err.message) {
						this.selectedQuery.error = `Search error: ${err.message}`;
					} else {
						this.selectedQuery.error = `Failed with unknown error.`;
					}
				});
		}
		catch (ex) {
			console.error(ex);
		}
	}

	page(pageInfo: any) {
		try {
			this.selectedQuery.searchList = _.remove(this.selectedQuery.searchList, (searchTerm: SearchListObj) => {
				return searchTerm.name != 'PageNo';
			});
			this.selectedQuery.searchList.push({ name: 'PageNo', value: (pageInfo.offset + 1).toString(), type: this.selectedQuery.searchList[0].type });

			this.searchInProgress = true;
			this.searchService.search(this.selectedQuery.searchList, this.selectedQueryTab, this.selectedQueryType)
				.then((result: SearchResult) => {
					//Need to preserve threat bar data to prevent it from being wiped.
					result.threatBadges = this.selectedResult.threatBadges;
					result.threatLevel = this.selectedResult.threatLevel;

					//We want to preserve paged results across tab selection, so updated the cached result object
					let cachedResult = this.getResultById(this.selectedResult.queryId);
					cachedResult.results = result.results;

					this.selectedResult = result;
					this.searchInProgress = false;
				})
				.catch((err) => {
					this.selectedQueryTab.loading = false;
					if (err.status) {
						if (err.status == 401)
							this.selectedQuery.error = 'Your login session has expired.  Please login again then retry your search.';
						else
							this.selectedQuery.error = `API failed with error code ${err.status}.`;
					} else if (err.message) {
						this.selectedQuery.error = `Search error: ${err.message}`;
					} else {
						this.selectedQuery.error = `Failed with unknown error.`;
					}
				});
		}
		catch (ex) {
			console.error(ex);
		}
	}

	sort(sortInfo: any) {
		try {
			this.selectedQuery.searchList = _.remove(this.selectedQuery.searchList, (searchTerm: SearchListObj) => {
				return searchTerm.name != 'SortColumn' && searchTerm.name != 'SortOrder' && searchTerm.name != 'PageNo';
			});
			this.selectedQuery.searchList.push({ name: 'SortColumn', value: sortInfo.sorts[0].prop, type: this.selectedQuery.searchList[0].type });
			this.selectedQuery.searchList.push({ name: 'SortOrder', value: sortInfo.sorts[0].dir, type: this.selectedQuery.searchList[0].type });

			this.searchInProgress = true;
			this.searchService.search(this.selectedQuery.searchList, this.selectedQueryTab, this.selectedQueryType)
				.then((result: SearchResult) => {
					//Need to preserve threat bar data to prevent it from being wiped.
					result.threatBadges = this.selectedResult.threatBadges;
					result.threatLevel = this.selectedResult.threatLevel;

					//We want to preserve paged results across tab selection, so updated the cached result object
					let cachedResult = this.getResultById(this.selectedResult.queryId);
					cachedResult.results = result.results;

					this.selectedResult = result;
					this.searchInProgress = false;
				})
				.catch((err) => {
					this.selectedQueryTab.loading = false;
					if (err.status) {
						if (err.status == 401)
							this.selectedQuery.error = 'Your login session has expired.  Please login again then retry your search.';
						else
							this.selectedQuery.error = `API failed with error code ${err.status}.`;
					} else if (err.message) {
						this.selectedQuery.error = `Search error: ${err.message}`;
					} else {
						this.selectedQuery.error = `Failed with unknown error.`;
					}
				});
		}
		catch (ex) {
			console.error(ex);
		}
	}

	getRawHistory: Function = (searchTerms: SearchListObj[]) => {
		try {
			return from(this.searchService.rawSearch(searchTerms, 'history'));
		}
		catch (ex) {
			console.error(ex);
		}
	}

	// addNoteToQueryLog(queryLog: AtlasQueryLogModel) {
	// 	try {
	// 		return this.searchService.updateHistoryQuery(queryLog);
	// 	}
	// 	catch (ex) {
	// 		console.error(ex);
	// 	}
	// }

	getThreatLevel(citations: CitationModel[]): ThreatLevel {
		//If no citations are found, the threat level will be None.
		// Otherwise, it will be Low.
		if (!citations || citations.length == 0)
			return ThreatLevel.None;
		else
			return ThreatLevel.Low;
	}

	getClassForThreatLevel(queryId: UniqueIdentifier) {
		if (!queryId)
			return 'info';

		let result = this.getResultById(queryId);

		if (!result || result.threatLevel == undefined)
			return 'info';

		let level = result.threatLevel;
		if (level == ThreatLevel.None)
			return 'success';
		else if (level == ThreatLevel.Low)
			return 'warning';
		else if (level == ThreatLevel.High)
			return 'danger';
		else
			return 'info';
	}

	getCitationThreats(charges: CitationCharge[], chargeExceptions: ChargeExceptions): ThreatBadge[] {
		let threats: ThreatBadge[] = [];

		//Recent DUI
		if (chargeExceptions.RecentDUIs)
			this.addBadgeByKeyword('recent dui', threats, 1);

		_.forEach(charges, (charge: CitationCharge) => {
			//Drug Charges
			if (_.includes(drugCharges, charge.Charge))
				this.addBadgeByKeyword('drugs', threats, charge.ChargeCount);

			//Violent Crime
			if (_.includes(violenceCharges, charge.Charge))
				this.addBadgeByKeyword('battery', threats, charge.ChargeCount);

			//Vehicle-related Alcohol Offense
			if (this.selectedQueryType == 'vehicle' && _.includes(vehicleAlcoholCharges, charge.Charge))
				this.addBadgeByKeyword('alcohol', threats, charge.ChargeCount);

			//Person-related Alcohol Offense
			if (this.selectedQueryType == 'person' && _.includes(personAlcoholCharges, charge.Charge))
				this.addBadgeByKeyword('alcohol', threats, charge.ChargeCount);

			//Murder
			if (_.includes(murderCharges, charge.Charge))
				this.addBadgeByKeyword('murder', threats, charge.ChargeCount);

			//Prohibited Firearm Possession
			if (_.includes(firearmCharges, charge.Charge))
				this.addBadgeByKeyword('firearm', threats, charge.ChargeCount);

			//Expired Tag
			if (_.includes(expiredTagCharges, charge.Charge))
				this.addBadgeByKeyword('expired tag', threats, charge.ChargeCount);

			//No Driver's License
			if (_.includes(noLicenseCharges, charge.Charge) || _.endsWith(charge.Charge, '')
				|| (_.endsWith(charge.Charge, '') && chargeExceptions.NoLicenses))
				this.addBadgeByKeyword('no license', threats, charge.ChargeCount);

			//Expired Driver's License
			if (_.endsWith(charge.Charge, '') && chargeExceptions.ExpiredLicenses)
				this.addBadgeByKeyword('expired license', threats, charge.ChargeCount);

			//Suspended Driver's License
			if (_.includes(suspendedLicenseCharges, charge.Charge))
				this.addBadgeByKeyword('suspended license', threats, charge.ChargeCount);

			//Speeding
			if (_.includes(speedingCharges, charge.Charge))
				this.addBadgeByKeyword('speeding', threats, charge.ChargeCount);

			//Seatbelt
			if (_.includes(seatbeltCharges, charge.Charge))
				this.addBadgeByKeyword('seatbelt violation', threats, charge.ChargeCount);
		});
		return threats;
	}

	private addBadgeByKeyword(keyword: string, badges: ThreatBadge[], chargeCount: number) {
		let newBadge = _.find(this.threatBadges, (badge: ThreatBadge) => {
			return _.includes(badge.keywords, keyword);
		});

		//If no charges matched one of the badges, we're done.
		if (!newBadge)
			return;

		//Clone the object since we're gonna be incrementing the counter.
		newBadge = _.cloneDeep(newBadge);

		//Check if newBadge is already in the list of threats; if so, avoid adding it again.
		let existingBadge = _.find(badges, ['badgeName', newBadge.badgeName]);
		if (!existingBadge) {
			newBadge.threatCount = chargeCount;
			badges.push(newBadge);
		}
	}

	printOverview() {
		try {
			// let notification = this.notificationService.info('Loading Printout', null, 
				// { showProgressBar: false, timeOut: 0, icons: { info: '<i class="fa fa-3x fa-lg fa-spinner fa-spin" style="margin-top: 10px; font-size: 42px;"></i>' } });
			let user = JSON.parse(sessionStorage.getItem('MSAtlasUser'));
			let queryStr = this.serializeQuery(this.selectedQuery.searchList, this.selectedQueryType);
			let queryType = this.selectedQueryType;

			//We need to be careful with the search terms to ensure we get the desired results;
			// first inflate the page size to make sure we get the full result set,
			// then cut the page number if it exists to avoid getting the nth page of a 100000 strong result set.
			let moddedSearchTerms: SearchListObj[] = _.cloneDeep(this.selectedQuery.searchList);
			moddedSearchTerms.push({ name: 'PageSize', value: '100000', type: this.selectedQueryType});
			_.remove(moddedSearchTerms, (searchTerm: SearchListObj) => {
				return searchTerm.name == 'PageNo';
			});

			//We want to pull the full result set for printing.
			this.searchService.rawSearch(moddedSearchTerms, queryType).then((fullResults) => {
				this.http.post(this.reportGenUrl + 'GenerateReport',
					{ citations: fullResults.citations, queryType: queryType, userName: user.UserName, serializedQuery: queryStr })
					.subscribe((fileStream: any) => {
						let formattedByteArray = new Uint8Array(fileStream);
						let file = new Blob([formattedByteArray], { type: 'application/pdf' });
						// FileSaver.saveAs(file, 'AtlasSearchResults_' + queryStr + '.pdf');
						// this.notificationService.remove(notification.id);
					}, (err: any) => {
						// this.notificationService.remove(notification.id);
						console.error(err);
					});
			}, (err) => {
				// this.notificationService.remove(notification.id);
				console.error(err);
			});
		}
		catch (ex) {
			console.error(ex);
		}
	}

	onResultsClose(event) {
		try {
			this.queries = [];
			this.tabs = [];
			this.results = [];
			this.selectedQuery = null;
			this.selectedQueryTab = null;
			this.selectedQueryType = null;
			this.selectedResult = null;
			this.selectedResultTab = OVERVIEW;
		}
		catch (ex) {
			console.error(ex);
		}
	}

	/**
	* Insert a tab that will show a loading animation until the search results are retrieved.
	*/
	insertLoadingTab(searchList: SearchListObj[], type: string): SearchQuery {
		try {
			let id = new UniqueIdentifier();
			let searchDate = moment();

			var query = new SearchQuery({
				queryId: id,
				searchList: searchList,
				searchedAt: searchDate.toDate(),
				error: '',
				notificationIds: []
			});

			var tab = new SearchTab({
				queryId: id,
				tabNumber: this.tabs[this.tabs.length - 1] ? this.tabs[this.tabs.length - 1].tabNumber + 1 : 1,
				tabLabel: this.serializeQuery(searchList, type),
				loading: true,
				loadingProgress: SearchLoadingProgress.noResults,
				showOffline: false,
				newResults: false,
				newPeople: false,
				newVehicles: false,
				highlightColor: "#D3D3D3"
			});

			this.selectedQueryTab = tab;
			this.selectedResultTab = OVERVIEW;
			this.queries.push(query);
			this.tabs.push(tab);
			return query;
		}
		catch (ex) {
			console.error(ex);
		}
	}

	// insertHistoryResultsTab(searchTerms: SearchListObj[], historyData: SearchHistoryData) {
	// 	try {
	// 		let id = new UniqueIdentifier();
	// 		let startDate = _.find(searchTerms, ['name', 'StartDate']);
	// 		let endDate = _.find(searchTerms, ['name', 'EndDate']);
	// 		startDate = startDate ? moment(startDate.value, 'YYYY-MM-DD').format('M/D/YYYY') : '';
	// 		endDate = endDate ? moment(endDate.value, 'YYYY-MM-DD').format('M/D/YYYY') : '';

	// 		this.selectedQueryTab = new SearchTab({
	// 			queryId: id,
	// 			tabNumber: 0,
	// 			tabLabel: 'Searches ' + startDate + ' - ' + endDate,
	// 			loading: false,
	// 			loadingProgress: SearchLoadingProgress.fullResults,
	// 			showOffline: true,
	// 			newResults: false,
	// 			newPeople: false,
	// 			newVehicles: false,
	// 			highlightColor: "#D3D3D3"
	// 		});

	// 		this.selectedQuery = new SearchQuery({
	// 			queryId: id,
	// 			searchList: searchTerms,
	// 			searchedAt: new Date(),
	// 			error: '',
	// 			notificationIds: []
	// 		});
	// 		this.selectedQueryType = 'history';

	// 		this.selectedResultTab = OVERVIEW;
	// 		this.selectedResult = new SearchResult({
	// 			queryId: id,
	// 			results: historyData,
	// 			resultCount: historyData.length
	// 		});
	// 		this.selectedResult.queryId = id; //selectedResult has its original id still, so overwrite it so we can find it later.

	// 		this.queries.push(this.selectedQuery);
	// 		this.tabs.push(this.selectedQueryTab);
	// 		this.results.push(this.selectedResult);
	// 	}
	// 	catch (ex) {
	// 		console.error(ex);
	// 	}
	// }

	numQueries() {
		try {
			return this.queries.length;
		}
		catch (ex) {
			console.error(ex);
		}
	}

	selectQueryTab(queryId: UniqueIdentifier) {
		try {
			if (!queryId)
				return;
			if (this.selectedQueryTab.queryId == queryId)
				return;

			let tab = this.getTabById(queryId);
			let query = this.getQueryById(queryId);
			let result = this.getResultById(queryId);
			tab.newResults = false;

			this.selectedQueryTab = tab;

			this.selectedQueryTab.loading = true;
			this.selectedQueryTab.loadingProgress = SearchLoadingProgress.noResults;

			this.selectedResultTab = OVERVIEW;
			this.selectedQuery = query;
			this.selectedResult = result;
			this.selectedQueryType = query.searchList[0].type;

			setTimeout(() => {
				this.selectedQueryTab.loading = false;
				this.selectedQueryTab.loadingProgress = SearchLoadingProgress.fullResults;
			}, 1);
		}
		catch (ex) {
			console.error(ex);
		}
	}

	closeQueryTab(queryId: UniqueIdentifier) {
		try {
			//Remove the query/tab/result objects associated with the given id while closing any open notifications.
			_.remove(this.tabs, (tab: SearchTab) => {
				if (tab.queryId == queryId) {
					//We need to select another tab as we close the current one, assuming there's more than one tab.
					let index = this.tabs.indexOf(tab);
					if (this.tabs.length > 1) {
						if (index > 0) //Open the tab to the left of the current one.
							this.selectQueryTab(this.tabs[index - 1].queryId);
						else //Open the tab to the right of the current one.
							this.selectQueryTab(this.tabs[index + 1].queryId);
					}

					return true;
				}
				else
					return false;
			});
			_.remove(this.queries, (query: SearchQuery) => {
				if (query.queryId == queryId) {
					_.forEach(query.notificationIds, (id) => {
						// this.notificationService.remove(id);
					});
					return true;
				}
				else
					return false;
			});
			_.remove(this.results, (result: SearchResult) => {
				return result.queryId == queryId;
			});

			if (this.tabs.length == 0) {
				this.threatAudioEvent = new Subject<any>();
				SearchService.onResultsClose.emit();
			}
		}
		catch (ex) {
			console.error(ex);
		}
	}

	closeAllTabs() {
		try {
			this.queries = [];
			this.tabs = [];
			this.results = [];
			this.threatAudioEvent = new Subject<any>();
			SearchService.onResultsClose.emit();
		}
		catch (ex) {
			console.error(ex);
		}
	}

	selectNextQueryTab() {
		try {
			// Select the tab to the right of the current one.
			let index = this.tabs.indexOf(this.selectedQueryTab);
			let nextTab: SearchTab = this.tabs[index + 1];

			if (!nextTab)
				return;

			this.selectQueryTab(nextTab.queryId);
		}
		catch (ex) {
			console.error(ex);
		}
	}

	selectPrevQueryTab() {
		try {
			// Select the tab to the left of the current one.
			let index = this.tabs.indexOf(this.selectedQueryTab);
			let prevTab: SearchTab = this.tabs[index - 1];

			if (!prevTab)
				return;

			this.selectQueryTab(prevTab.queryId);
		}
		catch (ex) {
			console.error(ex);
		}
	}

	selectNextResultTab() {
		//Select the result tab BELOW the current one
		// let numCarriers = this.selectedResult.carriers.length;
		// let hasPeople = this.selectedResult.people && this.selectedResult.people.responses.length > 0;
		// let hasVehicles = this.selectedResult.vehicles && this.selectedResult.vehicles.responses.length > 0;

		// if (_.isNumber(this.selectedResultTab)) {
		// 	// A carrier result tab is currently selected; show the next one if it exists.
		// 	if (this.selectedResultTab < numCarriers - 1)
		// 		this.selectedResultTab = _.toNumber(this.selectedResultTab) + 1;
		// 	else
		// 		return;
		// }
		// else {
		// 	// The overview, person, or vehicles result tab is currently selected.
		// 	//Show the next summary tab that exists or show the first carrier tab.
		// 	switch (this.selectedResultTab) {
		// 		case OVERVIEW:
		// 			if (hasPeople)
		// 				this.selectedResultTab = PEOPLE;
		// 			else if (hasVehicles)
		// 				this.selectedResultTab = VEHICLES;
		// 			else if (numCarriers > 0)
		// 				this.selectedResultTab = 0;
		// 			break;
		// 		case PEOPLE:
		// 			if (hasVehicles)
		// 				this.selectedResultTab = VEHICLES;
		// 			else if (numCarriers > 0)
		// 				this.selectedResultTab = 0;
		// 			break;
		// 		case VEHICLES:
		// 			if (numCarriers > 0)
		// 				this.selectedResultTab = 0;
		// 			else
		// 				return;
		// 			break;
		// 		default:
		// 			return;
		// 	}
		// }
	}

	selectPrevResultTab() {
		//Select the result tab ABOVE the current one
		// let numCarriers = this.selectedResult.carriers.length;
		// let hasPeople = this.selectedResult.people && this.selectedResult.people.responses.length > 0;
		// let hasVehicles = this.selectedResult.vehicles && this.selectedResult.vehicles.responses.length > 0;

		// if (_.isNumber(this.selectedResultTab)) {
		// 	// A carrier result tab is currently selected; show the previous one if it exists.
		// 	//Otherwise, show the first summary result tab that exists.
		// 	if (this.selectedResultTab > 0)
		// 		this.selectedResultTab = _.toNumber(this.selectedResultTab) - 1;
		// 	else if (this.selectedResultTab == 0) {
		// 		if (hasVehicles)
		// 			this.selectedResultTab = VEHICLES;
		// 		else if (hasPeople)
		// 			this.selectedResultTab = PEOPLE;
		// 		else
		// 			this.selectedResultTab = OVERVIEW;
		// 	}
		// }
		// else {
		// 	// The overview, person, or vehicles result tab is currently selected.
		// 	switch (this.selectedResultTab) {
		// 		case OVERVIEW:
		// 			return;
		// 		case PEOPLE:
		// 			this.selectedResultTab = OVERVIEW;
		// 			break;
		// 		case VEHICLES:
		// 			if (hasPeople)
		// 				this.selectedResultTab = PEOPLE;
		// 			else
		// 				this.selectedResultTab = OVERVIEW;
		// 			break;
		// 		default:
		// 			return;
		// 	}
		// }
	}

	/**
 * Keydown event that can close the currently active tab or all of the open tabs.
 */
	@HostListener('window:keydown', ['$event'])
	handleKeyboardEvent(event: KeyboardEvent) {
		try {
			if (this.tabs.length > 0) { //These shortcuts only apply if the results are being shown.
				if ((event.ctrlKey || event.metaKey) && event.shiftKey) {
					switch (String.fromCharCode(event.which).toLowerCase()) {
						case 'x':
							event.preventDefault();
							this.closeQueryTab(this.selectedQueryTab.queryId);
							break;
						case 'a':
							event.preventDefault();
							this.closeAllTabs();
							break;
					}
				} else if ((event.ctrlKey || event.metaKey)) {
					switch (event.keyCode) {
						case 37: // ctrl+left
							this.selectPrevQueryTab();
							event.preventDefault();
							break;
						case 38: // ctrl+up
							this.selectPrevResultTab();
							event.preventDefault();
							break;
						case 39: // ctrl+right
							this.selectNextQueryTab();
							event.preventDefault();
							break;
						case 40: // ctrl + down
							this.selectNextResultTab();
							event.preventDefault();
							break;
					}
				}
			}
		}
		catch (ex) {
			console.error(ex);
		}
	}
}
