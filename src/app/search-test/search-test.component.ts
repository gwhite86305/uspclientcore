import { Component, Input, EventEmitter, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ModalDirective } from "ngx-bootstrap";
// import { ReleaseNote } from '../core';
// import {
// 	SearchQuery, SearchResult, UniqueIdentifier,
// 	CarrierModel, VehicleModel, OwnerModel, SocketResponse, SocketResponseList, SearchCategory
// } from '../search';
// import { AuthService } from '../auth';
import { SearchService } from './search.service';
import { SearchCategory } from '../../../projects/usp-client-core/src/public_api';
const categoriesJSON = require('../../core-config.json').searchCategoryConfig;
// const releaseNotesJSON = require('../../config/releasenotes.json');

@Component({
	selector: "search-test",
	template: require('./search-test.view.html')
})
export class SearchTest {
	showHistory: boolean = false;
	categories: SearchCategory[] = categoriesJSON;
	// releaseNotes: ReleaseNote[] = releaseNotesJSON;
	onResultsClose: EventEmitter<any> = new EventEmitter<any>();

	@ViewChild('releaseNotesModal')
	releaseNotesModal: ModalDirective;

	// constructor(private service: SearchService, private authService: AuthService, private http: HttpClient) {
	constructor() {
			
	}

	search(searchTerms?: any) {
		console.info('Search has been initiated with the following terms', searchTerms);
		// this.showHistory = false;
		SearchService.onSearch.emit(searchTerms);
	}

	// historySearch(searchTerms?: any) {
	// 	this.showHistory = false;
	// 	SearchService.onSearchHistory.emit(searchTerms);
	// 

	// deleteHistory() {
	// 	// this.service.deleteHistory();
	// }

	// resultsClose() {

	// }

	// showReleaseNotes() {
	// 	this.releaseNotesModal.show();
	// }

	// fontSizeChanged(newSize) {
	// 	//Most of the application will respond to the font-slider automatically with a few allowable exceptions
	// 	// However, the ngx-datatables will not respond and we need to intervene with regards to those by directly applying the font-size
	// 	$('#fontSizeOverrides').empty().append('.datatable-body-cell, .datatable-header-cell, .datatable-footer { font-size: ' + newSize + 'px !important; }');
	// }
}
