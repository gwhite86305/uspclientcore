import { Injectable, Inject, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from "@angular/common/http";
import { ReplaySubject, Subject, Observable } from 'rxjs';
import * as moment from 'moment';
const _ = require('lodash');

import { SearchLoadingProgress, LETSResult, LETSDetails } from '../../../projects/usp-client-core/src/public_api';
import { SearchQuery, SearchTab, SearchResult, UniqueIdentifier, SearchListObj, SearchHistoryData, AtlasQueryLogModel, CitationModel } from '../../../projects/usp-client-core/src/public_api';

@Injectable()
export class MobileSearchService {
	searchResults: any[];
	private selectedResultDetails: LETSDetails[];

	constructor() {

	}

	ngOnInit() {

	}

	setSelectedResultDetails(result: LETSResult) {
		this.selectedResultDetails = result.getDetails();
	}

	getSelectedResultDetails(): LETSDetails[] {
		return this.selectedResultDetails;
	}
}