import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Subscription, Observable } from 'rxjs';
import { ConfigService, SearchCategory } from '../../../../projects/usp-client-core/src/public_api';
const _ = require('lodash');

@Component({
	selector: 'mobile-search',
	templateUrl: './mobile-search.component.html',
	styleUrls: ['./mobile-search.component.css']
})
export class MobileSearch implements OnInit, OnDestroy {
	detailCategory: string = '';
	categoryConfig$: Promise<SearchCategory> = null;
	private paramSub: Subscription;

	constructor(private route: ActivatedRoute, private router: Router, private configService: ConfigService) {

	}

	ngOnInit() {
		this.categoryConfig$ = new Promise<SearchCategory>((resolve, reject) => {
			this.paramSub = this.route.params.subscribe(params => {
				this.detailCategory = params['category'];
				this.configService.configStream.subscribe((config) => {
					let selectedCategory = _.find(config.searchCategoryConfig, ['name', this.detailCategory]);
					resolve(selectedCategory);
				});
			});
		});

	}

	ngOnDestroy() {
		this.paramSub.unsubscribe();
	}

	search(params: any) {
		console.warn('Search invoked', params);
	}
}
