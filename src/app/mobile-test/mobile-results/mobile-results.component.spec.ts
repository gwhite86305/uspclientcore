import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MobileResultsComponent } from './mobile-results.component';

describe('MobileResultsComponent', () => {
  let component: MobileResultsComponent;
  let fixture: ComponentFixture<MobileResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MobileResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MobileResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
