import { Component, OnInit, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { CitationResult, LETSResult } from '../../../../projects/usp-client-core/src/public_api';
import { MobileSearchService } from '../mobile-search.service';

@Component({
  selector: 'mobile-results',
  templateUrl: './mobile-results.component.html',
  styleUrls: ['./mobile-results.component.css']
})
export class MobileResults implements OnInit {
	toggler: EventEmitter<any> = new EventEmitter<any>();
	testCitations: CitationResult[] = [];

	constructor(private router: Router, private searchService: MobileSearchService) { 
		this.testCitations = [new CitationResult({
				"DocumentID": "",
				"TicketNum": "123456789",
				"DateIssued": "2000-01-01T00:00:00-05:00",
				"TicketCity": "TEST",
				"TicketCounty": "TEST",
				"DocStatus": "0",
				"Description": "TEST DESCRIPTION",
				"FirstName": "FIRST",
				"MiddleName": "MIDDLE",
				"LastName": "LAST",
				"LicNum": "1234567",
				"LicState": "AL",
				"TagNum": "1234567",
				"TagState": "AL"
			}), new CitationResult({
				"DocumentID": "",
				"TicketNum": "123456789",
				"DateIssued": "2000-01-01T00:00:00-05:00",
				"TicketCity": "TEST",
				"TicketCounty": "TEST",
				"DocStatus": "0",
				"Description": "TEST DESCRIPTION",
				"FirstName": "FIRST2",
				"MiddleName": "MIDDLE2",
				"LastName": "LAST2",
				"LicNum": "1234567",
				"LicState": "AL",
				"TagNum": "1234567",
				"TagState": "AL"
			})
		]
	}

	ngOnInit() {
	}

	toggleSidebar() {
		this.toggler.emit();
	}

	showDetails(selectedResult: LETSResult) {
		this.searchService.setSelectedResultDetails(selectedResult);
		this.router.navigate(['./mobile-details']);
	}
}
