import { Component, OnInit, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

import { MobileSearchService } from './mobile-search.service';
import { SearchCategory, ConfigService, LETSRecentSearch } from '../../../projects/usp-client-core/src/public_api';
const _ = require('lodash');

@Component({
	selector: 'mobile-test',
	templateUrl: './mobile-test.component.html',
	styleUrls: ['./mobile-test.component.css']
})
export class MobileTest implements OnInit {
	toggler: EventEmitter<any> = new EventEmitter<any>();
	searchBarToggler: EventEmitter<any> = new EventEmitter<any>();
	sliderToggler: EventEmitter<SearchCategory> = new EventEmitter<SearchCategory>();
	searchCategories: SearchCategory[] = [];
	searchCategories$: Promise<SearchCategory[]> = null;

	recentSearches: LETSRecentSearch[] = [];

	constructor(private router: Router, private configService: ConfigService, private searchService: MobileSearchService) {
		this.searchCategories$ = new Promise<SearchCategory[]>((resolve, reject) => {
			this.configService.configStream.subscribe((config) => {
				console.log('Mobile test invoked', config.searchCategoryConfig);
				this.searchCategories = config.searchCategoryConfig;
				resolve(this.searchCategories);
			});
		});

		this.recentSearches = [
			new LETSRecentSearch('search:AlaSafe ssn:777777777 dob:2001-01-01'),
			new LETSRecentSearch('search:NameMaster ssn:777777777 state:AL'),
			new LETSRecentSearch('search:SexOffender search:ProtectionOrder search:NameMaster search:HotfileWarrant search:DOC search:DL ssn:777777777 city:Huntsville state:AL countydistance:all')
		]
	}

	ngOnInit() {
	}

	toggleSidebar() {
		this.toggler.emit();
	}

	toggleSearchBar() {
		this.searchBarToggler.emit();
	}

	toggleSlider(category: SearchCategory) {
		this.sliderToggler.emit(category);
	}

	// showDetails(data: any) {
	// 	this.searchService.setSelectedResultDetails(data);
	// 	this.router.navigate(['/mobile-details']);
	// }

	parseSmartSearchParams(rawParams) {
		let parsedParams: string[] = [];
		_.map(_.split(rawParams, ' '), (keyValue: string) => {
			if (!_.includes(keyValue, 'search:')) {
				let splitPair = _.split(keyValue, ':');
				let key = splitPair[0];
				let val = splitPair[1];
				parsedParams[key] = val;
			}
		});
		return parsedParams;
	}

	parseSmartSearchSources(rawParams) {
		let parsedSources: string[] = [];
		_.map(_.split(rawParams, ' '), (keyValue: string) => {
			if (_.includes(keyValue, 'search:')) {
				let splitPair = _.split(keyValue, ':');
				let key = splitPair[0];
				let val = splitPair[1];
				parsedSources.push(val);
			}
		});
		return parsedSources;
	}

	search(params: any) {
		console.warn('Search method invoked', params);
	}
}
