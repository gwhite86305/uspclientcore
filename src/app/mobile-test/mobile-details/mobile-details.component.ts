import { Component, OnInit, EventEmitter } from '@angular/core';
import { LETSDetails } from '../../../../projects/usp-client-core/src/public_api';
import { MobileSearchService } from '../mobile-search.service';
const _ = require('lodash');

@Component({
	selector: 'mobile-details',
	templateUrl: './mobile-details.component.html',
	styleUrls: ['./mobile-details.component.css']
})
export class MobileDetails implements OnInit {
	toggler: EventEmitter<any> = new EventEmitter<any>();
	details: LETSDetails[];

	constructor(private searchService: MobileSearchService) {
		this.details = searchService.getSelectedResultDetails();
	}

	ngOnInit() {

	}
}
