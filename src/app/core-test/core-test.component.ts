import { Component, Input, ViewChild, EventEmitter } from '@angular/core';
import { ModalDirective } from "ngx-bootstrap";
import { ReleaseNote } from '../../../projects/usp-client-core/src/lib/core-types';
const _ = require('lodash');
const statesJSON = require('./states.json');

export interface RefData {
	text: string;
	value: string;
}

@Component({
	selector: "core-test",
	templateUrl: './core-test.view.html'
})
export class CoreTest {
    dropdownModel: string = 'AL';
	states: RefData[];
	apiStatusOpen: boolean = true;
	testSources: any[] = [{ name: 'TEST1',	value: '111' }, { name: 'TEST2', value: '222' }];
	releaseNotes: ReleaseNote[] = [{ version: '1.1.0', notes: ['TEST'] }, { version: '1.0.0', notes: ['TEST', 'MORE TEST'] }];

	toggler: EventEmitter<any> = new EventEmitter<any>();
	@ViewChild('testWrapper')
	testWrapper: HTMLDivElement;

	@ViewChild('releaseNotesModal')
	releaseNotesModal: ModalDirective;
    
	constructor() {
		this.states = _.map(statesJSON, (state) => {
			return {
				text: state.name,
				value: state.abbreviation
			};
		});
	}

	dropdownChanged(state) {
		this.dropdownModel = state;
	}

	testExport() {
		console.log('Export method working');
	}

	showReleaseNotes() {
		this.releaseNotesModal.show();
	}

	toggleSidebar() {
		this.toggler.emit();
	}
}
